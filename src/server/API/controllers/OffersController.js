const Offer = require('../../models/Offer');
const Offer2 = require('../../models/OfferDummy');

module.exports = {
  find: (params, callback) => {
    Offer.find(params, (err, offers) => {
      if (err) {
        callback(err, null);
        return;
      }
      const list = [];
      offers.map((offer) => {
        list.push(offer.summary());
      });
      callback(null, list);
    });
  },

  findById: (id, callback) => {
    Offer.findById(id, (err, offer) => {
      if (err) {
        callback(err, null);
        return;
      }

      callback(null, offer);
    });
  },

  findOne: (params, callback) => {
    Offer.findOne(params, (err, offer) => {
      if (err) {
        callback(err, null);
        return;
      }

      callback(null, offer);
    });
  },

  findByIdAndRemove: (id, callback) => {
    Offer.findByIdAndRemove(id, (err, offer) => {
      if (err) {
        callback(err, null);
        return;
      }

      callback(null, offer);
    });
  },

  findOneSync(params, callback) {
    Offer.findOne(params, (err, offer) => {
      if (err) {
        callback(err, null);
        return;
      }

      callback(null, offer);
    });
  },

  create(params, callback) {
    Offer.create(params, (err, offer) => {
      if (err) {
        callback(err, null);
        return;
      }

      callback(null, offer);
    });
  },

  update: (foundOne, params, callback) => {
    Offer.findOneAndUpdate(foundOne, params, { new: true }, (err, offer) => {
      if (err) {
        callback(err, null);
        return;
      }

      callback(null, offer);
    });
  },

  sort: (params, alias, callback) => {
    // console.log('params', params);
    Offer.find(params, (err, offers) => {
      if (err) {
        callback(err, null);
        return;
      }

      const list = [];
      offers.map((offer) => {
        list.push(offer.summary(alias));
        return list;
      });

      callback(null, list);
    });
  },

  sortOld: (params, callback) => {
    Offer.find(params, (err, offers) => {
      if (err) {
        callback(err, null);
        return;
      }

      callback(null, offers);
    });
  },
};
