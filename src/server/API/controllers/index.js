const UserController = require('./UsersController');
const OfferController = require('./OffersController');
const tempOfferController = require('./tempOfferController');

module.exports = {
  OfferController,
  user: UserController,
  tempOffer: tempOfferController,
};
