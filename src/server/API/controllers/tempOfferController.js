const TempOffer = require('../../models/TempOffer.js');

module.exports = {
  find: params =>
    new Promise((resolve, reject) => {
      TempOffer.find(params, (err, offers) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(offers);
      });
    }),

  findById: id =>
    new Promise((resolve, reject) => {
      TempOffer.findById(id, (err, offer) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(offer);
      });
    }),

  create: params =>
    new Promise((resolve, reject) => {
      TempOffer.create(params, (err, offer) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(offer);
      });
    }),

  update: (filter, params) =>
    new Promise((resolve, reject) => {
      TempOffer.findOneAndUpdate(filter, params, { new: true }, (err, offer) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(offer);
      });
    }),

  delete: id =>
    new Promise((resolve, reject) => {
      TempOffer.findByIdAndRemove(id, (err, offer) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(offer);
      });
    }),
};
