import axios from 'axios';
import { sysApiURL, sysLogin, sysUnlockWallet } from './SysGeneralController';

const sysApiOffernewURL = `${sysApiURL}/offernew`;
const sysApiOfferinfoURL = `${sysApiURL}/offerinfo`;

const sysOfferinfo = (token, guid) =>
  axios
    .get(`${sysApiOfferinfoURL}?guid=${guid}`, {
      headers: {
        Token: token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => response.data)
    .catch((err) => {
      throw err;
    });

const sysOffernew = (token, offer) =>
  axios
    .post(sysApiOffernewURL, offer, {
      headers: {
        Token: token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => response)
    .catch((err) => {
      throw err;
    });

const sysNewOffer = (walletPwd, offer) =>
  sysLogin()
    .then((token) => {
      console.log('Syscoin Loged:', token ? 'true' : 'false');
      return sysUnlockWallet(token, walletPwd);
    })
    .then((res) => {
      console.log('Sys Wallet Unlocked: ', res.unlocked);
      return sysOffernew(res.token, offer);
    })
    .then((res) => {
      console.log('Sys Offer Created: ', res.data ? 'true' : 'false');
      return res.data; // [txit, guid]
    })
    .catch((err) => {
      console.log('New Offer Error: ', err.response.data);
      throw err.response.data;
    });

const sysInfoOffer = guid =>
  sysLogin()
    .then((token) => {
      console.log('Syscoin Loged:', token ? 'true' : 'false');
      return sysOfferinfo(token, guid);
    })
    .then((res) => {
      console.log('Syscoin Offer Info recieved: ', !!res.offer);
      return res;
    })
    .catch((err) => {
      console.log('Syscoin Offer Info Error: ', err.response.data);
      throw err.response.data;
    });

export { sysNewOffer, sysInfoOffer };
