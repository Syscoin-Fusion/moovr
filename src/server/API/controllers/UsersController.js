const bcrypt = require('bcryptjs');

const User = require('../../models/User');

module.exports = {
  get: (params, isRaw) =>
    new Promise((resolve, reject) => {
      User.find(params, (err, users) => {
        if (err) {
          reject(err);
          return;
        }
        if (isRaw) {
          resolve(users);
        } else {
          const list = [];
          users.forEach((user) => {
            list.push(user.summary());
          });

          resolve(list);
        }
      });
    }),

  getUserByAliasname: aliasname =>
    new Promise((resolve, reject) => {
      User.findOne({ aliasname }, (err, user) => {
        if (err) {
          reject(err);
          return;
        }

        if (user == null) {
          reject({ message: 'Unknown User' });
          return;
        }

        resolve(user);
      });
    }),

  getUserByEmail: email =>
    new Promise((resolve, reject) => {
      User.findOne({ email }, (err, user) => {
        if (err) {
          reject(err);
          return;
        }

        if (user == null) {
          reject({ message: 'Unknown User' });
          return;
        }

        resolve(user);
      });
    }),

  comparePassword: (password, user) =>
    new Promise((resolve, reject) => {
      bcrypt.compare(`${user.aliasname}${password}`, user.password, (err, isMatch) => {
        if (err) {
          reject(err);
          return;
        }
        if (isMatch === true) {
          resolve(user.summary());
        } else {
          reject({ message: 'Wrong Password' });
        }
      });
    }),

  getById: (id, isRaw) =>
    new Promise((resolve, reject) => {
      User.findById(id, (err, user) => {
        if (err) {
          reject(err);
          return;
        }

        if (user == null) {
          reject(err);
        }

        if (isRaw) {
          resolve(user);
        } else {
          resolve(user.summary());
        }
      });
    }),

  post: (params, isRaw) =>
    new Promise((resolve, reject) => {
      // hash password
      if (params.password) {
        params.password = bcrypt.hashSync(`${params.aliasname}${params.password}`, 10);
      }

      User.create(params, (err, user) => {
        if (err) {
          if (err.toJSON().code === 11000) {
            reject(`${err.toJSON().op.aliasname || err.toJSON().op.email} has already been taken`);
            return;
          }
          console.log(JSON.stringify(err));
          reject(err);
          return;
        }
        if (isRaw) {
          resolve(user);
        } else {
          resolve(user.summary());
        }
      });
    }),

  delById: userId =>
    new Promise((resolve, reject) => {
      User.remove({ _id: userId }, (err, succeed) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(succeed);
      });
    }),
};
