import axios from 'axios';

const sysAuthHash = '7c0a25c06ea30bae50e39a37a5997e31a1a96e20';
const sysApiURL = 'https://d2fzm6xoa70bg8.cloudfront.net';

const sysApiLogingURL = `${sysApiURL}/login?auth=${sysAuthHash}`;
const sysApiWalletpassphraseURL = `${sysApiURL}/walletpassphrase`;

const sysLogin = () =>
  axios
    .get(sysApiLogingURL)
    .then(response => response.data.token)
    .catch((err) => {
      throw err;
    });

const sysUnlockWallet = (token, password) =>
  axios
    .post(
      sysApiWalletpassphraseURL,
    {
      passphrase: password,
      timeout: 90,
    },
    {
      headers: {
        Token: token,
      },
    },
    )
    .then((response) => {
      const tokenResponse =
        response.data === null ? { unlocked: true, token } : { unlocked: false, token: '' };
      return tokenResponse;
    })
    .catch((err) => {
      throw err;
    });

export { sysAuthHash, sysApiURL, sysLogin, sysUnlockWallet };
