import axios from 'axios';
import { sysApiURL, sysLogin, sysUnlockWallet } from './SysGeneralController';

const sysApiAliasnewURL = `${sysApiURL}/aliasnew`;
const sysApiAliasinfoURL = `${sysApiURL}/aliasinfo`;

const sysAliasnew = (token, alias) =>
  axios
    .post(sysApiAliasnewURL, alias, {
      headers: {
        Token: token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => response)
    .catch((err) => {
      throw err;
    });

const sysAliasinfo = (token, alias) =>
  axios
    .get(sysApiAliasinfoURL, {
      headers: {
        Token: token,
        'Content-Type': 'application/json',
      },
      params: {
        aliasname: alias,
      },
    })
    .then(response => response)
    .catch((err) => {
      throw err;
    });

const sysNewAlias = (walletPwd, alias) =>
  sysLogin()
    .then((token) => {
      console.log('Syscoin Logged:', token ? 'true' : 'false');
      return sysUnlockWallet(token, walletPwd);
    })
    .then((res) => {
      console.log('Wallet Unlocked: ', res.unlocked);
      return sysAliasnew(res.token, alias);
    })
    .then((res) => {
      console.log('Alias Created: ', res.data ? 'true' : 'false');
      return res.data;
    })
    .catch((err) => {
      console.log('New Alias Error: ', err.response.data);
      throw err.response.data;
    });

const sysInfoAlias = alias =>
  sysLogin()
    .then((token) => {
      console.log('Syscoin Logged:', token ? 'true' : 'false');
      return sysAliasinfo(token, alias);
    })
    .then(res => res.data)
    .catch((err) => {
      console.log('Alias Info Error: ', err.response.data);

      throw err.response.data;
    });

export { sysNewAlias, sysInfoAlias };
