// const geolib = require('geolib');
import express from 'express';
import axios from 'axios';
import bcrypt from 'bcryptjs';

import OffersHelper from '../helpers/OffersHelper_old';
import controllers from '../controllers';
import { sysOfferNew, sysOfferInfo } from '../helpers/SysOffersHelper';
import { offerFilter, isOffer } from '../helpers/OffersHelper';
import { checkTempOffers } from '../helpers/TempOffersHelper';

const phraseToSign = process.env.PHRASE_TO_SIGN;

const router = express.Router();

router.get('/tempoffer', (req, res) => {
  checkTempOffers();
});

/** ****************
 * NEW API ROUTES *
 ***************** */

// CREATE_OFFER
router.post('/new', (req, res) => {
  const walletPwd = 'password'; // this should be a Value passed in req.body
  const rawOffer = req.body;
  const hash = bcrypt.hashSync(phraseToSign, 10); // 8
  const offer = {
    // required fields
    alias: rawOffer.alias,
    category: rawOffer.category,
    title: rawOffer.title,
    quantity: rawOffer.quantity,
    price: rawOffer.price,
    description: JSON.stringify({
      hash,
      featured: false,
      description: rawOffer.description,
      media: rawOffer.media,
    }),
    currency: rawOffer.currency,
    // optional fields
    paymentoptions: rawOffer.paymentoptions,
    private: rawOffer.private,
    geolocation: JSON.stringify(rawOffer.geolocation),
  };
  sysOfferNew(walletPwd, offer)
    .then((response) => {
      console.log('ACZ (sysOfferNew response):', response);
      res.json({
        response,
      });
      offer.txid = response[0];
      offer.offer = response[1];
      return controllers.tempOffer.create(offer);
    })
    .then((data) => {
      console.log(`TBA (tempOffer): ${data}`);
      checkTempOffers();
    })
    .catch((error) => {
      res.send(error);
    });
});

// EDIT_OFFER
router.put('/edit/:id', (req, res) => {
  const { id } = req.params;
  const url =
    'https://d2fzm6xoa70bg8.cloudfront.net/login?auth=e4031de36f45af2172fa8d0f054efcdd8d4dfd62';
  axios
    .get(url)
    .then((response) => {
      axios
        .post('https://d2fzm6xoa70bg8.cloudfront.net/offerupdate', req.body, {
          headers: {
            Token: response.data.token,
          },
        })
        .then((results) => {
          OffersHelper.update(
            {
              offer: results.data.offer,
            },
            results.data,
            (err, result) => {
              if (err) {
                res.json({
                  confirmation: 'fail',
                  message: err,
                });

                return;
              }

              res.json({
                confirmation: 'success',
                result,
              });
            },
          );
        })
        .catch((err) => {
          console.error('err', err);
          res.json({
            error: err,
          });
        });
    })
    .catch((err) => {
      res.json({
        err,
      });
    });
});

// OFFER_FILTER
router.get('/offerfilter', (req, res) => {
  offerFilter(req.query)
    .then((results) => {
      res.json({
        confirmation: 'success',
        results: results.data || results,
        totalPages: results.totalPages,
      });
      checkTempOffers();
    })
    .catch((err) => {
      res.json({
        confirmation: 'fail',
        message: err,
      });
    });
});

//-----------------
// LEGACY CRUD API
//-----------------
// GET_ALL_OFFERS
router.get('/', (req, res) => {
  OffersHelper.find(req.query, (err, results) => {
    if (err) {
      res.json({
        confirmation: 'fail',
        message: err,
      });
      return;
    }
    res.json({
      confirmation: 'success',
      results,
    });
  });
});

// DELETE_OFFER
router.delete('/delete/:id', (req, res) => {
  OffersHelper.findByIdAndRemove(req.params.id, (err, result) => {
    if (err) {
      res.json({
        confirmation: 'fail',
        message: err,
      });
      return;
    }

    res.json({
      confirmation: 'success',
      result,
    });
  });
});

/** ****************
 * OLD API ROUTES *
 ***************** */
// -ACZ Note:--------------------------------------------------
//  these old API routes are going to be deleted in the future
//------------------------------------------------------------

// GET_OFFER
router.get('/:id', (req, res) => {
  OffersHelper.findOne(
    {
      _id: req.params.id,
    },
    (err, result) => {
      if (err) {
        res.json({
          confirmation: 'fail',
          message: err,
        });

        return;
      }

      res.json({
        confirmation: 'success',
        result,
      });
    },
  );
});

router.get('/sort', (req, res, next) => {
  const { type, price, title, category } = req.query;
  const params = {};
  const symbols = [];

  type === 'btc'
    ? symbols.push('BTC')
    : {
      ...params,
    };
  type === 'sys'
    ? symbols.push('SYS')
    : {
      ...params,
    };
  type === 'zec'
    ? symbols.push('ZEC')
    : {
      ...params,
    };

  if ((category !== 'Z-A' || 'A-Z') && category) {
    params.category = category;
  }

  params.paymentoptions_display = new RegExp(symbols.join('|'), 'i');

  OffersHelper.sortOld(params, (err, results) => {
    if (err) {
      res.json(err);
      next(err);
      return;
    }

    const newResults = results.map((item) => {
      const { media, description } = JSON.parse(item.description);

      const geolocation = JSON.parse(item.geolocation);

      return {
        _id: item._id,
        offer: item.offer,
        txid: item.txid,
        expires_in: item.expires_in,
        expires_on: item.expires_on,
        expired: item.expired,
        height: item.height,
        category: item.category,
        title: item.title,
        quantity: item.quantity,
        currency: item.currency,
        sysprice: item.sysprice,
        price: item.price,
        ismine: item.ismine,
        commission: item.commission,
        offerlink: item.offerlink,
        private: item.private,
        paymentoptions: item.paymentoptions,
        paymentoptions_display: item.paymentoptions_display,
        alias_peg: item.alias_peg,
        description,
        media,
        alias: item.alias,
        address: item.address,
        alias_rating_display: item.alias_rating_display,
        offers_sold: item.offers_sold,
        geolocation,
        alias_rating_count: item.alias_rating_count,
        alias_rating: item.alias_rating,
        safetylevel: item.safetylevel,
        safesearch: item.safesearch,
        offerlink_seller: item.offerlink_seller,
        offerlink_guid: item.offerlink_guid,
        time: item.time,
        cert: item.cert,
        __v: item.__v,
      };
    });

    if (title === 'ascending') {
      newResults.sort((a, b) => {
        const x = a.title.toLowerCase();
        const y = b.title.toLowerCase();
        if (x < y) {
          return -1;
        }
        if (x > y) {
          return 1;
        }
        return 0;
      });
      res.json(newResults);
      return;
    }
    if (title === 'descending') {
      newResults.sort((a, b) => {
        const x = a.title.toLowerCase();
        const y = b.title.toLowerCase();
        if (x < y) {
          return 1;
        }
        if (x > y) {
          return -1;
        }
        return 0;
      });
      res.json(newResults);
      return;
    }
    if (price === 'ascending') {
      newResults.sort((a, b) => a.sysprice - b.sysprice);
      res.json(newResults);
      return;
    }

    if (price === 'descending') {
      newResults.sort((a, b) => b.sysprice - a.sysprice);
      res.json(newResults);
      return;
    }
    res.json(newResults);
  });
});

router.get('/sync', (req, res, next) => {
  axios
    .get(
      'https://d2fzm6xoa70bg8.cloudfront.net/login?auth=e4031de36f45af2172fa8d0f054efcdd8d4dfd62',
    )
    .then((response) => {
      axios
        .get('https://d2fzm6xoa70bg8.cloudfront.net/offerfilter?', {
          headers: {
            Token: response.data.token,
          },
        })
        .then((results) => {
          const { data } = results;

          const items = data.map((item) => {
            OffersHelper.findOne(
              {
                offer: item.offer,
              },
              (err, result) => {
                if (err) {
                  return next(err);
                }
                if (result) {
                  return;
                }
                if (!result) {
                  const params = {
                    offer: item.offer,
                    cert: item.cert,
                    txid: item.txid,
                    expires_in: item.expires_in,
                    expires_on: item.expires_on,
                    expired: item.expired,
                    height: item.height,
                    time: item.time,
                    category: item.category,
                    title: item.title,
                    quantity: item.quantity,
                    currency: item.currency,
                    sysprice: item.sysprice,
                    price: item.price,
                    ismine: item.ismine,
                    commission: item.commission,
                    offerlink: item.offerlink,
                    offerlink_guid: item.offerlink_guid,
                    offerlink_seller: item.offerlink_seller,
                    private: item.private,
                    safesearch: item.safesearch,
                    safetylevel: item.safetylevel,
                    paymentoptions: item.paymentoptions,
                    paymentoptions_display: item.paymentoptions_display,
                    alias_peg: item.alias_peg,
                    description: item.description,
                    alias: item.alias,
                    address: item.address,
                    alias_rating: item.alias_rating,
                    alias_rating_count: item.alias_rating_count,
                    alias_rating_display: item.alias_rating_display,
                    geolocation: item.geolocation,
                    offers_sold: item.offers_sold,
                  };
                }
              },
            );
          });

          res.json(items);
        })
        .catch((err) => {
          console.error('err 2nd request', err);
          res.json({
            confirmation: 'fail after 2nd request',
            message: err,
          });
        });
    })
    .catch((err) => {
      console.error('err 1st request');
      res.json({
        confirmation: 'fail before 2nd request',
        message: err,
      });
    });
});

router.get('/search/:id', (req, res) => {
  const regx = {
    $regex: req.params.id,
  };

  const query = {
    $or: [
      {
        title: regx,
      },
      {
        description: regx,
      },
      {
        alias: regx,
      },
    ],
  };

  OffersHelper.find(query, (err, result) => {
    if (err) {
      res.json({
        confirmation: 'fail',
        message: err,
      });

      return;
    }

    res.json({
      confirmation: 'success',
      result,
    });
  });
});

module.exports = router;
