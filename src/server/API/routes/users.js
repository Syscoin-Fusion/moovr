import express from 'express';
import passport from 'passport';
import bcrypt from 'bcryptjs';

import { sysAliasNew, sysAliasInfo } from '../helpers/SysAliasHelper';
import { privatevalue, publicvalue } from '../aliasDefaultPubPrivValues';

const router = express.Router();
const controllers = require('../controllers');

/* User Register */

router.post('/register', (req, res) => {
  const walletPwd = 'password'; // this should be a Value passed in req.body
  const fromClientData = req.body;
  const newAlias = {
    aliasname: fromClientData.aliasname.toLowerCase(),
    password: fromClientData.password,
    publicvalue: publicvalue(fromClientData.aliasname),
    privatevalue: privatevalue(fromClientData.aliasname),
  };
  sysAliasNew(walletPwd, newAlias)
    .then((response) => {
      const alias = response.alias;
      // req.login logs user to session using passport
      req.login(alias, (err) => {
        if (err) {
          res.json({
            cofirmation: 'fail',
            message: err,
          });
          return;
        }
        res.json({
          confirmation: 'success',
          user: alias,
        });
      });
    })
    .catch((err) => {
      res.json({
        confirmation: 'fail',
        message: err,
      });
    });

});

/* User Login */
router.post('/login', (req, res) => {
  const aliasname = req.body.aliasname.toLowerCase();
  const password = req.body.password;
  sysAliasInfo(aliasname)
    .then((sysAliasInfo) => {
      const PswFromAlias = sysAliasInfo.password;
      /* console.log('ACZ (Psw From Alias)', sysAliasInfo); */
      if (bcrypt.compareSync(`${aliasname}${password}`, PswFromAlias)) {
        console.log('ACZ (compare succeed)');
        // passport.authenticate sets user to req.user session using strat.
        passport.authenticate('local', (err, user) => {
          if (err) {
            res.json({
              confirmation: 'fail',
              message: err,
            });
            return;
          }
          if (!user) {
            res.json({
              confirmation: 'fail',
              message: 'No user found by that name',
            });
            return;
          }
          req.logIn(user, (err2) => {
            console.log('ACZ (err2): ', err2);
            if (err2) {
              res.json({
                confirmation: 'fail',
                message: err2,
              });
              return;
            }
            // console.log('ACZ (SYS Info Login): ', sysAliasInfo);
            const newUserObj = {
              ...user,
              balance: sysAliasInfo.balance,
              address: sysAliasInfo.address,
            };
            res.json({
              confirmation: 'success',
              user: newUserObj,
            });
          });
        })(req, res);
      } else {
        res.json({
          confirmation: 'fail',
          message: 'Wrong credentials',
        });
      }
    })
    .catch(err =>
      res.json({
        confirmation: 'fail',
        message: err,
      }),
    );
});

/* User Logout */
router.get('/logout', (req, res) => {
  // req.logout uses passport to log out user
  req.logout();
  res.json({
    confirmation: 'success',
    user: null,
    message: 'User has been logged out',
  });
});

/* Get Current User */
router.get('/currentuser', (req, res) => {
  // req.isAuthenticated checks to see if user is in the session
  // if so you can access user by req.user
  if (req.isAuthenticated()) {
    res.json({
      confirmation: 'success',
      user: req.user,
    });
  } else {
    res.json({
      confirmation: 'NotLogged',
      user: null,
    });
  }
});

/* Get User by ID */
router.get('/:id', (req, res) => {
  controllers.user
    .getById(req.params.id)
    .then((result) => {
      res.json({
        confirmation: 'success',
        user: result,
      });
    })
    .catch((err) => {
      res.json({
        confirmation: 'fail',
        message: err,
      });
    });
});

module.exports = router;
