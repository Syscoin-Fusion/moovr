import axios from 'axios';

const express = require('express');

const router = express.Router();
const multerPkg = require('multer');
const multerS3 = require('multer-s3');
const aws = require('aws-sdk');

const s3Info = {
  accesskeyid: 'AKIAJKMOZEVFTR62XJ2Q',
  secretaccesskey: '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',
  bucket: 'shopshots',
  acl: 'public-read',
};

const s3 = new aws.S3({
  accessKeyId: s3Info.accesskeyid,
  secretAccessKey: s3Info.secretaccesskey,
});

const uploadS3 = multerPkg({
  storage: multerS3({
    s3,
    bucket: 'shopshots',
    acl: 'public-read',
    key(req, file, cb) {
      cb(null, `${Date.now().toString()}.jpg`);
    },
  }),
});

router.post('/create', uploadS3.array('photos', 3), (req, res) =>
  res.status(200).send(req.files[0]),
);

module.exports = router;
