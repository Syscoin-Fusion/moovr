import axios from 'axios';

const express = require('express');

const router = express.Router();
const exec = require('child-process-promise').exec;
const multerPkg = require('multer');
const fs = require('fs-extra');
const async = require('async');

const appPath = process.cwd();

const storage = multerPkg.diskStorage({
  destination(req, file, cb) {
    cb(null, './uploads');
  },
  filename(req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.webm`);
  },
});

const multer = multerPkg({ storage });
const subParser = require('subtitles-parser');
const CloudConverter = new (require('cloudconvert'))(
  'CsTDoO3rMuZvRcXrUbhzscq2eKmZv0NQot-DRKrsgnQ9OZbpsPb1CwBI14LRwuu7wjE3fKp_UXDJY50Cop5Ulg',
);

const s3Info = {
  accesskeyid: 'AKIAJKMOZEVFTR62XJ2Q',
  secretaccesskey: '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',
  bucket: 'shopshots',
  acl: 'public-read',
};
const awsFilePath = 'https://s3-us-west-2.amazonaws.com/shopshots/';

router.post(
  '/create',
  (req, res, next) => {
    fs.ensureDirSync(`${appPath}/uploads`);
    next();
  },
  multer.single('video'),
  (req, res) => {
    const mimetype =
      req.file.mimetype.indexOf('/') >= 0
        ? req.file.mimetype.split('/')[1].trim()
        : req.file.mimetype.trim();
    async.parallel(
      [
        function (done) {
          console.log('ACZ (TEST in the middle)');
          if (mimetype === 'quicktime') {
            const params = {
              localFile: `uploads/${req.file.filename}`,
              s3Params: {
                Bucket: s3Info.bucket,
                Key: `${req.file.filename.slice(0, -5)}.mp4`,
              },
            };
            const uploader = client.uploadFile(params);
            uploader.on('error', (err) => {
              console.error('unable to upload:', err.stack);
              return done(err.stack);
            });
            uploader.on('progress', () => {
              console.log(
                'progress',
                uploader.progressMd5Amount,
                uploader.progressAmount,
                uploader.progressTotal,
              );
            });
            uploader.on('end', () => {
              console.log('done uploading');
              return done(null);
            });
          } else {
            let format = '';
            if (mimetype == 'x-flv') format = 'flv';
            else if (mimetype == 'x-msvideo') return done('AVI is not supported');
            else if (mimetype == 'x-ms-wmv') format = 'wmv';
            else format = mimetype;
            console.log('Video Format:', format);
            console.log('Video Path:', req.file.path);
            CloudConverter.createProcess(
              {
                inputformat: format,
                outputformat: 'mp4',
              },
              (error, process) => {
                if (error) {
                  return done(error);
                }
                process.start({
                  input: 'upload',
                  file: fs.createReadStream(req.file.path),
                  outputformat: 'mp4',
                  output: { s3: s3Info },
                  converteroptions: {
                    video_codec: 'H264',
                    video_bitrate: 1024,
                    video_resolution: '640x480',
                    video_ratio: null,
                    video_fps: '',
                    audio_codec: 'AAC',
                    audio_bitrate: '128',
                    audio_channels: '',
                    audio_frequency: '44100',
                    trim_from: null,
                    trim_to: null,
                    audio_normalize: null,
                    video_crf: '23',
                    video_qscale: -1,
                    audio_qscale: -1,
                    faststart: null,
                    thumbnail_format: null,
                    thumbnail_size: null,
                    thumbnail_count: null,
                    video_bitrate_max: null,
                    video_profile: 'high',
                    video_profile_level: '3.0',
                    strip_metatags: null,
                    video_transpose: null,
                    command: null,
                    audio_all_streams: null,
                    subtitles_copy: null,
                    subtitles_srt: null,
                    subtitles_ass: null,
                    subtitles_srt_soft: null,
                    subtitles_ass_soft: null,
                  },
                });
                process.refresh();
                process.wait((err, proc) => {
                  if (err) {
                    return done(err);
                  }
                  return done(null, proc);
                });
              },
            );
          }
        },
        //        function (done) {
        //          exec(`autosub uploads/${req.file.filename} -F srt -S en -D en`)
        //            .then((out) => {
        //              fs.readFile(
        //                `./uploads/${req.file.filename.slice(0, -5)}.srt`,
        //                'utf-8',
        //                (err, data) => {
        //                  if (err) {
        //                    return done(err);
        //                  }
        //                  const subStr = subParser.fromStr(data);
        //                  return done(null, subStr);
        //                },
        //              );
        //            })
        //            .catch((err) => {
        //              console.log(err);
        //              return done(null, []);
        //            });
        //        },
      ],
      // Callback function
      (err, results) => {
        if (err && mimetype !== 'quicktime') {
          console.log('ACZ (Converting error): ', err.message);
          fs.unlink(req.file.path, (firstError) => {
            if (firstError) console.log('first delete:', firstError);
          });
          const message = err.message;
          /* fs.unlink(`${appPath}/uploads/${req.file.filename.slice(0, -5)}.srt`, (lastError) => {
            if (lastError) console.log('last delete:', lastError);
          }); */
          return res.status(500).send({
            success: false,
            message,
            status: 500,
          });
        }

        /* fs.unlink(req.file.path, (firstError) => {
          if (firstError) console.log('Video File delete:', firstError);
        }); */

        /* fs.unlink(`${appPath}/uploads/${req.file.filename.slice(0, -5)}.srt`, (lastError) => {
          if (lastError) console.log('Video Subtitle delete:', lastError);
        }); */
        console.log('ACZ (Checking result): ', results);
        res.status(200).send({
          success: true,
          data: {
            videoLink: `${awsFilePath + req.file.filename.slice(0, -5)}.mp4`,
            videoSubs: results[1] || '',
          },
        });
      },
    );
    /*
  axios
    .post('https://d3j22jloo6hpq6.cloudfront.net/API/parse', req.body)
    .then((response) => {
      res.json(response.data);
    })
    .catch((err) => {
      res.json(err);
    }); */
  },
);

module.exports = router;
