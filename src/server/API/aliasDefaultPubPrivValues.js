const publicvalue = alias => ({
  name: `${alias} Smith`,
  email: `${alias}_smith@moovr.com`,
  avatar:
    'https://www.dropbox.com/s/df1lm42dckdftkk/220px-Melopsittacus_undulatus_-Fort_Worth_Zoo-8a-4c.jpg?dl=0',
  currency: 'USD',
  about:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer luctus mi ut mi egestas, eget venenatis mi commodo. Curabitur iaculis eget metus ac tincidunt. Nulla lacinia dolor eu est aliquam, eget egestas ligula egestas. Etiam sit amet sem sem. Vivamus vitae bibendum diam. Maecenas ultrices ex eu placerat aliquam. Praesent.',
  address: 'At my sweety home',
});

const privatevalue = alias => ({
  shippingAddr: 'At my perfect office',
  phone: '555555555',
  social: {
    twitter: `@${alias}`,
    facebook: `${alias}_the_great`,
    youtube: `${alias}_filmmaker`,
  },
});

export { publicvalue, privatevalue };
