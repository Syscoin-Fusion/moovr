const bcrypt = require('bcryptjs');

const comparePassword = (password, user) =>
  new Promise((resolve, reject) => {
    bcrypt.compare(`${user.aliasname}${password}`, user.password, (err, isMatch) => {
      console.log('ACZ (compare succeed)');
      if (err) {
        reject(err);
        return;
      }
      if (isMatch === true) {
        resolve(user);
      } else {
        reject({ message: 'Wrong Password' });
      }
    });
  });

export { comparePassword };
