import { sysOfferInfo } from '../helpers/SysOffersHelper';
import OffersController from '../controllers/OffersController';
import tempOfferController from '../controllers/tempOfferController';

const checkTempOffers = () => {
  tempOfferController
    .find()
    .then((data) => {
      if (data.length === 0) {
        console.log('No tempOffers');
        return;
      }

      data.map((tempOffer) => {
        sysOfferInfo(tempOffer.offer)
          .then((res) => {
            OffersController.create(res, (err, offer) => {
              console.log('New Offer created: ', !!offer);

              tempOfferController
                .delete(tempOffer._id)
                .then((data) => {
                  console.log('Temp Offer Deleted: true');
                })
                .catch((err) => {
                  console.log('ERROR:', err);
                });
            });
          })
          .catch((err) => {
            console.log('ERROR: ', err);
          });
      });
    })
    .catch(err => err);
};

export { checkTempOffers };
