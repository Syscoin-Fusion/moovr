import bcrypt from 'bcryptjs';

import { sysNewAlias, sysInfoAlias } from '../controllers/SysAliasController';
// Helpers Functions

function smartParse(json, def) {
  try {
    return JSON.parse(json);
  } catch (e) {
    return def;
  }
}

const aliasParse = (sysAlias) => {
  const summary = {
    aliasname: sysAlias.name || sysAlias.aliasname,
    password: sysAlias.password,
    publicProfile: smartParse(sysAlias.value || sysAlias.publicvalue, ''),
    privateProfile: smartParse(sysAlias.privatevalue, ''),
  };

  return summary;
};

// const sysAliasNew = sysNewAlias;
const sysAliasNew = (walletPwd, newAlias) => {
  const aliasname = newAlias.aliasname;
  const aliaspsw = newAlias.password;
  const password = bcrypt.hashSync(`${aliasname}${aliaspsw}`, 10); // params.password;
  const publicvalue = JSON.stringify(newAlias.publicvalue);
  const privatevalue = JSON.stringify(newAlias.privatevalue);
  const alias = {
    aliasname,
    password,
    aliaspeg: 'sysrates.peg',
    publicvalue,
    privatevalue,
    safesearch: 'Yes',
    accepttransfers: 'Yes',
    expire: '31536000',
    nrequired: 0,
    aliases: [],
  };
  return sysNewAlias(walletPwd, alias)
    .then(res => ({
      sysRes: res,
      alias: aliasParse(alias),
    }))
    .catch((err) => {
      throw err.split('ERRCODE:')[1];
    });
};

/**
 * OLD Info
 */
// const sysAliasInfo = alias =>
//  sysInfoAlias(alias)
//    .then(res => res)
//    .catch((err) => {
//      throw err.split('ERRCODE:')[1];
//    });

const sysAliasInfo = alias =>
  sysInfoAlias(alias)
    .then(sysAlias => sysAlias)
    .then(sysAlias => aliasParse(sysAlias))
    .catch((err) => {
      throw err.split('ERRCODE:')[1];
    });

export { sysAliasNew, sysAliasInfo };
