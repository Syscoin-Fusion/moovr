import OffersController from '../controllers/OffersController';
import Promise from 'bluebird';
import bcrypt from 'bcryptjs';
import axios from 'axios';
import geolib from 'geolib';

const phraseToSign = process.env.PHRASE_TO_SIGN;

const isOffer = (id) => {
  OffersController.findById(id, (err, offer) => {
    if (err) {
      return err;
    }

    if (!offer) {
      return false;
    }

    if (offer) {
      return true;
    }
  });
};

function regexEscape(str) {
  return str.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

function reg(input) {
  let flags;
  // could be any combination of 'g', 'i', and 'm'
  flags = 'i';
  input = regexEscape(input);
  return new RegExp(input, flags);
}

function regexEscape(str) {
  return str.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

function reg(input) {
  let flags;
  // could be any combination of 'g', 'i', and 'm'
  flags = 'i';
  input = regexEscape(input);
  return new RegExp(input, flags);
}

function sortOffers(offers, sort) {
  switch (sort) {
    case 'Title: ascending':
      offers.sort((a, b) => a.title.localeCompare(b.title));
      return offers;

    case 'Title: descending':
      offers.sort((a, b) => b.title.localeCompare(a.title));
      return offers;

    case 'Price: ascending':
      offers.sort((a, b) => b.price - a.price);

      return offers;

    case 'Price: descending':
      offers.sort((a, b) => a.price - b.price);
      return offers;

    default:
      return offers;
  }
}

const offerFilter = (params) => {
  const {
    radius,
    itemPerPage,
    activePage,
    offerId,
    mediaType,
    paymentoptions_display,
    category,
    alias,
    belongsTo,
    sort,
    myPosLat,
    myPosLng,
    featured
  } = params;
  let updated = {};

  if (params.regExp.trim().length > 0) {
    const regex = reg(params.regExp);

    updated = {
      $or: [
        {
          title: regex,
        },
        {
          description: regex,
        },
        {
          alias: regex,
        },
      ],
    };
  } else {
    updated = {};
  }

  const paymentType = [];
  paymentType.push(params.paymentoptions_display);

  if (paymentoptions_display.length > 0) {
    updated.paymentoptions_display = new RegExp(paymentType.join('|'), 'i');
  }

  if(featured === true){
    updated.description = new RegExp('\\\"featured\\\":\\\"true\\\"')
  }

  if (mediaType !== 'All' && mediaType.length > 0) {
    updated.description = new RegExp(`\\\"mediaType\\\":\\\"${params.mediaType}\\\"`);
  }

  if(mediaType === 'txt'){
    updated.description = { $not: /^.*mediaType/ }
  }

  if (category !== 'All' && category.length > 0) {
    updated.category = new RegExp(params.category, 'i');
  }

  if (offerId.trim().length > 0) {
    if (offerId.trim().length === 16) {
      updated = {
        offer: offerId,
      };
    }
    if (offerId.trim().length === 24) {
      updated = {
        _id: offerId,
      };
    }
  }

  return new Promise((resolve, reject) => {
    OffersController.sort(updated, params.alias, (err, results) => {
      if (err) {
        reject(err);
        return;
      }

      if (offerId.trim().length > 0) {
        // check hash
        if (results.length === 1 && (results[0].hash !== undefined && results[0].hash !== null)) {
          resolve(results);
        }
        reject({ message: 'No hash for this offer' });
      }

      let data = [];
      results.map((result) => {
        // check hash
        if (result.hash !== null && result.hash !== undefined) {
          const isMatchHash = bcrypt.compareSync(phraseToSign, result.hash);
          if (isMatchHash === true) {
            if (result.geolocation.coords) {
              const { lat, lng } = result.geolocation.coords;
              if (lat.length === 0 || lng.length === 0) {
                return;
              }
              if (typeof lat && typeof lng === 'string') {
                result.geolocation.coords.lat = Number(lat);
                result.geolocation.coords.lng = Number(lng);
              }
              if (belongsTo !== undefined) {
                if (belongsTo === 'true' || 'false') {
                  if (`${result.belongsTo}` === belongsTo) {
                    data.push(result);
                    return;
                  }
                  return;
                }
                return;
              }
              data.push(result);
            }
          }
        }
      });

      if (radius > 0) {
        const newData = [];
        data.map((item) => {
          let itemGeolocation;
          if (item.geolocation) {
            itemGeolocation = item.geolocation.coords;
          }

          const currentLocation = {
            latitude: myPosLat,
            longitude: myPosLng,
          };
          const distanceArr = geolib.orderByDistance(currentLocation, [itemGeolocation]);
          const miles = (distanceArr[0].distance / 1609.34).toFixed(2);
          if (miles <= Number(radius)) {
            item.miles = miles;
            newData.push(item);
          }
        });
        sortOffers(newData, sort);
        resolve(newData);

        return;
      }

      if (itemPerPage > 0 && Number(radius) === 0) {
        const totalPages = Math.ceil(data.length / itemPerPage);
        const endNumber = itemPerPage * activePage;
        const startNumber = itemPerPage * activePage - itemPerPage;
        sortOffers(data, sort);
        data = data.slice(startNumber, endNumber);
        resolve({ data, totalPages });
        return;
      }

      sortOffers(data, sort);
      resolve(data);
    });
  });
};

export { offerFilter, isOffer };
