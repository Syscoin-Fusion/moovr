import { sysNewOffer, sysInfoOffer } from '../controllers/SysOffersController';

const sysOfferNew = sysNewOffer;
const sysOfferInfo = sysInfoOffer;

export { sysOfferNew, sysOfferInfo };
