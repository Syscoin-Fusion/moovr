const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const sessions = require('client-sessions');
const passport = require('passport');
const http = require('http');
const fs = require('fs-extra');
require('./utils/passport');
require('dotenv').config();

const config = require('./config/vars');

function normalizePort(val) {
  const normPort = parseInt(val, 10);

  if (isNaN(normPort)) {
    // named pipe
    return val;
  }

  if (normPort >= 0) {
    // port number
    return normPort;
  }

  return false;
}

const port = normalizePort(process.env.PORT || '3000');

mongoose.connect(config.MONGO_URI, (err) => {
  if (err) {
    console.log('');
    console.log('---------------------');
    console.log('DB CONNECTION FAILED');
    console.log('---------------------');
    console.log('');
  } else {
    console.log('');
    console.log('-----------------------');
    console.log(' DB CONNECTION SUCCESS');
    console.log(` Listening port:${port}`);
    console.log('-----------------------');
    console.log('');
  }
});

const offers = require('./API/routes/offers');
const index = require('./API/routes/index');
const users = require('./API/routes/users');
const images = require('./API/routes/images');
const videos = require('./API/routes/videos');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(favicon(path.join(__dirname, '../../build', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(
  sessions({
    cookieName: 'session',
    secret: process.env.SESSION_SECRET,
    duration: 24 * 60 * 60 * 1000,
    activeDuration: 30 * 60 * 1000,
  }),
);
// Passport
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, '../../build')));

app.use('/API/videos', videos);
app.use('/API/images', images);
app.use('/API/offers', offers);
app.use('/API/users', users);
app.use('/', index);

app.get('*', (req, res) => {
  res.render('index');
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

if (app.get('env') === 'development') {
  app.use((err, req, res) => {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res) => {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
  });
});

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

app.set('port', port);

const server = http.createServer(app);
server.listen(port);
server.on('error', onError);
server.on('listening', () => {
  if (app.get('env') === 'development') console.log(`Listening on ${port}`);
});

module.exports = app;
