const passport = require('passport');
const LocalStrat = require('passport-local').Strategy;

const controllers = require('../API/controllers');
const sysAliasInfo = require('../API/helpers/SysAliasHelper').sysAliasInfo;
const comparePassword = require('../API/helpers/UsersHelper').comparePassword;

passport.serializeUser((user, done) => {
  done(null, user.aliasname);
});

passport.deserializeUser((aliasname, done) => {
  sysAliasInfo(aliasname.toLowerCase())
    .then(authUser => done(null, authUser))
    .catch(err => done(err.message || err));
});

passport.use(
  new LocalStrat({ usernameField: 'aliasname' }, (aliasname, password, done) => {
    sysAliasInfo(aliasname.toLowerCase())
      .then(user => comparePassword(password, user))
      .then(authUser => done(null, authUser))
      .catch(err => done(err.message || err));
  }),
);