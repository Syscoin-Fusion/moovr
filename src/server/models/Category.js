const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    initial: true,
  },
  categoryDescription: {
    type: String,
    wysiwyg: true,
    initial: true,
  },
});

module.exports = mongoose.model('CategorySchema', CategorySchema);
