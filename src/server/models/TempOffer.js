const mongoose = require('mongoose');

const TempOfferSchema = new mongoose.Schema({
  offer: { type: String },
  txid: { type: String },
  title: { type: String },
  description: { type: String },
  category: { type: String },
  price: { type: String },
  currency: { type: String },
  quantity: { type: String },
  geolocation: { type: Object },
  media: { type: Object },
});

module.exports = mongoose.model('TempOffer', TempOfferSchema);
