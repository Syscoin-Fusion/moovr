const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  aliasname: { type: String, default: '', unique: true, dropDups: true },
  password: { type: String, default: '' },
  publicvalue: { type: Object, default: {} },
  privatevalue: { type: Object, default: {} },
  timestamp: { type: Date, default: Date.now },
});

function smartParse(json, def) {
  try {
    return JSON.parse(json);
  } catch (e) {
    return def;
  }
}

UserSchema.methods.summary = function () {
  const summary = {
    id: this._id.toString(),
    aliasname: this.aliasname,
    publicProfile: smartParse(this.publicvalue, ''),
    privateProfile: smartParse(this.privatevalue, ''),
    timestamp: this.timestamp,
  };

  return summary;
};

module.exports = mongoose.model('UserSchema', UserSchema);
