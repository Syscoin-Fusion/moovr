const mongoose = require('mongoose');

const SaleSchema = new mongoose.Schema({
  saleDate: {
    type: Date,
    require: true,
    initial: true,
    index: true,
    yearRange: [1990, 2099],
    default: Date.now,
  },
  items: {
    type: String,
    ref: 'StoreItem',
    many: true,
    required: true,
    initial: true,
  },
  quantity: {
    type: Number,
    require: true,
    initial: true,
  },
  seller: {
    type: String,
    ref: 'User',
    required: true,
    initial: true,
    index: true,
  },
  total: {
    type: Number,
    required: true,
    initial: true,
  },
  buyer: {
    type: String,
    ref: 'User',
    required: true,
    initial: true,
    index: true,
  },
});

module.exports = mongoose.model('SaleSchema', SaleSchema);
