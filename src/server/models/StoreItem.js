const mongoose = require('mongoose');

const StoreItemSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    initial: true,
    index: true,
  },
  publishedDate: {
    type: Date,
    default: Date.now(),
  },
  publisher: {
    type: String,
    ref: 'User',
    initial: true,
    required: true,
    index: true,
  },
  category: {
    type: String,
    ref: 'Category',
    initial: true,
    required: true,
    index: true,
  },
  price: {
    type: Number,
    required: true,
    initial: true,
    index: true,
  },
  currency: {
    type: String,
    options: 'BTC, USD, EUR',
    require: true,
    initial: true,
  },
  paymentOptions: {
    type: String,
    options: 'SYS',
    required: true,
    initial: true,
  },
  certificate: {
    type: Boolean,
  },
  itemDescription: {
    type: String,
    wysiwyg: true,
    initial: true,
  },
  productVideo: {
    type: String,
  },
});

module.exports = mongoose.model('StoreItemSchema', StoreItemSchema);
