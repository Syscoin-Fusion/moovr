import React from 'react';
import { IndexRoute, Router, Route, browserHistory } from 'react-router';
import { ReduxAsyncConnect } from 'redux-connect';

import App from './components/App';
import Test from './components/Test';
import Store from './components/Store';
import Offer from './components/Offer';
import BrowserPage from './components/Browser';
import ProfilePage from './components/Profile';
import { Register, Login } from './components/User';

const Routes = (
  <Router render={props => <ReduxAsyncConnect {...props} />} history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={BrowserPage.Browser} />
      <Route path="/test" component={Test} />
      <Route path="m/test" component={Test} />
      <Route path="/user/register" component={Register} />
      <Route path="m/user/register" component={Register} />
      <Route path="/user/login" component={Login} />
      <Route path="m/user/login" component={Login} />
      <Route path="/store/newItem" component={Store.SellPageSwitcher} />
      <Route path="m/store/newItem" component={Store.SellPageSwitcher} />
      <Route path="/profile" component={ProfilePage} />
      <Route path="m/profile" component={ProfilePage} />
      <Route path="/offer" component={Offer}>
        <Route path=":id" component={Offer} />
      </Route>
      <Route path="m/offer" component={Offer}>
        <Route path=":id" component={Offer} />
      </Route>
      <Route path="/message" component={BrowserPage.Message} />
      <Route path="m/message" component={BrowserPage.Message} />
    </Route>

    <Route path="/m" component={App}>
      <IndexRoute component={BrowserPage.Browser} />
      <Route path="/test" component={Test} />
      <Route path="m/test" component={Test} />
      <Route path="/user/register" component={Register} />
      <Route path="m/user/register" component={Register} />
      <Route path="/user/login" component={Login} />
      <Route path="m/user/login" component={Login} />
      <Route path="/store/newItem" component={Store.SellPageSwitcher} />
      <Route path="m/store/newItem" component={Store.SellPageSwitcher} />
      <Route path="/profile" component={ProfilePage} />
      <Route path="m/profile" component={ProfilePage} />
      <Route path="/offer" component={Offer}>
        <Route path=":id" component={Offer} />
      </Route>
      <Route path="m/offer" component={Offer}>
        <Route path=":id" component={Offer} />
      </Route>
      <Route path="/message" component={BrowserPage.Message} />
      <Route path="m/message" component={BrowserPage.Message} />
    </Route>
  </Router>
);

export default Routes;
