import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory, Router } from 'react-router';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore, compose } from 'redux';
import throttle from 'lodash/throttle';

import routes from './routes';
import reducers from './redux/reducers/index';
import middleware from './redux/middleware';

/* eslint-disable no-underscore-dangle */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, {}, composeEnhancers(middleware));
/* eslint-enable */

const component = (
  <Provider store={store}>
    <Router history={browserHistory}>{routes}</Router>
  </Provider>
);

ReactDOM.render(component, document.getElementById('react-view'));
