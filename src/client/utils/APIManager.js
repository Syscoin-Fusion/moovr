import axios from 'axios';
import Promise from 'bluebird';

export default {
  get: (url, params) =>
    new Promise((resolve, reject) => {
      axios
        .get(url, { params })
        .then((response) => {
          if (response.data.confirmation !== 'success') {
            throw new Error(response.data.message);
          }
          resolve(response.data);
        })
        .catch((err) => {
          reject(err || err.message);
        });
    }),

  post: (url, params) =>
    new Promise((resolve, reject) => {
      axios
        .post(url, params)
        .then((response) => {
          if (response.data.confirmation !== 'success') {
            throw new Error(response.data.message);
          }
          resolve(response.data);
        })
        .catch((err) => {
          reject(err || err.message);
        });
    }),

  put: (url, params) =>
    new Promise((resolve, reject) => {
      axios
        .put(url, params)
        .then((response) => {
          if (response.data.confirmation !== 'success') {
            throw new Error(response.data.message);
          }
          resolve(response.data);
        })
        .catch((err) => {
          reject(err || err.message);
        });
    }),

  delete: url =>
    new Promise((resolve, reject) => {
      axios
        .delete(url)
        .then((response) => {
          if (response.data.confirmation !== 'success') {
            throw new Error(response.data.message);
          }
          resolve(response.data);
        })
        .catch((err) => {
          reject(err || err.message);
        });
    }),
};
