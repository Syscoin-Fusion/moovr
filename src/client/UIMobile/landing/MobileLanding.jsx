import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { grey600, grey900, grey300 } from 'material-ui/styles/colors';
import { Link } from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Col, Button } from 'react-bootstrap';
import { connect } from 'react-redux';

import viewActions from '../../redux/actions/viewActions';
import './styles/MobileLanding.scss';

const styles = {
  button: {
    margin: 12,
  },
};

class MobileLanding extends Component {
  constructor(props) {
    super(props);

    this.changeView = this.changeView.bind(this);
  }

  changeView() {
    this.props.changeView('Map');
    document.getElementsByTagName('body')[0].className = 'map-view';
  }

  render() {
    const muiTheme = getMuiTheme({
      palette: {
        primary1Color: grey900,
        primary2Color: grey600,
        primary3Color: grey300,
        canvasColor: grey300,
      },
      appBar: {
        height: 70,
      },
    });
    return (
      <div>
        <MuiThemeProvider muiTheme={muiTheme}>
          <div className="row mobile_view">
            <Col xs={12}>
              <div className="logo_button">
                <span className="button_text">moovr</span>
              </div>
            </Col>
            <Col xs={12}>
              <Col xs={6}>
                <Link to="store/newItem">
                  <div className="record_button">
                    <span className="button_text">
                      <p>Record</p>
                      <p className="button_innerText">Sell Things</p>
                      <span className="underline" />
                    </span>
                  </div>
                </Link>
              </Col>
              <Col xs={6}>
                <Link to="/">
                  <div className="browse_button">
                    <span className="button_text">
                      <p>Browse</p>
                      <p className="button_innerText">Buy things</p>
                      <span className="underline" />
                    </span>
                  </div>
                </Link>
              </Col>
            </Col>
            <Col xs={12}>
              <Link to="/">
                <div onClick={this.changeView} className="find_button">
                  <span className="button_text">
                    <p>Find</p>
                    <p className="button_innerText">Nearby things</p>
                    <span className="underline" />
                  </span>
                </div>
              </Link>
            </Col>
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}

const stateToProps = state => ({});

const dispatchToProps = dispatch => ({
  changeView: view => dispatch(viewActions.changeView(view)),
});

export default connect(stateToProps, dispatchToProps)(MobileLanding);
