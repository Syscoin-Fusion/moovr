import constants from '../constants';

const initialState = {
  guid: null,
  txid: null,
  loading: false,
  error: null,
  success: false,
  message: '',
  showSnackbar: false,
  offer: {
    offer: null,
    txid: null,
    title: '',
    description: '',
    category: '',
    price: '',
    currency: '',
    quantity: '',
    geolocation: { coords: { lat: 0, lng: 0 } },
    media: {
      defaultIdx: 0,
      mediaVault: [],
    },
  },
};

export default function categoryReducer(state = initialState, action) {
  switch (action.type) {
    case constants.NEWOFFER_RESET:
      return {
        ...state,
        guid: null,
        txid: null,
        loading: false,
        error: null,
        success: false,
        message: '',
        showSnackbar: false,
        offer: {
          offer: null,
          txid: null,
          title: '',
          description: '',
          category: '',
          price: '',
          currency: '',
          quantity: '',
          geolocation: { coords: { lat: 0, lng: 0 } },
          media: {
            defaultIdx: 0,
            mediaVault: [],
          },
        },
      };

    case constants.NEWOFFER_START:
      return { ...state, loading: true };

    case constants.NEWOFFER_MEDIA_ADD: {
      const { offer } = state;
      const newMedia = { id: offer.media.mediaVault.length, ...action.data };
      offer.media.mediaVault.push(newMedia);
      return {
        ...state,
        loading: false,
        offer,
      };
    }

    case constants.NEWOFFER_ERR:
      return { ...state, error: true, loading: false, message: action.message, showSnackbar: true };

    case constants.NEWOFFER_SAVETEMP: {
      let { offer } = state;
      offer = {
        ...offer,
        title: action.data.title,
        description: action.data.description,
        category: action.data.category,
        price: action.data.price,
        currency: action.data.currency,
        quantity: action.data.quantity,
        geolocation: action.data.geolocation, // { coords: { lat: 0, lng: 0 } },
      };
      return {
        ...state,
        offer,
      };
    }

    case constants.NEWOFFER_SUCCESS: {
      let { offer } = state;
      offer = {
        ...offer,
        offer: action.guid,
        txid: action.txid,
      };
      return {
        ...state,
        guid: action.guid,
        txid: action.txid,
        offer,
        success: true,
        loading: false,
        error: false,
      };
    }

    case constants.SHOW_SNACKBAR:
      return { ...state, showSnackbar: false };

    default:
      return state;
  }
}
