import constants from '../constants';
import list from '../featuredOffers';

const initialState = {
  action: '', // Last action done
  succeed: true, // if the action succeed or not
  message: '', // message to show at succeed or error
  filter: {
    offerId: '', // if !== "" then override all following coditions
    regExp: '', // Search by this string, if === "" then search ALL
    myPosLat: null, // Lat position from browser
    myPosLng: null, // Lng position from browser
    radius: 0, // if !== 0 then override pagination
    belongsTo: null, // [ true | false | null ]
    category: 'All', // if === 'All' show all offers
    mediaType: '', // if "" return all types, [ vid | img | aud | txt | "" ]
    paymentoptions_display: '', // if "" return all types, [ SYS | BTC | ZEC ]
    itemPerPage: 0, // if === 0 then override pagination, the value are fixed by client
    activePage: 1, // active page
    totalPages: 1,
    sort: '',
    featured: true, // it can be true, false or null
  }, // the last filter sent to '/offerfilter'
  list, // the array returned by '/offerfilter'
};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.FEATURED_GET:
      return { ...state };
    case constants.FEATURED_SET:
      return { ...state };
    case constants.FEATURED_DEL:
      return { ...state };
    case constants.SORT_OFFERS:
      return { ...state };
    default:
      return state;
  }
};
