import constants from '../constants';

const initialState = {
  localUrl: null,
  url: null,
  error: null,
  recorded: false,
  loading: false,
  deleted: false,
  subtitles: [],
  videoDuration: 0,
  videoUploaded: false,
};

function setTimetoSeconds(value) {
  let output = 0;
  const match = value.match(/([0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3})/);

  if (match) {
    const valuee = value.split(':');
    const h = parseInt(valuee[0]) * 3600;
    const m = parseInt(valuee[1]) * 60;
    const s = parseFloat(valuee[2].replace(',', '.'));

    output = h + m + s;
  }
  return output;
}

function setFormatTime(arr) {
  const newArr = arr;
  newArr.map((a) => {
    a.startTime = setTimetoSeconds(a.startTime);
    a.endTime = setTimetoSeconds(a.endTime);
    return a;
  });
  return newArr;
}

const videoReducers = (state = initialState, action) => {
  switch (action.type) {
    case constants.SET_VIDEO_DURATION:
      return { ...state, videoDuration: action.duration };

    case constants.DELETE_RECORD:
      return { ...state, localUrl: null, recorded: false };

    case constants.UPLOAD_START:
      return { ...state, localUrl: action.url, loading: true };

    case constants.UPLOAD_ERROR:
      return { ...state, error: true, loading: false };

    case constants.UPLOAD_SUCCESS: {
      console.log('ACZ (reducer action):', action);
      return {
        ...state,
        url: action.payload.videoLink,
        recorded: true,
        error: false,
        loading: false,
        subtitles: setFormatTime(action.payload.videoSubs),
      };
    }

    case constants.SET_OFFER:
      return { ...state, videoUploaded: true, recorded: false };

    case constants.UPDATE_SUBTITLES:
      return { ...state, subtitles: action.subtitles };

    default:
      return state;
  }
};

export default videoReducers;
