import { combineReducers } from 'redux';
import { reducer as reduxAsyncConnect } from 'redux-connect';
import { reducer as reduxForm } from 'redux-form';

import appReducer from './appReducer';
// import storeReducers from './store';
import categoryReducers from './categoryReducers';
import currencyReducers from './currencyReducers';
import videoReducers from './videoReducer';
import browserReducers from './browser';
import imageReducers from './imageReducers';
import userReducer from './userReducer';
import offerReducer from './offerReducer';
import newOfferReducer from './newOfferReducer';
import featuredReducer from './featuredReducer';
import viewReducer from './viewReducer';

export default combineReducers({
  // categories: storeReducers.categoryReducer,
  // currencies: storeReducers.currencyReducer,
  reduxAsyncConnect,
  app: appReducer,
  categories: categoryReducers,
  currencies: currencyReducers,
  newOffer: newOfferReducer,
  video: videoReducers,
  browser: browserReducers,
  image: imageReducers,
  form: reduxForm,
  account: userReducer,
  offers: offerReducer,
  featured: featuredReducer,
  view: viewReducer,
});
