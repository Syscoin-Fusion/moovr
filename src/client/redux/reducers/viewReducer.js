import constants from '../constants';

const initialState = {
  layout: 'Grid',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.VIEW_CHANGED:
      return { ...state, layout: action.data };
    default:
      return { ...state };
  }
};
