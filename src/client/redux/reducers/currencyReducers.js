import {
  CURRENCY_REQ_ERR,
  CURRENCY_REQ_START,
  CURRENCY_REQ_SUCCESS,
} from '../actions/currencyActions';

const initialState = {
  loading: false,
  error: null,
  success: false,
  rates: [{ currency: 'SYS' }, { currency: 'BTC' }, { currency: 'ZEC' }],
};

export default function currencyReducer(state = initialState, action) {
  switch (action.type) {
    case CURRENCY_REQ_START:
      return { ...state, loading: true };
    case CURRENCY_REQ_ERR:
      return { ...state, error: true, loading: false };
    case CURRENCY_REQ_SUCCESS:
      return { ...state, success: true, loading: false, rates: action.payload };

    default:
      return state;
  }
}
