import constants from '../constants';

const initialState = {
  action: '', // Last action done
  succeed: true, // if the action succeed or not
  message: '', // message to show at succeed or error
  filter: {
    offerId: '', // if !== "" then override all following coditions
    regExp: '', // Search by this string, if === "" then search ALL
    myPosLat: null, // Lat position from browser
    myPosLng: null, // Lng position from browser
    radius: 0, // if !== 0 then override pagination
    belongsTo: null, // [ true | false | null ]
    category: 'All', // if === 'All' show all offers
    mediaType: '', // if "" return all types, [ vid | img | aud | txt | "" ]
    paymentoptions_display: '', // if "" return all types, [ SYS | BTC | ZEC ]
    itemPerPage: 10, // if === 0 then override pagination, the value are fixed by client
    activePage: 1, // active page
    totalPages: 1,
    sort: '',
    featured: false,
  }, // the last filter sent to '/offerfilter'
  list: [], // the array returned by '/offerfilter'
};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.CREATE_OFFER:
      return {
        ...state,
        list: state.list.concat(action.payload),
        [action.payload.id]: action.payload,
      };

    case constants.GET_OFFERS:
      return { ...state, list: action.payload };

    case constants.GET_OFFER:
      return { ...state, [action.payload.id]: action.payload };

    case constants.EDIT_OFFER:
      return {
        ...state,
        [action.payload.id]: action.payload,
        list: state.list.map(item => (item.id === action.payload.id ? action.payload : item)),
      };

    case constants.DELETE_OFFER: {
      const newState = {
        ...state,
        list: state.list.filter(item => item.id !== action.payload.id),
      };
      delete newState[action.payload.id];

      return newState;
    }

    case constants.SORT_OFFERS:
      return {
        ...state,
        list: action.payload,
        filter: {
          ...action.params,
          totalPages: action.totalPages,
        },
      };

    default:
      return state;
  }
};
