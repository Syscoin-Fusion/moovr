import constants from '../constants';
import userOffers from '../userOffers';

const initialState = {
  user: null,
  userOffers,
};

export default (state = initialState, action) => {
  const newState = { ...state };

  switch (action.type) {
    case constants.USER_CREATED:
      newState.user = action.payload;
      return newState;

    case constants.USER_LOGIN:
      newState.user = action.payload;
      return newState;

    case constants.USER_IS_CURRENT_USER:
      newState.user = action.payload;
      return newState;

    case constants.USER_RECEIVED:
      newState[action.payload.id] = action.payload;
      return newState;

    case constants.USER_LOGOUT:
      newState.user = action.payload;
      return newState;

    default:
      return state;
  }
};
