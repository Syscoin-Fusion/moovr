import APIManager from '../../utils';

import constants from '../constants';

const getRequest = (path, params, actionType) => dispatch =>
  APIManager.get(path, params)
    .then((response) => {
      const payload = response.results || response.result || response.user;
      dispatch({
        type: actionType,
        payload,
        params,
      });

      return payload;
    })
    .catch((err) => {
      throw err;
    });

const postRequest = (path, params, actionType) => (dispatch) => {
  dispatch({ type: 'LOADING', appLoadingGlobal: true });
  return APIManager.post(path, params)
    .then((response) => {
      const payload = response.results || response.result || response.user;
      dispatch({
        type: actionType,
        appLoadingGlobal: false,
        payload,
        params,
      });

      return payload;
    })
    .catch((err) => {
      dispatch({ type: 'LOADING', appLoadingGlobal: false });
      throw err;
    });
};

export default {
  checkCurrentUser: () => dispatch =>
    dispatch(getRequest('/API/users/currentuser', null, constants.USER_IS_CURRENT_USER)),

  register: credentials => dispatch =>
    dispatch(postRequest('/API/users/register', credentials, constants.USER_CREATED)),

  login: credentials => dispatch =>
    dispatch(postRequest('/API/users/login', credentials, constants.USER_LOGIN)),

  fetchUser: id => dispatch =>
    dispatch(getRequest(`/API/users/${id}`, {}, constants.USER_RECEIVED)),

  logout: () => dispatch => dispatch(getRequest('/API/users/logout', {}, constants.USER_LOGOUT)),
};
