import constants from '../constants';

export const appLoadingGlobal = data => dispatch =>
  dispatch({
    type: constants.APP_LOADING_GLOBAL,
    globaloader: data,
  });
