import 'whatwg-fetch';
import axios from 'axios';

import constants from '../constants';

function uploadStart(payload) {
  return {
    type: constants.UPLOAD_START,
    appLoadingGlobal: true,
    url: payload,
  };
}

function uploadError(payload) {
debugger
  return {
    type: constants.UPLOAD_ERROR,
    appLoadingGlobal: false,
    payload: payload?payload.data: null,
  };
}

function uploadSuccess(payload) {
  return {
    type: constants.UPLOAD_SUCCESS,
    appLoadingGlobal: false,
    payload: payload.data,
  };
}

export function setDuration(payload) {
  return {
    type: constants.SET_VIDEO_DURATION,
    duration: payload,
  };
}

export function deleteRecord(payload) {
  return (dispatch) => {
    dispatch({
      type: constants.DELETE_RECORD,
      deleted: payload,
    });
  };
}

export function setOfferForm() {
  return (dispatch) => {
    dispatch({
      type: constants.SET_OFFER,
    });
  };
}

export function updateSubtitles(subtitles) {
  return (dispatch) => {
    dispatch({
      type: constants.UPDATE_SUBTITLES,
      subtitles,
    });
  };
}

// https://d3j22jloo6hpq6.cloudfront.net/API/parse
// https://shopshots-argvil19.c9users.io/API/parse

export function setRecord(data, url) {
  return (dispatch) => {
    dispatch(uploadStart(url));
    // This is a hack to check if data is the FormData which composed from formdata-polyfill
    // If yes, convert it to native FormData

    const nativeData = data._asNative ? data._asNative() : data;
    axios
      .post('/API/videos/create', nativeData)
      .then((response) => {
        // console.log('ACZ (Video Then response): ', response);
        if (!response.data.success) {
          dispatch(uploadError(response));
        } else {
          dispatch(uploadSuccess(response.data));
        }
      })
      .catch((err) => {
        dispatch(uploadError());
      });
  };
}

export function parseVideo(data, url) {
  return (dispatch) => {
    dispatch(uploadStart(url));
    const nativeData = data._asNative ? data._asNative() : data;
    axios
      .post('https://d3j22jloo6hpq6.cloudfront.net/API/parse', nativeData)
      .then((response) => {
        console.log('ACZ (Video ".then" response from cloudfrom): ', response);
        if (!response.data.success) {
          dispatch(uploadError(response));
        } else {
          dispatch(uploadSuccess(response.data));
        }
      })
      .catch((err) => {
        console.log('ACZ (Video ".catch" response from cloudfrom): ', err);
        dispatch(uploadError());
      });
  };
}
