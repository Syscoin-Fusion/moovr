import APIManager from '../../utils';
import constants from '../constants';

const getRequest = (path, params, actionType) => (dispatch) => {
  dispatch({ type: 'LOADING', appLoadingGlobal: true });
  return APIManager.get(path, params)
    .then((response) => {
      const payload = response.results || response.result || response.user;
      const totalPages = response.totalPages || 0;
      dispatch({
        type: actionType,
        appLoadingGlobal: false,
        payload,
        params,
        totalPages,
      });

      return payload;
    })
    .catch((err) => {
      dispatch({ type: 'LOADING', appLoadingGlobal: false });
      throw err;
    });
};

const postRequest = (path, params, actionType) => (dispatch) => {
  dispatch({ type: 'LOADING', appLoadingGlobal: true });
  APIManager.post(path, params)
    .then((response) => {
      const payload = response.results || response.result || response.user;
      dispatch({
        type: actionType,
        appLoadingGlobal: false,
        payload,
        params,
      });

      return payload;
    })
    .catch((err) => {
      dispatch({ type: 'LOADING', appLoadingGlobal: false });
      throw err;
    });
};

const putRequest = (path, params, actionType) => (dispatch) => {
  dispatch({ type: 'LOADING', appLoadingGlobal: true });
  APIManager.put(path, params)
    .then((response) => {
      const payload = response.results || response.result || response.user;
      dispatch({
        type: actionType,
        appLoadingGlobal: false,
        payload,
        params,
      });

      return payload;
    })
    .catch((err) => {
      dispatch({ type: 'LOADING', appLoadingGlobal: false });
      throw err;
    });
};

const deleteRequest = (path, actionType) => (dispatch) => {
  dispatch({ type: 'LOADING', appLoadingGlobal: true });
  APIManager.delete(path)
    .then((response) => {
      const payload = response.results || response.result || response.user;
      dispatch({
        type: actionType,
        appLoadingGlobal: false,
        payload,
      });

      return payload;
    })
    .catch((err) => {
      dispatch({ type: 'LOADING', appLoadingGlobal: false });
      throw err;
    });
};

export default {
  getOffers: params => dispatch =>
    dispatch(getRequest('/API/offers', params, constants.GET_OFFERS)),

  sortOffers: params => dispatch =>
    dispatch(getRequest('/API/offers/offerfilter', params, constants.SORT_OFFERS)),

  getOffer: id => dispatch => dispatch(getRequest(`/API/offers/${id}`, null, constants.GET_OFFER)),

  createOffer: params => dispatch =>
    dispatch(postRequest('/API/offers/new', params, constants.CREATE_OFFER)),

  editOffer: (id, params) => dispatch =>
    dispatch(putRequest(`/API/offers/edit/${id}`, params, constants.EDIT_OFFER)),

  deleteOffer: id => dispatch =>
    dispatch(deleteRequest(`/API/offers/delete/${id}`, constants.DELETE_OFFER)),
};
