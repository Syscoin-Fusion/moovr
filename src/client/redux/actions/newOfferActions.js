import 'whatwg-fetch';
import axios from 'axios';

import constants from '../constants';

const itemCreateStart = () => ({
  type: constants.NEWOFFER_START,
  appLoadingGlobal: true,
});

function itemCreateErr(payload) {
  return {
    type: constants.NEWOFFER_ERR,
    appLoadingGlobal: false,
    message: payload,
  };
}

function itemCreateSuccess(payload) {
  return {
    type: constants.NEWOFFER_SUCCESS,
    appLoadingGlobal: false,
    txid: payload.response[0],
    guid: payload.response[1],
  };
}

export function showSnackbar() {
  return {
    type: constants.SHOW_SNACKBAR,
    appLoadingGlobal: false,
  };
}

export function newOfferAddMedia(data) {
  return {
    type: constants.NEWOFFER_MEDIA_ADD,
    data,
  };
}

export function newOfferSaveTemp(data) {
  return {
    type: constants.NEWOFFER_SAVETEMP,
    data,
  };
}

export function doItemCreate(params) {
  return (dispatch) => {
    dispatch(itemCreateStart());
    axios
      .post('/API/offers/new', params)
      .then((response) => {
        if (typeof response.data !== 'string') {
          dispatch(itemCreateSuccess(response.data));
        } else {
          dispatch(itemCreateErr(response.data));
        }
      })
      .catch((error) => {
        dispatch(itemCreateErr(error));
      });
  };
}

export function newOfferReset() {
  return {
    type: constants.NEWOFFER_RESET,
  };
}
