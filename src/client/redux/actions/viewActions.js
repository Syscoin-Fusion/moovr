import constants from '../constants';

export default {
  changeView: (view) => {
    return {
      type: constants.VIEW_CHANGED,
      data: view
    }
  }
}
