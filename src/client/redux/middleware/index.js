import { applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';

/* import checkSession from './checkSession'; */
import checkLoading from './checkLoading';

export default applyMiddleware(
  /* checkSession, */
  checkLoading,
  reduxThunk,
);
