import { appLoadingGlobal } from '../actions/appActions';

const checkLoading = store => next => (action) => {
  if (action.appLoadingGlobal !== undefined) {
    store.dispatch(appLoadingGlobal(action.appLoadingGlobal));
  }
  const result = next(action);
  return result;
};

export default checkLoading;
