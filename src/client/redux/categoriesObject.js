const categories = [
  {
    text: 'All',
    cat: 'All',
    icon: '',
  },
  {
    text: 'For Sale',
    cat: 'For Sale',
    icon: '',
    items: [
      {
        text: 'All',
        cat: 'For Sale',
      },
      {
        text: 'Auto/Vehicle',
        cat: 'For Sale > Auto/Vehicle',
        icon: '',
        items: [
          {
            text: 'All',
            cat: 'For Sale > Auto/Vehicle',
            icon: '',
          },
          {
            text: 'ATVs/UTV/Snow',
            cat: 'For Sale > Auto/Vehicle > ATVs/UTV/Snow',
            icon: '',
          },
          {
            text: 'Boats',
            cat: 'For Sale > Auto/Vehicle > Boats',
            icon: '',
          },
          {
            text: 'Cars/Trucks',
            cat: 'For Sale > Auto/Vehicle > Cars/Trucks',
            icon: '',
          },
          {
            text: 'Motorcycles',
            cat: 'For Sale > Auto/Vehicle > Motorcycles',
            icon: '',
          },
          {
            text: 'RVs/campers',
            cat: 'For Sale > Auto/Vehicle > RVs/campers',
            icon: '',
          },
        ],
      },
      {
        text: 'Alternatives',
        cat: 'For Sale > Alternatives',
        icon: '',
      },
      {
        text: 'Antiques',
        cat: 'For Sale > Antiques',
        icon: '',
      },
      {
        text: 'Appliances',
        cat: 'For Sale > Appliances',
        icon: '',
      },
      {
        text: 'Arts/Crafts',
        cat: 'For Sale > Arts/Crafts',
        icon: '',
      },
      {
        text: 'Baby/Kid',
        cat: 'For Sale > Baby/Kid',
        icon: '',
      },
      {
        text: 'Beauty/Health',
        cat: 'For Sale > Beauty/Health',
        icon: '',
      },
      {
        text: 'Books',
        cat: 'For Sale > Books',
        icon: '',
      },
      {
        text: 'Bicycles',
        cat: 'For Sale > Bicycles',
        icon: '',
      },
      {
        text: 'Business',
        cat: 'For Sale > Business',
        icon: '',
      },
      {
        text: 'CDs/DVDs/VHS',
        cat: 'For Sale > CDs/DVDs/VHS',
        icon: '',
      },
      {
        text: 'Clothing/Accessories',
        cat: 'For Sale > Clothing/Accessories',
        icon: '',
      },
      {
        text: 'Collectibles',
        cat: 'For Sale > Collectibles',
        icon: '',
      },
      {
        text: 'Cryptocurrency',
        cat: 'For Sale > Cryptocurrency',
        icon: '',
        items: [
          {
            text: 'All',
            cat: 'For Sale > Cryptocurrency',
          },
          {
            text: 'BTC',
            cat: 'For Sale > Cryptocurrency > BTC',
            icon: '',
            items: [
              {
                text: 'All',
                cat: 'For Sale > Cryptocurrency > BTC',
              },
              {
                text: 'Cash',
                cat: 'For Sale > Cryptocurrency > BTC > Cash',
                icon: '',
              },
              {
                text: 'Gift Card',
                cat: 'For Sale > Cryptocurrency > BTC > Gift Card',
                icon: '',
              },
              {
                text: 'Interac',
                cat: 'For Sale > Cryptocurrency > BTC > Interac',
                icon: '',
              },
              {
                text: 'Bank Transfer',
                cat: 'For Sale > Cryptocurrency > BTC > Bank Transfer',
                icon: '',
              },
            ],
          },
          {
            text: 'SYS',
            cat: 'For Sale > Cryptocurrency > SYS',
            icon: '',
            items: [
              {
                text: 'All',
                cat: 'For Sale > Cryptocurrency > SYS',
              },
              {
                text: 'Cash',
                cat: 'For Sale > Cryptocurrency > SYS > Cash',
                icon: '',
              },
              {
                text: 'Gift Card',
                cat: 'For Sale > Cryptocurrency > SYS > Gift Card',
                icon: '',
              },
              {
                text: 'Interac',
                cat: 'For Sale > Cryptocurrency > SYS > Interac',
                icon: '',
              },
              {
                text: 'Bank Transfer',
                cat: 'For Sale > Cryptocurrency > SYS > Bank Transfer',
                icon: '',
              },
            ],
          },
        ],
      },
      {
        text: 'Electronics',
        cat: 'For Sale > Electronics',
        icon: '',
      },
      {
        text: 'Food&Drink',
        cat: 'For Sale > Food&Drink',
        icon: '',
      },
      {
        text: 'Furniture',
        cat: 'For Sale > Furniture',
        icon: '',
      },
      {
        text: 'Jewelry',
        cat: 'For Sale > Jewelry',
        icon: '',
      },
      {
        text: 'Medicinal',
        cat: 'For Sale > Medicinal',
        icon: '',
      },
      {
        text: 'Musical Instruments',
        cat: 'For Sale > Musical Instruments',
        icon: '',
      },
      {
        text: 'Outdoors',
        cat: 'For Sale > Outdoors',
        icon: '',
        items: [
          {
            text: 'BBQs/Grills',
            cat: 'For Sale > Outdoors',
          },
          {
            text: 'BBQs/Grills',
            cat: 'For Sale > Outdoors > BBQs/Grills',
            icon: '',
          },
          {
            text: 'Gardening',
            cat: 'For Sale > Outdoors > Gardening',
            icon: '',
          },
          {
            text: 'Lawnnmowers',
            cat: 'For Sale > Outdoors > Lawnnmowers',
            icon: '',
          },
        ],
      },
      {
        text: 'Photo/Video',
        cat: 'For Sale > Photo/Video',
        icon: '',
      },
      {
        text: 'Tickets',
        cat: 'For Sale > Tickets',
        icon: '',
      },
      {
        text: 'Tools/Power Equipment',
        cat: 'For Sale > Tools/Power Equipment',
        icon: '',
      },
      {
        text: 'Toys/Games',
        cat: 'For Sale > Toys/Games',
        icon: '',
      },
    ],
  },
  {
    text: 'Services',
    cat: 'Services',
    icon: '',
    items: [
      {
        text: 'All',
        cat: 'Services',
      },
      {
        text: 'Art/Media/Design',
        cat: 'Services > Art/Media/Design',
        icon: '',
      },
      {
        text: 'Legal/Paralegal',
        cat: 'Services > Legal/Paralegal',
        icon: '',
      },
      {
        text: 'General Labor',
        cat: 'Services > General Labor',
        icon: '',
      },
      {
        text: 'Manufacturing',
        cat: 'Services > Manufacturing',
        icon: '',
      },
      {
        text: 'Marketing/PR/Ad',
        cat: 'Services > Marketing/PR/Ad',
        icon: '',
      },
      {
        text: 'Software/Systems',
        cat: 'Services > Software/Systems',
        icon: '',
      },
      {
        text: 'TV/Film/Video',
        cat: 'Services > TV/Film/Video',
        icon: '',
      },
      {
        text: 'Writing/Editing',
        cat: 'Services > Writing/Editing',
        icon: '',
      },
      {
        text: 'Other',
        cat: 'Services > Other',
        icon: '',
      },
    ],
  },
  {
    text: 'Wanted',
    cat: 'Wanted',
    icon: '',
    items: [
      {
        text: 'All',
        cat: 'Wanted',
      },
      {
        text: 'Cryptocurrency',
        cat: 'Wanted > Cryptocurrency',
        icon: '',
        items: [
          {
            text: 'All',
            cat: 'Wanted > Cryptocurrency',
          },
          {
            text: 'BTC',
            cat: 'Wanted > Cryptocurrency > BTC',
            icon: '',
            items: [
              {
                text: 'All',
                cat: 'Wanted > Cryptocurrency > BTC',
              },
              {
                text: 'Cash',
                cat: 'Wanted > Cryptocurrency > BTC > Cash',
                icon: '',
              },
              {
                text: 'Gift Card',
                cat: 'Wanted > Cryptocurrency > BTC > Gift Card',
                icon: '',
              },
              {
                text: 'Interac',
                cat: 'Wanted > Cryptocurrency > BTC > Interac',
                icon: '',
              },
              {
                text: 'Bank Transfer',
                cat: 'Wanted > Cryptocurrency > BTC > Bank Transfer',
                icon: '',
              },
            ],
          },
          {
            text: 'SYS',
            cat: 'Wanted > Cryptocurrency > SYS',
            icon: '',
            items: [
              {
                text: 'All',
                cat: 'Wanted > Cryptocurrency > SYS',
              },
              {
                text: 'Cash',
                cat: 'Wanted > Cryptocurrency > SYS > Cash',
                icon: '',
              },
              {
                text: 'Gift Card',
                cat: 'Wanted > Cryptocurrency > SYS > Gift Card',
                icon: '',
              },
              {
                text: 'Interac',
                cat: 'Wanted > Cryptocurrency > SYS > Interac',
                icon: '',
              },
              {
                text: 'Bank Transfer',
                cat: 'Wanted > Cryptocurrency > SYS > Bank Transfer',
                icon: '',
              },
            ],
          },
        ],
      },
    ],
  },
  {
    text: 'Certificates',
    cat: 'Certificates',
    icon: '',
    items: [
      {
        text: 'All',
        cat: 'Certificates',
      },
      {
        text: 'Digital Downloads',
        cat: 'Certificates > Digital Downloads',
        icon: '',
      },
      {
        text: 'Software',
        cat: 'Certificates > Software',
        icon: '',
      },
      {
        text: 'General Labor',
        cat: 'Certificates > General Labor',
        icon: '',
      },
      {
        text: 'Game Codes',
        cat: 'Certificates > Game Codes',
        icon: '',
      },
      {
        text: 'Company Shares',
        cat: 'Certificates > Company Shares',
        icon: '',
      },
      {
        text: 'Gift Cards',
        cat: 'Certificates > Gift Cards',
        icon: '',
      },
    ],
  },
];

export default categories;
