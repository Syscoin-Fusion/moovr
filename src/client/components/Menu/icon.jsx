
import React from "react"


export default class SSIcon extends React.Component {
    render(){
      return (
      <svg xmlns="http://www.w3.org/2000/svg" xmlSpace="preserve" width="40px" height="40px" version="1.1" viewBox="0 0 68 68" xmlnsXlink="http://www.w3.org/1999/xlink">
      <g id="Layer_x0020_1">
        <metadata id="CorelCorpID_0Corel-Layer"/>
        <rect style={{fill: '#64F000'}} transform="matrix(0.402255 0.402255 -0.402255 0.402255 34.1118 -5.95524)" width="99.6065" height="99.6065" rx="25" ry="25"/>
        <path style={{fill:'white', fillRule: 'nonzero'}} d="M38 30l0 16 -7 0 0 -12c0,-1 -1,-2 -1,-2 0,-1 0,-2 0,-2 0,-1 -1,-1 -1,-1 -1,0 -1,-1 -2,-1 -1,0 -2,1 -2,1 -1,0 -1,0 -2,1l0 16 -7 0 0 -23 7 0 0 3c1,-1 2,-2 4,-3 1,0 2,0 3,0 2,0 3,0 4,1 1,0 2,1 3,3 1,-2 3,-3 4,-3 1,-1 3,-1 4,-1 2,0 4,0 5,2 2,1 2,3 2,6l0 15 -7 0 0 -12c0,-1 0,-2 0,-2 0,-1 0,-2 0,-2 -1,-1 -1,-1 -1,-1 -1,0 -2,-1 -2,-1 -1,0 -2,1 -2,1 -1,0 -1,0 -2,1z"/>
      </g>
      </svg>
      );
    }
}
