import React from 'react';
import { Link, browserHistory } from 'react-router';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import { ListItem } from 'material-ui/List';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import swal from 'sweetalert2';
import Badge from 'material-ui/Badge';
import IconMenu from 'material-ui/IconMenu';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import ToolTip from 'react-portal-tooltip';

import FontIcon from 'material-ui/FontIcon';
import { Col } from 'react-bootstrap';

import userActions from '../../redux/actions/userActions';
import { doCategoryReq } from '../../redux/actions/categoryActions';
// import { doLogout } from '../../redux/actions/userSession';
import SSIcon from './icon';
import { Login, Register } from '../User';
import offerActions from '../../redux/actions/offerActions';

import './styles/MenuBrowser.scss';

const styles = {
  viewOfferBtn: {
    height: '25px',
  },
  viewOfferLbl: {
    fontSize: '0.8em',
    top: '4px',
  },
  viewOfferIcon: {
    fontSize: '1.5em',
  },
  button: {
    margin: 12,
    width: '170px',
    backgroundColor: 'none',
    borderRadius: '7px',
    // height: '41px',
  },
  aboutIcon: {
    top: '5px',
    float: 'left',
    fontSize: '34px',
    marginRight: '12px',
  },
  loginIcon: {
    float: 'left',
    fontSize: '33px',
    marginRight: '5px',
    marginLeft: '8px',
  },
  customContentStyle: {
    maxWidth: '380px',
    padding: '0px',
    margin: '0 auto',
  },
  bodyStyle: {
    padding: '0px',
  },
};

class MenuBrowser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      regexp: '',
      safesearch: 'No',
      category: null,
      loginModal: false,
      registerModal: false,
      openDropDown: false,
      isTooltipActive: false,
    };

    this.props.getCategories();
    this.handleToggle = this.handleToggle.bind(this);
    this.showTooltip = this.showTooltip.bind(this);
    this.hideTooltip = this.hideTooltip.bind(this);
    this.loginModalOpen = this.loginModalOpen.bind(this);
    this.loginModalClose = this.loginModalClose.bind(this);
    this.registerModalOpen = this.registerModalOpen.bind(this);
    this.registerModalClose = this.registerModalClose.bind(this);
    this.toggleLoginRegister = this.toggleLoginRegister.bind(this);
    this.logout = this.logout.bind(this);
    this.login = this.login.bind(this);
    this.register = this.register.bind(this);
    this.handleCategoryFilter = this.handleCategoryFilter.bind(this);
  }

  componentDidMount() {
    this.props
      .checkCurrentUser()
      .then(() => {})
      .catch((err) => {});
    const data = {
      regexp: this.props.searchData,
      from: '',
      safesearch: 'No',
      category: null,
    };
    swal.setDefaults({
      /* confirmButtonColor: '#55b82e', */
      buttonsStyling: false,
      confirmButtonClass: 'MenuBrowser__swalBottom',
    });
  }

  handleCategoryFilter(category) {
    const { filter } = this.props.offers;
    filter.category = category;
    this.handleToggle();
    this.props.sortOffersNew(filter);
    browserHistory.push('/');
  }

  loginModalOpen() {
    this.setState({
      loginModal: true,
    });
  }

  loginModalClose() {
    this.setState({
      loginModal: false,
    });
  }

  registerModalOpen() {
    this.setState({
      registerModal: true,
    });
  }

  registerModalClose() {
    this.setState({
      registerModal: false,
    });
  }

  toggleLoginRegister() {
    this.setState({
      loginModal: !this.state.loginModal,
      registerModal: !this.state.registerModal,
    });
  }

  handleToggle() {
    this.setState({
      open: !this.state.open,
    });
  }

  // a function for capetalizing first letter of sub categories
  firstToUpperCase(category) {
    // adding space before and after /
    let Category = category.replace(/\//g, ' / ');
    // adding space before and after +
    Category = Category.replace(/\+/g, ' + ');
    // capetalizing first character of every word of the string
    Category = category.toLowerCase().replace(/(\?:^|\s)\S/g, letter => letter.toUpperCase());

    return Category;
  }
  /* a function for split category
  * for example instead of 'some-word > another-word' it will be 'another-word'
  */
  splitCategory(category) {
    const OldCatItem = category;
    const NewCatItem = OldCatItem.split('>').pop();

    return NewCatItem;
  }

  renderCategories(_categoryArray, key) {
    const __nestedMenu = [];
    if (_categoryArray && _categoryArray.length > 0) {
      for (let i = 0; i < _categoryArray.length; i++) {
        if (_categoryArray[i].items) {
          const category = _categoryArray[i].cat;
          __nestedMenu.push(
            <MenuItem
              required
              key={key + i}
              value={_categoryArray[i].cat}
              primaryText={_categoryArray[i].text}
              primaryTogglesNestedList
              nestedItems={this.renderCategories(_categoryArray[i].items, _categoryArray[i].cat)}
            />,
          );
        } else {
          const category = _categoryArray[i].cat;
          __nestedMenu.push(
            <MenuItem
              required
              key={key + i}
              value={_categoryArray[i].cat}
              onClick={() => this.handleCategoryFilter(category)}
              primaryText={_categoryArray[i].text}
            />,
          );
        }
      }
    }
    return __nestedMenu;
  }

  renderMenuChild() {
    return [
      <MenuItem primaryText="Show Level 2" />,
      <MenuItem primaryText="Grid lines" checked />,
      <MenuItem primaryText="Page breaks" insetChildren />,
      <MenuItem primaryText="Rules" checked />,
    ];
  }

  showTooltip() {
    console.log('Menu Browser: Show Tool Tip');
    this.setState({ isTooltipActive: true });
  }
  hideTooltip() {
    console.log('Menu Browser: Hide Tool Tip');
    this.setState({ isTooltipActive: false });
  }

  register(credentials) {
    this.setState({
      registerModal: false,
    });

    this.props
      .register(credentials)
      .then((response) => {
        swal({
          title: 'Thank you for joining!',
          text: `${response.aliasname}`,
          type: 'success',
        });
      })
      .catch((err) => {
        swal({
          title: 'Oops...',
          text: err,
          type: 'error',
        });
      });
  }

  login(credentials) {
    this.setState({
      loginModal: false,
    });
    this.props
      .login(credentials)
      .then((response) => {
        swal({
          title: 'Welcome Back!',
          text: `${response.aliasname}`,
          type: 'success',
        });
      })
      .catch((err) => {
        swal({
          title: 'Oops...',
          text: err,
          type: 'error',
        });
      });
  }

  logout() {
    browserHistory.push('/');
    this.props
      .logout()
      .then(() => {
        swal({
          title: 'User has been logged out',
          text: 'See you soon, we hope :)',
          type: 'success',
        });
      })
      .catch((err) => {
        swal({
          title: 'Oops...',
          text: err,
          type: 'error',
        });
      });
  }

  render() {
    if (this.props.categories.error) {
      alert(`Error:\nCould not fetch categories\n${this.props.categories.message}`);
    }
    const props = Object.assign({}, this.props);
    delete props.categories;
    delete props.getCategories;

    const mobileCustomContentStyle = { width: '70%' };
    const CustomTitleStyle = { padding: '24px 35px 20px' };
    const abouticonLogo = require('./assets/AboutIcon.png');
    const Logged = props => (
      <IconMenu
        menuStyle={{ backgroundColor: '#F3F2F7' }}
        {...props}
        iconButtonElement={
          <IconButton
            iconStyle={{ width: 40, height: 40, fill: '#64f000' }}
            style={{ width: 40, height: 40, padding: 0, color: 'red' }}
          >
            <MoreVertIcon />
          </IconButton>
        }
        targetOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
      >
        <MenuItem
          style={{ color: '#999' }}
          primaryText="Profile"
          leftIcon={
            <FontIcon className="material-icons" style={{ color: '#999' }}>
              perm_identity
            </FontIcon>
          }
          containerElement={<Link to="/profile" />}
        />
        <MenuItem
          style={{ color: '#999' }}
          primaryText="Shopping Cart"
          leftIcon={
            <FontIcon className="material-icons" style={{ color: '#999' }}>
              shopping_cart
            </FontIcon>
          }
        />
        <MenuItem
          style={{ color: '#999' }}
          primaryText="Wallet"
          leftIcon={
            <FontIcon className="material-icons" style={{ color: '#999' }}>
              account_balance_wallet
            </FontIcon>
          }
        />
        <MenuItem
          style={{ color: '#999' }}
          primaryText="Sign Out"
          onClick={this.logout}
          leftIcon={
            <FontIcon className="material-icons" style={{ color: '#999' }}>
              exit_to_app
            </FontIcon>
          }
        />
      </IconMenu>
    );

    Logged.muiName = 'IconMenu';

    return window.location.pathname != '/m' ? (
      <AppBar
        className="appbar-color"
        onLeftIconButtonTouchTap={this.handleToggle}
        iconElementLeft={
          <IconButton className="btnStyle">
            <SSIcon />
          </IconButton>
        }
        iconElementRight={
          <div className="menu-icons-right">
            {!this.props.user && (
              <div>
                <div className="menu-icon-container">
                  <FlatButton
                    onClick={this.loginModalOpen}
                    label="Login"
                    labelStyle={{ textTransform: 'capitalize' }}
                    style={{ color: '#828282' }}
                    icon={
                      <FontIcon className="material-icons" style={styles.viewOfferIcon}>
                        perm_identity
                      </FontIcon>
                    }
                  />
                </div>
                <div>
                  <Dialog
                    bodyStyle={styles.bodyStyle}
                    contentStyle={styles.customContentStyle}
                    modal={false}
                    open={this.state.loginModal}
                    onRequestClose={this.loginModalClose}
                  >
                    <Login
                      onLogin={this.login}
                      goToRegister={this.toggleLoginRegister}
                      closeModal={this.loginModalClose}
                    />
                  </Dialog>
                </div>
                <div className="menu-icon-container">
                  <FlatButton
                    onClick={this.registerModalOpen}
                    label="Register "
                    labelStyle={{ textTransform: 'capitalize' }}
                    style={{ color: '#828282' }}
                    icon={
                      <FontIcon className="material-icons" style={styles.viewOfferIcon}>
                        mode_edit
                      </FontIcon>
                    }
                  />
                </div>
                <div>
                  <Dialog
                    bodyStyle={styles.bodyStyle}
                    contentStyle={styles.customContentStyle}
                    modal={false}
                    open={this.state.registerModal}
                    onRequestClose={this.registerModalClose}
                  >
                    <Register
                      onRegister={this.register}
                      goToLogin={this.toggleLoginRegister}
                      closeModal={this.registerModalClose}
                    />
                  </Dialog>
                </div>
              </div>
            )}
            {this.props.user && (
              <div>
                <div className="menu-icon-container" />
                <Logged />
                <div className="menu-icon-container" style={{ paddingTop: '10px' }}>
                  <Badge
                    badgeContent={12}
                    secondary
                    badgeStyle={{
                      top: -10,
                      right: 2,
                      width: '20px',
                      height: '20px',
                      color: '#5FA02F',
                      backgroundColor: '#fff',
                    }}
                    onMouseEnter={this.showTooltip.bind(this)}
                    onMouseLeave={this.hideTooltip.bind(this)}
                    id="messageNotificationBox"
                  >
                    <FontIcon
                      className="material-icons"
                      style={{ color: '#5FA02F', paddingRight: '10px', paddingLeft: '10px' }}
                    >
                      mode_comment
                    </FontIcon>
                  </Badge>

                  <ToolTip
                    active={this.state.isTooltipActive}
                    position="bottom"
                    arrow="center"
                    parent="#messageNotificationBox"
                  >
                    <div className="menu-message-notification-box">
                      <div className="message-container">
                        <p>
                          <span className="menu-message-name">Johan Sonata</span>{' '}
                          <span className="menu-message-timestamp">12 min</span>
                        </p>
                        <p className="menu-message-text">Hello Quang Nguyen, how are you</p>
                      </div>
                    </div>
                    <div className="menu-message-notification-box">
                      <div className="message-container">
                        <p>
                          <span className="menu-message-name">Johan Sonata</span>{' '}
                          <span className="menu-message-timestamp">12 min</span>
                        </p>
                        <p className="menu-message-text">Hello Quang Nguyen, how are you</p>
                      </div>
                    </div>
                  </ToolTip>
                </div>
              </div>
            )}
          </div>
        }
      >
        <Drawer
          open={this.state.open}
          docked={false}
          onRequestChange={open => this.setState({ open })}
          containerStyle={{ backgroundColor: '#FFF' }}
        >
          <AppBar
            showMenuIconButton={false}
            title="Menu"
            style={{
              backgroundColor: '#fff',
              boxShadow: 'none',
              padding: '0 35px 0 35px',
              borderBottom: '5px solid #5FA02F',
            }}
            titleStyle={{
              color: '#5FA02F',
              fontFamily: 'Roboto Light',
              fontSize: '30px',
              fontWeight: 'bold',
            }}
          />

          <div className="list-cat">
            <Link to="/">
              <MenuItem
                onTouchTap={this.handleToggle}
                primaryText="Buy"
                innerDivStyle={{ padding: '0 35px', color: '#5FA02F' }}
              />
            </Link>
            <Link to="/store/newItem">
              <MenuItem
                onTouchTap={this.handleToggle}
                primaryText="Sell"
                innerDivStyle={{ padding: '0 35px', color: '#9d9d9d' }}
              />
            </Link>
              <Link to="/message">
                <MenuItem
                  onTouchTap={this.handleToggle}
                  primaryText="Messages"
                  innerDivStyle={{ padding: '0 35px', color: '#5FA02F' }}
                />
              </Link>
            <ListItem
              primaryText="Categories"
              style={{ padding: '0 18px', color: '#9d9d9d' }}
              className="menu-nested-list"
              nestedItems={this.renderCategories(this.props.categories.list, 'parent')}
            />
            <Link to="">
              <MenuItem
                disabled
                onTouchTap={this.handleToggle}
                primaryText="About"
                innerDivStyle={{ padding: '0 35px' }}
              />
            </Link>
            <Link to="">
              <MenuItem
                disabled
                onTouchTap={this.handleToggle}
                primaryText="Register"
                innerDivStyle={{ padding: '0 35px' }}
              />
            </Link>
            <Link to="">
              <MenuItem
                disabled
                onTouchTap={this.handleToggle}
                primaryText="Profile"
                innerDivStyle={{ padding: '0 35px' }}
              />
            </Link>
          </div>
        </Drawer>
      </AppBar>
    ) : (
      <div className="mobile-view-buttons">
        <div>
          <Dialog
            bodyStyle={styles.bodyStyle}
            contentStyle={styles.customContentStyle}
            modal={false}
            open={this.state.loginModal}
            onRequestClose={this.loginModalClose}
          >
            <Login
              onLogin={this.login}
              goToRegister={this.toggleLoginRegister}
              closeModal={this.loginModalClose}
            />
          </Dialog>
        </div>
        <div>
          <Dialog
            bodyStyle={styles.bodyStyle}
            contentStyle={styles.customContentStyle}
            modal={false}
            open={this.state.registerModal}
            onRequestClose={this.registerModalClose}
          >
            <Register
              onRegister={this.register}
              goToLogin={this.toggleLoginRegister}
              closeModal={this.registerModalClose}
            />
          </Dialog>
        </div>
        <div className="row bottom_button">
          <Col xs={12} style={{ top: '100', textAlign: 'centre' }}>
            <RaisedButton
              label="About"
              labelPosition="after"
              icon={<img src={abouticonLogo} />}
              style={styles.button}
              buttonStyle={{ borderRadius: '7px' }}
              labelStyle={{
                textTransform: 'capitalize',
                fontSize: '16px',
                float: 'left',
                top: '5px',
              }}
              className="mobile-menu-btn"
            />
          </Col>
          <Col xs={12} style={{ top: '100', textAlign: 'centre' }}>
            <RaisedButton
              onClick={this.loginModalOpen}
              label="Login/Register"
              labelPosition="after"
              icon={
                <FontIcon className="material-icons" color="#FFFFFF" style={styles.loginIcon}>
                  person
                </FontIcon>
              }
              style={styles.button}
              backgroundColor="#00d900"
              labelColor="#FFFFFF"
              buttonStyle={{ borderRadius: '7px', height: '45px' }}
              labelStyle={{ textTransform: 'capitalize', fontSize: '15px' }}
            />
          </Col>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const browser = state.browser;
  const categories = state.categories;
  // const filterOption = state.sorter.option;
  const session = state.session;
  const user = state.account.user;
  const { offers } = state;

  return { categories, browser, session, user, offers };
}

function mapDispatchToProps(dispatch) {
  return {
    // doLogout: () => dispatch(doLogout()),
    getCategories: () => dispatch(doCategoryReq()),
    register: credentials => dispatch(userActions.register(credentials)),
    login: credentials => dispatch(userActions.login(credentials)),
    checkCurrentUser: () => dispatch(userActions.checkCurrentUser()),
    logout: () => dispatch(userActions.logout()),
    sortOffersNew: params => dispatch(offerActions.sortOffers(params)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuBrowser);
