import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router';
import { Redirect } from 'react-router';
import Platform from 'react-platform-js';
import uaParser from 'ua-parser-js';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { grey600, grey900, grey300 } from 'material-ui/styles/colors';
import { CircularProgress } from 'material-ui';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Tabs, Tab } from 'material-ui/Tabs';
import { Col, Row, Button } from 'react-bootstrap';
import { MenuBrowser } from '../Menu';

import './styles/App.scss';

const styles = {
  overlay: {
    position: 'fixed',
    display: 'block',
    width: '100%',
    height: '100%',
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    backgroundColor: 'rgba(0,0,0,0.5)',
    zIndex: '2000',
  },
  loading: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
    color: '#168327',
  },
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
    color: '#5FA02F',
  },
  bar: {
    backgroundColor: '#5FA02F',
  },
};

injectTapEventPlugin();

const GlobalLoader = () => (
  <div style={styles.overlay}>
    <CircularProgress style={styles.loading} color={styles.loading.color} size={80} thickness={8} />
  </div>
);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewBrowser: false,
      dataToSearch: null,
    };
  }

  componentWillMount() {
    const browser_identifier = new uaParser().getResult();
    browser_identifier.device.type === 'mobile' ? browserHistory.push('/m') : null;
    if (this.props.router.location.pathname === '/') {
      this.setState({ viewBrowser: true });
    } else {
      this.setState({ viewBrowser: false });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.router.location.pathname === '/') {
      this.setState({ viewBrowser: true });
    } else {
      this.setState({ viewBrowser: false });
    }
  }

  handleActive(e) {
    const activeTabs = document.getElementsByClassName('active-link');
    if (activeTabs) {
      for (let i = 0; i < activeTabs.length; i++) {
        activeTabs[i].className = '';
      }
    }
    e.target.className = '';
    e.target.className = 'active-link';
  }

  handleOnActive(tab) {
    browserHistory.push(tab.props['data-route']);
  }

  render() {
    const muiTheme = getMuiTheme({
      palette: {
        primary1Color: grey900,
        primary2Color: grey600,
        primary3Color: grey300,
        canvasColor: grey300,
      },
      appBar: {
        height: 70,
      },
    });

    return (
      <div>
        {Platform.DeviceType == 'mobile' ? (
          <MuiThemeProvider muiTheme={muiTheme}>
            <div className="row mobile_view" style={{ marginLeft: '0px', marginRight: '0px' }}>
              {this.props.loading && <GlobalLoader />}
              <div
                className="App__childrenWrap"
                style={{
                  marginTop:
                    muiTheme.appBar.height -
                    (this.props.location.pathname.indexOf('/user/') !== -1 ? 70 : 20),
                }}
              >
                <div>{this.props.children}</div>
              </div>
              <MenuBrowser
                searchData={this.state.dataToSearch}
                stateUrl={this.props.location.pathname}
              />
            </div>
          </MuiThemeProvider>
        ) : (
          <MuiThemeProvider muiTheme={muiTheme}>
            <div>
              {this.props.loading && <GlobalLoader />}

              <MenuBrowser
                searchData={this.state.dataToSearch}
                stateUrl={this.props.location.pathname}
              />
              {window.location.pathname == '/' ||
              window.location.pathname == '/store/newItem' ||
              window.location.pathname == '/message' ? (
                <Row className="App__tabWrap">
                  <Col lg={12} xs={12} className="tab-wrapper no-mobile-padding">
                    <Col className="inner-tabs-div col-lg-4 col-md-4 col-sm-12">
                      <Tabs inkBarStyle={styles.bar} className="tab-container">
                        <Tab
                          label="BUY"
                          onClick={e => this.handleActive(e)}
                          className="tab"
                          data-route="/"
                          onActive={this.handleOnActive}
                          id="buyTabLabel"
                        >
                          <div className="hidden" />
                        </Tab>
                        <Tab
                          label="SELL"
                          onClick={e => this.handleActive(e)}
                          className="tab"
                          data-route="/store/newItem"
                          onActive={this.handleOnActive}
                          id="sellTabLabel"
                        >
                          <div className="hidden" />
                        </Tab>
                        <Tab
                          label="MESSAGES"
                          onClick={e => this.handleActive(e)}
                          className="tab"
                          data-route="/message"
                          onActive={this.handleOnActive}
                          id="messageTabLabel"
                        >
                          <div className="hidden" />
                        </Tab>
                      </Tabs>
                    </Col>
                  </Col>
                </Row>
              ) : null}
              <div className="App__childrenWrap">
                <div>{this.props.children}</div>
              </div>
            </div>
          </MuiThemeProvider>
        )}
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.node,
  loading: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    userAgent: state.userAgent,
    loading: state.app.loader.globalShow,
  };
}

export default connect(mapStateToProps)(App);
