
import React from 'react';
import { Col } from 'react-bootstrap';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import TextField from 'material-ui/TextField';

class ProfilePageWallets extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTabIndex: null,
      activeTabAction: null,
    };
    this.wallet = [{
      walletName: 'SYS Wallet',
      walletBitcoin: "00.0000",
      wallApproximateBitcoin: "0.0",
      walletImage: "SYSicon.png",
    }, {
      walletName: 'BTC Wallet',
      walletBitcoin: "00.0000",
      wallApproximateBitcoin: "0.0",
      walletImage: "BTCicon.png",
    },{
      walletName: 'SYS Wallet',
      walletBitcoin: "00.0000",
      wallApproximateBitcoin: "0.0",
      walletImage: "ZECicon.png",
    }];
    this.renderWallets = this.renderWalletsDesktop.bind(this);
    this.renderWalletsMobile = this.renderWalletsMobile.bind(this);
  }

  toggle(index, action) {
    if (action === this.state.activeTabAction && index === this.state.activeTabIndex) {
      this.setState({
        activeTabAction: null,
        activeTabIndex: null
      })
    }else {
      this.setState({
        activeTabIndex: index,
        activeTabAction: action
      })
    }
  }

  renderWalletsDesktop() {
    const walletList = this.wallet.map((wallet, index) => {
        return (<li key={index}>
          <Col lg={6} md={6} style={{ paddingBottom: '20px', cursor: 'pointer' }}>
            <div className="wallet-info">
              <div className="wallet-image"><img src={require('./asset/'+wallet.walletImage)}/></div>
              <div className="wallet-detail">
                <div className="wallet-name">
                  <h4 style={{ color: '#4894cb' }}>{wallet.walletName}</h4>
                </div>
                <div className="wallet-price">
                  {wallet.walletBitcoin} BTC ~${wallet.wallApproximateBitcoin}
                </div>
              </div>
            </div>
          </Col>
          <Col lg={2} md={2} style={{ textAlign: 'right', top: '10px', padding: '0px 5px' }}>
            <RaisedButton
              label="Send"
              labelStyle={{ textTransform: 'capitalize' }}
              labelColor="#000"
              icon={<img src={require('./asset/sendIcon.png')} style={{ marginRight: '8px' }} />}
              style={{ width: '93%' }}/>
          </Col>
          <Col lg={2} md={2} style={{ textAlign: 'right', top: '10px', padding: '0px 5px' }} onClick={() => { this.toggle(index, 'transaction') }}>
            <RaisedButton
              label="Transaction"
              labelStyle={{ textTransform: 'capitalize' }}
              backgroundColor="#f1c40f"
              labelColor="#fff"
              icon={<FontIcon className="material-icons" color="#fff">swap_horiz</FontIcon>}
              style={{ width: '93%' }}/>
          </Col>
          <Col lg={2} md={2} style={{ textAlign: 'right', top: '10px', padding: '0px 5px' }} onClick={() => { this.toggle(index, 'exchange') }}>
            <RaisedButton
              label="Exchange"
              labelStyle={{ textTransform: 'capitalize' }}
              backgroundColor="#3498db"
              labelColor="#fff"
              icon={<FontIcon className="material-icons" color="#fff">cached</FontIcon>}
              style={{ width: '93%' }}
            />
          </Col>
          {this.state.activeTabIndex == index && this.state.activeTabAction === 'transaction' ? <div className="transaction-tab">
            <div className="search-transaction">
              <TextField
                hintText="Search"
                hintStyle={{ paddingLeft: '20px' }}
                underlineShow={false}
                style={{ backgroundColor: '#fff' }}
              />
              <FontIcon className="material-icons" color="#777" style={{ top: '12px', right: '40px' }}>search</FontIcon>
            </div>
            <ul className="row">
              <li>
                <Col lg={1} xs={6}>
                  <div className="transaction-date">
                    <h4 style={{ marginTop: '0px', marginBottom: '0px' }}>APR 7</h4>
                  </div>
                </Col>
                <Col lg={1} xs={6} style={{ top: '10px' }}>
                  <div className="arrow">
                    <img src={require('./asset/forward-arrow.png')} />
                  </div>
                </Col>
                <Col lg={8} xs={12}>
                  <h4 style={{ marginTop: '0px', marginBottom: '0px' }}>Sent SYS</h4>
                  <span>To 1bkfgbkdghfkjhfgkEtgjnkfdgFHGYHRDFGkfg</span>
                </Col>
                <Col lg={2} xs={12}>
                  <h4>-102.000121236 SYS</h4>
                  <span>+15.25 $</span>
                </Col>
              </li>
              <li>
                <Col lg={1} xs={6}>
                  <div className="transaction-date">
                    <h4 style={{ marginTop: '0px', marginBottom: '0px' }}>APR 7</h4>
                  </div>
                </Col>
                <Col lg={1} xs={6} style={{ top: '10px' }}>
                  <div className="arrow">
                    <img src={require('./asset/back-arrow.png')} />
                  </div>
                </Col>
                <Col lg={8} xs={12}>
                  <h4 style={{ marginTop: '0px', marginBottom: '10px' }}>Sent SYS</h4>
                  <span>To 1bkfgbkdghfkjhfgkEtgjnkfdgFHGYHRDFGkfg</span>
                </Col>
                <Col lg={2} xs={12}>
                  <h4>-102.000121236 SYS</h4>
                  <span>+15.25 $</span>
                </Col>
              </li>
            </ul>
          </div> : null}
          {this.state.activeTabIndex == index && this.state.activeTabAction === 'exchange' ? <div className="transaction-tab center">
            <h3>To be implemented</h3>
          </div> : null}
        </li>);
    });
    return walletList;
  }

  renderWalletsMobile() {
    const walletList = this.wallet.map((wallet, index) => {
        return (<li key={index}>
          <Col className="col-xs-12 no-padding">
            <Col xs={6} className="mobile-wallet no-padding">
              <div className="wallet-info">
              <div className="wallet-image"><img src={require('./asset/'+wallet.walletImage)}/></div> 
                <div className="wallet-detail">
                  <div className="wallet-name">
                    <h4 style={{ color: '#4894cb' }}>{wallet.walletName}</h4>
                  </div>
                  <div className="wallet-price">
                    {wallet.walletBitcoin} BTC ~$ {wallet.wallApproximateBitcoin}
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={6} style={{ textAlign: 'right', top: '10px', padding: '0px 3px' }}>
              <RaisedButton
                label="Send"
                labelStyle={{ textTransform: 'capitalize' }}
                labelColor="#000"
                icon={<img src={require('./asset/sendIcon.png')} style={{ marginRight: '8px' }} />}
                style={{ width: '100%' }}/>
            </Col>
          </Col>
          <Col xs={6} style={{ top: '10px', padding: '0px 3px' }}>
            <RaisedButton
              label="Transaction"
              labelStyle={{ textTransform: 'capitalize' }}
              backgroundColor="#f1c40f"
              labelColor="#fff"
              icon={<FontIcon className="material-icons" color="#fff">swap_horiz</FontIcon>}
              style={{ width: '100%' }}
              onClick={() => { this.toggle(index, 'transaction')}}/>
          </Col>
          <Col xs={6} style={{ top: '10px', padding: '0px 3px' }}>
            <RaisedButton
              label="Exchange"
              labelStyle={{ textTransform: 'capitalize' }}
              backgroundColor="#3498db"
              labelColor="#fff"
              icon={<FontIcon className="material-icons" color="#fff">cached</FontIcon>}
              style={{ width: '100%' }}
              onClick={() => { this.toggle(index, 'exchange')}}/>
          </Col>
          {this.state.activeTabIndex == index && this.state.activeTabAction === 'transaction' ? <div className="transaction-tab">
            <div className="search-transaction">
              <TextField
                hintText="Search"
                hintStyle={{ paddingLeft: '20px' }}
                underlineShow={false}
                style={{ backgroundColor: '#fff' }}/>
              <FontIcon className="material-icons" color="#777" style={{ top: '12px', right: '40px' }}>search</FontIcon>
            </div>
            <ul className="row">
              <li>
                <Col lg={1} xs={6}>
                  <div className="transaction-date">
                    <h4 style={{ marginTop: '0px', marginBottom: '0px' }}>APR 7</h4>
                  </div>
                </Col>
                <Col lg={1} xs={6} style={{ top: '10px' }}>
                  <div className="arrow">
                    <img src={require('./asset/forward-arrow.png')} />
                  </div>
                </Col>
                <Col lg={8} xs={12}>
                  <h4 style={{ marginTop: '0px', marginBottom: '0px' }}>Sent SYS</h4>
                  <span>To 1bkfgbkdghfk</span>
                </Col>
                <Col lg={2} xs={12}>
                  <h4>-102.000121236 SYS</h4>
                  <span>+15.25 $</span>
                </Col>
              </li>
              <li>
                <Col lg={1} xs={6}>
                  <div className="transaction-date">
                    <h4 style={{ marginTop: '0px', marginBottom: '0px' }}>APR 7</h4>
                  </div>
                </Col>
                <Col lg={1} xs={6} style={{ top: '10px' }}>
                  <div className="arrow">
                    <img src={require('./asset/back-arrow.png')} />
                  </div>
                </Col>
                <Col lg={8} xs={12}>
                  <h4 style={{ marginTop: '0px', marginBottom: '10px' }}>Sent SYS</h4>
                  <span>To 1bkfgbkdghfkjh</span>
                </Col>
                <Col lg={2} xs={12}>
                  <h4>-102.000121236 SYS</h4>
                  <span>+15.25 $</span>
                </Col>
              </li>
            </ul>
          </div> : null}
          {this.state.activeTabIndex == index && this.state.activeTabAction === 'exchange' ? <div className="transaction-tab center">
            <h3>To be implemented</h3>
          </div> : null}
        </li>)
    });
    return walletList
  }

  render() {
    return (
      <div className="wallet-tab">
        <Col xsHidden>
          {/* view for big screen */}
          <ul className="row">
            {this.renderWalletsDesktop()}
          </ul>
        </Col>
        {/* view for small screens */}
        <Col lgHidden mdHidden>
          <ul className="row">
            {this.renderWalletsMobile()}
          </ul>
        </Col>
      </div>
    )
  }
}

export default ProfilePageWallets;
