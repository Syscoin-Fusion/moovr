
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Grid, Image } from 'react-bootstrap';
import Badge from 'material-ui/Badge';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { Tabs, Tab } from 'material-ui/Tabs';
import { List, ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';


class ProfilePageInfo extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);
  }

  render() {
    const { view, userOffers } = this.props;
    const placeholderImage = require('./asset/no_image.png');
    const address = require('./asset/address.png');
    const phoneNumber = require('./asset/phoneNumber.png');
    const facebook = require('./asset/facebook.png');
    const twitter = require('./asset/twitter.png');
    const youtube = require('./asset/youtube.png');
    const placeholder = require('./asset/avatar-placeholder.png');

    return (
      <div className="info-tab">
        <Col style={{ fontSize: '18px' }}>
          <ul className="row info-main">


            {/* profile pic */}
            <div className="profile-pic">
              <li>
                <Col lg={2} xs={12} className="avatar">
                  {
                    this.props.publicProfile.avatar ?
                      <Image src={placeholder} alt="image not load" circle className="profile_image" />
                      :
                      <Image src={placeholder} alt="no image" circle className="profile_image" />

                  }
                </Col>
                <Col lg={8} xs={12} className="description" style={{ margin: '60px 0px  0px 20px' }}>
                  <div className="personName">
                    {this.props.publicProfile.name}
                  </div>
                  <div style={{ color: '#BFBFBF', fontSize: '95%', textAlign: 'justify' }}>
                    {this.props.publicProfile.about}
                  </div>
                </Col>
              </li>
            </div>

            {/* personal info */}
            <li>
              <Col lg={1} xs={2} style={{ height: '100%' }} >
                <div className="user-icon">
                  <FontIcon className="material-icons" color="#5FA02F">person</FontIcon>
                </div>
              </Col>
              <Col xs={3} className="labelTitle">
                <div className="user-name">Name :</div>
              </Col>
              <Col lg={9} xs={7} >
                <div>
                  {this.props.publicProfile.name}
                </div>
              </Col>
            </li>

            <li>
              <Col lg={1} xs={2} style={{ height: '100%' }} >
                <div className="user-icon">
                  <FontIcon className="material-icons" color="#5FA02F">email</FontIcon>
                </div>
              </Col>
              <Col xs={3} className="labelTitle">
                <div className="user-name">Email :</div>
              </Col>
              <Col lg={9} xs={7} >
                <div>
                  {this.props.publicProfile.email}
                </div>
              </Col>
            </li>


            <li>
              <Col lg={1} xs={2} style={{ height: '100%' }} >
                <div className="user-icon">
                  <FontIcon className="material-icons" color="#5FA02F">speaker</FontIcon>
                </div>
              </Col>
              <Col xs={3} className="labelTitle">
                <div className="user-name">Alias :</div>
              </Col>
              <Col lg={9} xs={7} >
                <div>
                  {this.props.userDetails.aliasname}
                </div>
              </Col>
            </li>

            <li>
              <Col lg={1} xs={2} style={{ height: '100%' }} >
                <div className="user-icon">
                  <FontIcon className="material-icons" color="#5FA02F">lock</FontIcon>
                </div>
              </Col>
              <Col xs={4} className="labelTitle">
                <div className="user-name">Password :</div>
              </Col>
              <Col lg={9} xs={6} >
                <div>
                  *********
              </div>
              </Col>
            </li>

            <li style={{ padding: '40px 0px 30px 20px' }}>
              <Col lg={10} >
                <div>Show Prices in</div>
                <div>
                  <SelectField value={this.props.publicProfile.currency} labelStyle={{ color: '#5FA02F' }} style={{ width: '180px' }}>
                    <MenuItem value={'USD'} label="USD" primaryText="USD" labelStyle={{ color: '#5FA02F' }} />
                    <MenuItem value={'EURO'} label="EURO" primaryText="EURO" labelStyle={{ color: '#5FA02F' }} />
                  </SelectField>
                </div>
              </Col>
            </li>
            <div className="userInfo">
              {/* Public Info */}
              <li>

                <Col lg={4} >
                  <h2 style={{ margin: '0px 0px 20px 20px' }}>Public Info</h2>
                </Col>
              </li>
              <li >
                <Col lg={1} xs={2} style={{ height: '100%' }} >
                  <div className="user-icon">
                    <img src={address} />
                  </div>
                </Col>
                <Col xs={4} className="labelTitle">
                  <div className="user-name">Public address:</div>
                </Col>
                <Col lg={9} xs={6} >
                  <div>
                    {this.props.publicProfile.address}
                  </div>
                  <div>
                    <FlatButton label="Or pin a Location in a Map" labelStyle={{ color: '#5FA02F', paddingLeft: '0px', textTransform: 'capitalize' }} />                          </div>
                </Col>
              </li>

              {/* Private Info */}
              <li>

                <Col lg={4} >
                  <h2 style={{ margin: '20px 0px 20px 20px' }}>Private Info</h2>
                </Col>
              </li>
              <li>
                <Col lg={1} xs={2} style={{ height: '100%' }} >
                  <div className="user-icon">
                    <img src={address} />
                  </div>
                </Col>
                <Col xs={5} className="labelTitle">
                  <div className="user-name">Shipping address :</div>
                </Col>
                <Col lg={9} xs={5} >
                  <div>
                    {this.props.privateProfile.shippingAddr}
                  </div>
                </Col>
              </li>
              <li>
                <Col lg={1} xs={2} style={{ height: '100%' }} >
                  <div className="user-icon">
                    <img src={phoneNumber} />
                  </div>
                </Col>
                <Col xs={5} className="labelTitle" >
                  <div className="user-name">Phone Number :</div>
                </Col>
                <Col lg={9} xs={5} >
                  {this.props.privateProfile.phone ?
                    <div>
                      {this.props.privateProfile.phone}
                    </div>
                    : <div>
                      <FlatButton label="add phone number" labelStyle={{ color: '#5FA02F', paddingLeft: '0px', textTransform: 'capitalize' }} />
                    </div>
                  }
                </Col>
              </li>
              <li>
                <Col lg={1} xs={2} style={{ height: '100%' }} >
                  <div className="user-icon">
                    <img src={youtube} />
                  </div>
                </Col>
                <Col xs={3} className="labelTitle">
                  <div className="user-name">Youtube</div>
                </Col>
                {this.props.privateProfile.social.youtube ?
                  <div>
                    <Col lg={2} xs={3} >
                      <div>
                        <FlatButton label="disconnect youtube" labelStyle={{ paddingLeft: '0px', textTransform: 'capitalize' }} />

                      </div>
                    </Col>
                    <Col lg={6} xs={3} >
                      <FlatButton label="verified" disabled labelStyle={{ color: '#5FA02F' }} style={{ marginLeft: '15px' }} />
                    </Col>
                  </div>
                  :
                  <Col lg={9} xs={7} >
                    <div>
                      <RaisedButton label="verify with youtube" primary={true} buttonStyle={{ backgroundColor: '#CC181E' }} />
                    </div>
                  </Col>
                }
              </li>
              <li>
                <Col lg={1} xs={2} style={{ height: '100%' }}  >
                  <div className="user-icon">
                    <img src={twitter} />
                  </div>
                </Col>
                <Col xs={3} className="labelTitle">
                  <div className="user-name">Twitter</div>
                </Col>
                {this.props.privateProfile.social.twitter ?
                  <div>
                    <Col lg={2} xs={3} >
                      <div>
                        <FlatButton label="disconnect twitter" labelStyle={{ paddingLeft: '0px', textTransform: 'capitalize' }} />

                      </div>
                    </Col>
                    <Col lg={6} xs={3} >
                      <FlatButton label="verified" disabled labelStyle={{ color: '#5FA02F' }} style={{ marginLeft: '15px' }} />
                    </Col>
                  </div>
                  :
                  <Col lg={9} xs={7} >
                    <div>
                      <RaisedButton label="verify with twitter" primary={true} buttonStyle={{ backgroundColor: '#10B3EA' }} />
                    </div>
                  </Col>
                }
              </li>
              <li>
                <Col lg={1} xs={2} style={{ height: '100%' }} >
                  <div className="user-icon">
                    <img src={facebook} />
                  </div>
                </Col>
                <Col xs={3} className="labelTitle">
                  <div className="user-name">Facbook</div>
                </Col>
                {this.props.privateProfile.social.facebook ?
                  <div>
                    <Col lg={2} xs={3} >
                      <div>
                        <FlatButton label="disconnect facebook" labelStyle={{ paddingLeft: '0px', textTransform: 'capitalize' }} />

                      </div>
                    </Col>
                    <Col lg={6} xs={3} >
                      <FlatButton label="verified" disabled labelStyle={{ color: '#5FA02F' }} style={{ marginLeft: '15px' }} />
                    </Col>
                  </div>
                  :
                  <Col lg={9} xs={7} >
                    <div>
                      <RaisedButton label="verify with facebook" primary={true} buttonStyle={{ backgroundColor: '#3A589B' }} />
                    </div>
                  </Col>

                }
              </li>

              {/* Theme */}
              <Col lg={4} >
                <h2 style={{ margin: '20px 0px 20px 20px' }}>Theme</h2>
              </Col>
            </div>
            <li>
              <Col lg={4} lgOffset={8} xsOffset={9} xs={3} style={{ top: '10px' }}>
                <RaisedButton
                  label="Edit"
                  labelStyle={{ textTransform: 'capitalize' }}
                  labelColor="#fff"
                  backgroundColor="#5FA02F"
                  icon={<img src={require('./asset/pen.png')} style={{ marginRight: '8px', color: '#fff' }} />}
                  className="editButton"
                />
              </Col>
            </li>
          </ul>
        </Col>
      </div>
    )
  }
}

export default ProfilePageInfo;
