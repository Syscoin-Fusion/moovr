import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Grid, Image } from 'react-bootstrap';
import Badge from 'material-ui/Badge';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { Tabs, Tab } from 'material-ui/Tabs';
import { List, ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';

import './styles/ProfilePage.scss';

// Tabs
import ProfilePageInfo from './ProfilePageInfo';
import ProfilePageOffers from './ProfilePageOffers';
import ProfilePageWallets from './ProfilePageWallets';

const styles = {
  bar: {
    backgroundColor: '#5FA02F',
  },
};

class ProfilePage extends Component {
  constructor(props) {
    console.log('profile', props);
    super(props);
    this.state = {
      profileTab: 'info',
    };
    this._changeProfileTab = this._changeProfileTab.bind(this);
  }

  _changeProfileTab(value) {
    console.log('in method');
    this.setState({
      profileTab: value,
    });
  }

  render() {
    const { view, userOffers } = this.props;
    const placeholderImage = require('./asset/no_image.png');
    const address = require('./asset/address.png');
    const phoneNumber = require('./asset/phoneNumber.png');
    const facebook = require('./asset/facebook.png');
    const twitter = require('./asset/twitter.png');
    const youtube = require('./asset/youtube.png');
    const placeholder = require('./asset/avatar-placeholder.png');

    return (
      <div className="ProfilePage">
        {/* profile-tabs */}
        <Col lg={12} xs={12} md={12} className="profilePage__wraper">
          <div className="profile-tabs row">
            {/* tabs for big screen */}
            <Row className="App__tabWrap">
              <Col className="tab-wrapper no-mobile-padding col-xs-12">
                <Col className="inner-tabs-div col-lg-4 col-md-4 col-xs-12">
                  <Tabs inkBarStyle={styles.bar} className="tab-container">
                    <Tab
                      label="Profile"
                      className={`tab ${this.state.profileTab == 'info' ? 'active' : ''}`}
                      onClick={() => {
                        this._changeProfileTab('info');
                      }}
                      data-route="/"
                      id="buyTabLabel"
                    >
                      <div className="hidden" />
                    </Tab>
                    <Tab
                      label="Offers"
                      className={`tab ${this.state.profileTab == 'order' ? 'active' : ''}`}
                      onClick={() => {
                        this._changeProfileTab('order');
                      }}
                      data-route="/store/newItem"
                      id="sellTabLabel"
                    >
                      <div className="hidden" />
                    </Tab>
                    <Tab
                      label="Wallets"
                      className={`tab ${this.state.profileTab == 'wallet' ? 'active' : ''}`}
                      onClick={() => {
                        this._changeProfileTab('wallet');
                      }}
                      data-route="/message"
                      id="messageTabLabel"
                    >
                      <div className="hidden" />
                    </Tab>
                  </Tabs>
                </Col>
              </Col>
            </Row>
            {/* <Col lg={12} xsHidden smHidden className="no-padding" style={{ justifyContent: 'center', display: 'flex' }}>
              <Col lg={2} md={2} className="no-padding">
                <div className={`tab ${this.state.profileTab == 'info' ? 'active' : ''}`} onClick={() => { this._changeProfileTab('info') }}>
                  Profile
                  </div>
              </Col>

              <Col lg={2} md={2} className="no-padding">
                <div className={`tab ${this.state.profileTab == 'order' ? 'active' : ''}`} onClick={() => { this._changeProfileTab('order') }}>
                  Offers
                    </div>
              </Col>

              <Col lg={2} md={2} className="no-padding">
                <div className={`tab ${this.state.profileTab == 'wallet' ? 'active' : ''}`} onClick={() => { this._changeProfileTab('wallet') }}>
                  Wallets
                    </div>
              </Col>

              <Col lg={3} md={3}>
                <div className={`tab ${this.state.profileTab == 'transaction' ? 'active' : ''}`} onClick={() => { this._changeProfileTab('transaction') }}>
                  Transactions
                    </div>
              </Col>
            </Col> */}
            {/* tabs for small screen */}
            <Col lg={12} xsHidden lgHidden mdHidden className="no-padding">
              <Col xs={4} className={`no-padding tab-mob ${this.state.profileTab == 'info' ? 'active' : ''}`} onClick={() => { this._changeProfileTab('info') }}>
                <div>
                  <FontIcon className="material-icons">perm_identity</FontIcon>
                </div>
                <div>Profile</div>
              </Col>
              <Col
                xs={4}
                className={`no-padding tab-mob ${this.state.profileTab == 'order' ? 'active' : ''}`}
                onClick={() => {
                  this._changeProfileTab('order');
                }}
              >
                <div>
                  <FontIcon className="material-icons">shopping_cart</FontIcon>
                </div>
                <div> Offers </div>
              </Col>

              <Col
                xs={4}
                className={`no-padding tab-mob ${
                  this.state.profileTab == 'wallet' ? 'active' : ''
                }`}
                onClick={() => {
                  this._changeProfileTab('wallet');
                }}
              >
                <div>
                  <FontIcon className="material-icons">account_balance_wallet</FontIcon>
                </div>
                <div> Wallets </div>
              </Col>
              {/* <Col xs={3} className={`tab-mob ${this.state.profileTab == 'transaction' ? 'active' : ''}`} onClick={() => { this._changeProfileTab('transaction') }}>
                <div>
                  <FontIcon className="material-icons">attach_money</FontIcon>
                </div>
                <div>
                  Transaction
                    </div>
              </Col> */}
            </Col>
          </div>
        </Col>

        {/* profile-tabs-info */}
        <Col lg={10} lgOffset={1} xs={12} xsOffset={0} className="profile-tabs-div">
          <div className="profile-tabs-info">
            {/* profile-tab */}
            {this.state.profileTab == 'info' ? (
              <ProfilePageInfo
                publicProfile={this.props.publicProfile}
                privateProfile={this.props.privateProfile}
                userDetails={this.props.userDetails}
              />
            ) : null}

            {/* orders-tab */}
            {this.state.profileTab == 'order' ? (
              <ProfilePageOffers userOffers={this.props.userOffers} />
            ) : null}

            {/* wallets-tab */}
            {this.state.profileTab == 'wallet' ? <ProfilePageWallets /> : null}
          </div>
        </Col>
      </div>
    );
  }
}

ProfilePage.propsTypes = {
  view: PropTypes.string,
};

function mapStateToProps(state) {
  console.log(state, 'state value in profile');
  return {
    clusters: state.clusters,
    userOffers: state.account.userOffers,
    userDetails: state.account.user,
    publicProfile: state.account.user.publicProfile,
    privateProfile: state.account.user.privateProfile,
  };
}

// export default connect(
//     state => ({
//       view: state.base.get('view'),
//     }),
// )(ProfilePage);

// export default connect(
//     state => ({
//       view: state.base.get('view'),
//     }),
// )(ProfilePage);

// function mapDispatchToProps(dispatch) {
//   return {

//   };
// }

// export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
export default connect(mapStateToProps, null)(ProfilePage);
