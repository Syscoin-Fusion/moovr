
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Grid, Image } from 'react-bootstrap';
import Badge from 'material-ui/Badge';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { Tabs, Tab } from 'material-ui/Tabs';
import { List, ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';


class ProfilePageOffers extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    const placeholderImage = require('./asset/no_image.png');

    return (
      <div className="order-tab">
        {/* view for big screens */}
        <Col xsHidden>
          <ul className="row">
            {this.props.userOffers.map((value, index) => {
              return (
                <li key={index}>
                  <Col lg={1} style={{ top: '30' }}>
                    <Badge
                      badgeContent={index + 1}
                      badgeStyle={{
                        top: 0,
                        left: 0,
                        width: '20px',
                        height: '20px',
                        color: '#fff',
                        backgroundColor: '#64f000',
                        fontSize: '16px',
                      }}
                      style={{ padding: '0' }}
                    >
                    </Badge>
                  </Col>
                  <Col lg={9}>
                    <div className="item-name">{value.title}</div>
                    <div className="item-info">
                      <div className="item-image">
                        <img src={value.media.mediaVault.length > 0 ? value.media.mediaVault[0].mediaURL : placeholderImage} />
                      </div>
                      <div className="item-detail">
                        <div className="item-title">{value.description}</div>
                        <div className="item-delievery-date">
                          Delive Date: 15 Aug/2017
                </div>
                      </div>
                    </div>
                  </Col>
                  <Col lg={2} className="order-status" style={{ color: 'red' }}>Undelivered</Col>
                </li>
              )
            })}
          </ul>
        </Col>

        {/* view for small screens */}
        <Col lgHidden mdHidden>
          <List className="mobile-view">
            {
              this.props.userOffers.map((value, index) => {
                return (<div key={index}>
                  <ListItem
                    leftAvatar={
                      <img className="item-thumb-img" src={value.media.mediaVault.length > 0 ? value.media.mediaVault[0].mediaURL : placeholderImage} />
                    }
                    rightIconButton={<p className="item-status">Undelivered</p>}
                    primaryText={value.title}
                    secondaryText={
                      <p>{value.description}<br />
                        <span className="item-delivery-date">Delive Date: 15 Aug/2017</span>
                      </p>
                    }
                    secondaryTextLines={2}
                  />
                  <Divider inset={true} />
                </div>)
              })
            }



          </List>
        </Col>

      </div>
    )

  }
}

export default ProfilePageOffers;
