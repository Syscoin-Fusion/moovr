import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import Formsy from 'formsy-react';
import { FormsyText } from 'formsy-material-ui/lib';

import SSIcon from './icon';

import './styles/Register.scss';

const styles = {
  register__fieldIcon: {
    margin: '20px 5px 0 0',
    color: '#b4b4b4',
  },
  floatingLabelFocusStyle: {
    color: '#47a400',
  },
  btn_boder_radius: {
    borderRadius: '0px',
  },
};

class Register extends Component {
  constructor() {
    super();

    this.state = {
      credentials: {
        aliasname: '',
        password: '',
      },
    };
    this.enableButton = this.enableButton.bind(this);
    this.disableButton = this.disableButton.bind(this);
    this.socialClick = this.socialClick.bind(this);
  }

  enableButton() {
    this.setState({ canSubmit: true });
  }

  disableButton() {
    this.setState({ canSubmit: false });
  }

  socialClick() {
    alert('Not Working Yet');
  }

  updateCredentails(field, event) {
    event.preventDefault();
    const updated = Object.assign({}, this.state.credentials);
    updated[field] = event.target.value;
    this.setState({
      credentials: updated,
    });
  }

  register(data) {
    this.props.onRegister(data);
  }

  renderRegisterForm() {
    return (
      <div className="register__form">
        <div className="register__icon">
          <SSIcon />
        </div>
        <h4 className="register__title">{'SIGNUP'}</h4>
        <div className="register__body">
          <Formsy.Form
            onValid={this.enableButton}
            onInvalid={this.disableButton}
            onValidSubmit={e => this.register(e)}
          >
            <div className="register__fieldWrap">
              <FontIcon className="material-icons " style={styles.register__fieldIcon}>
                person
              </FontIcon>
              <FormsyText
                name="aliasname"
                hintText="Enter your Alias"
                floatingLabelText="Alias"
                required
                requiredError="This field is required"
                floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
              />
            </div>
            {/* <div className="register__fieldWrap">
              <FontIcon className="material-icons " style={styles.register__fieldIcon}>
                email
              </FontIcon>
              <FormsyText
                name="email"
                hintText="Enter a valid email"
                floatingLabelText="Email"
                required
                validations="isEmail"
                validationError="This is not a valid email"
                requiredError="This field is required"
                floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                onChange={(e, val) => {
                  this.setState({ email: val });
                }}
              />
            </div> */}
            <div className="register__fieldWrap">
              <FontIcon className="material-icons " style={styles.register__fieldIcon}>
                lock
              </FontIcon>
              <FormsyText
                name="password"
                hintText="Enter a password"
                floatingLabelText="Password"
                type="password"
                required
                requiredError="This field is required"
                style={styles.register_input}
                floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
              />
            </div>
            <div className="register__fieldWrap">
              <FontIcon className="material-icons " style={styles.register__fieldIcon}>
                lock
              </FontIcon>
              <FormsyText
                hintText="Password Again"
                type="password"
                name="repeated_password"
                validations="equalsField:password"
                validationError="Passwords must match"
                floatingLabelText="Password Again"
                floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
              />
            </div>
            <RaisedButton
              label="Signup"
              type="submit"
              primary={false}
              buttonStyle={styles.btn_boder_radius}
              className="register__botton"
              backgroundColor="#50bb00"
              labelColor="white"
              fullWidth
              disabled={!this.state.canSubmit}
            />
          </Formsy.Form>
          <div className="register__socialWrap">
            <div className="register_socialText">Signup with: </div>
            <img
              className="register__socialLogo"
              onClick={this.socialClick}
              alt=""
              src={require('./assets/google-plus.svg')}
            />
            <img
              className="register__socialLogo"
              onClick={this.socialClick}
              alt=""
              src={require('./assets/facebook.svg')}
            />
            <img
              className="register__socialLogo"
              onClick={this.socialClick}
              alt=""
              src={require('./assets/twitter.svg')}
            />
          </div>
        </div>
        <p className="register__footer">
          Already have an account?{' '}
          <span onClick={this.props.goToLogin}>
            <a>{'Login'}</a>
          </span>
        </p>
      </div>
    );
  }

  render() {
    return <div className="container register__container">{this.renderRegisterForm()}</div>;
  }
}

export default Register;
