import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import Formsy from 'formsy-react';
import { FormsyText } from 'formsy-material-ui/lib';

import SSIcon from './icon';

import './styles/Login.scss';

const styles = {
  login__fieldIcon: {
    margin: '20px 5px 0 0',
    color: '#b4b4b4',
  },
  floatingLabelFocusStyle: {
    color: '#47a400',
  },
  btn_boder_radius: {
    borderRadius: '0px',
  },
};

class Login extends Component {
  constructor() {
    super();

    this.state = {
      canSubmit: false,
    };
    this.enableButton = this.enableButton.bind(this);
    this.disableButton = this.disableButton.bind(this);
    this.socialClick = this.socialClick.bind(this);
    /* this.updateCredentails = this.updateCredentails.bind(this); */
    this.renderLoginForm = this.renderLoginForm.bind(this);
    this.login = this.login.bind(this);
  }

  login(data) {
    this.props.onLogin(data);
  }

  socialClick() {
    alert('Not Working Yet');
  }

  enableButton() {
    this.setState({ canSubmit: true });
  }

  disableButton() {
    this.setState({ canSubmit: false });
  }

  renderLoginForm() {
    return (
      <div className="login__form">
        <div className="login__icon">
          <SSIcon />
        </div>
        <h4 className="login__title">{'LOGIN'}</h4>
        <div className="login__body">
          <Formsy.Form
            onValid={this.enableButton}
            onInvalid={this.disableButton}
            onValidSubmit={e => this.login(e)}
          >
            <div className="login__fieldWrap">
              <FontIcon className="material-icons " style={styles.login__fieldIcon}>
                perm_identity
              </FontIcon>
              <FormsyText
                name="aliasname"
                floatingLabelText="Alias"
                hintText="Enter an Alias"
                required
                requiredError="This field is required"
                floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
              />
            </div>
            <div className="login__fieldWrap">
              <FontIcon className="material-icons " style={styles.login__fieldIcon}>
                lock
              </FontIcon>
              <FormsyText
                name="password"
                floatingLabelText="Password"
                hintText="Enter a password"
                type="password"
                required
                requiredError="This field is required"
                style={styles.register_input}
                floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
              />
            </div>
            <RaisedButton
              label="Login"
              type="submit"
              primary={false}
              buttonStyle={styles.btn_boder_radius}
              className="login__botton"
              backgroundColor="#50bb00"
              labelColor="white"
              fullWidth
              disabled={!this.state.canSubmit}
            />
          </Formsy.Form>
          <div className="login__socialWrap">
            <div className="register_socialText">Signup with: </div>
            <img
              className="login__socialLogo"
              onClick={this.socialClick}
              alt=""
              src={require('./assets/google-plus.svg')}
            />
            <img
              className="login__socialLogo"
              onClick={this.socialClick}
              alt=""
              src={require('./assets/facebook.svg')}
            />
            <img
              className="login__socialLogo"
              onClick={this.socialClick}
              alt=""
              src={require('./assets/twitter.svg')}
            />
          </div>
        </div>
        <p className="login__footer">
          New to Moovr?{' '}
          <span onClick={this.props.goToRegister}>
            <a>{'Signup'}</a>
          </span>
        </p>
      </div>
    );
  }

  render() {
    return <div className="container login__container">{this.renderLoginForm()}</div>;
  }
}

export default Login;
