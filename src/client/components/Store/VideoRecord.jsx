import React from 'react';
import RecordRTC from 'recordrtc';
import { RaisedButton } from 'material-ui';
import { Row, Col, Button, Glyphicon } from 'react-bootstrap';

import Time from './Time';

const styles = {
  white: {
    color: '#fff',
  },
  colCentered: {
    float: 'none',
    margin: '0 auto',
    width: '100%',
  },
  marginTopVideo: {
    marginTop: '5px',
  },
  centerText: {
    margin: 'auto',
    display: 'table',
  },
};

const hasGetUserMedia = !!(
  navigator.getUserMedia ||
  navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia ||
  navigator.msGetUserMedia
);

const captureUserMedia = (callback) => {
  const params = { audio: true, video: true };
  navigator.getUserMedia(params, callback, (error) => {
    alert(JSON.stringify(error));
  });
};

const Webcam = props => <video autoPlay muted src={props.src} />;

class VideoRecord extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      recordVideo: null,
      src: null,
      timer: 0,

      open: false,
      counter: 0,
      isRecording: false,
      permissions: {
        audio: true,
        video: true,
      },
      videoOptions: {
        mimeType: 'video/webm',
        bitsPerSecond: 128000,
      },
    };

    this.requestUserMedia = this.requestUserMedia.bind(this);
    this.startRecord = this.startRecord.bind(this);
    this.stopRecord = this.stopRecord.bind(this);

    this.intervalTrigger;
    this.localStream = null;
    this.audio;
    this.video;
  }

  componentWillUnmount() {
    if (this.localStream !== null) this.localStream.stop();
  }

  componentDidMount() {
    if (!hasGetUserMedia) {
      alert('Your browser cannot stream from your webcam. Please switch to Chrome or Firefox.');
    }
    this.requestUserMedia();
    if (this.props.startRecord) {
      this.startRecord();
    }
  }

  requestUserMedia() {
    console.log('requestUserMedia');
    captureUserMedia((stream) => {
      this.setState({ src: window.URL.createObjectURL(stream) });
      console.log('setting state', this.state);
    });
  }

  startRecord() {
    console.log('ACZ (START RECORDING) --> ');

    captureUserMedia((stream) => {
      let counter = 0;
      this.setState({ isRecording: true });
      this.state.recordVideo = RecordRTC(stream, {
        mimeType: 'video/webm',
        bitsPerSecond: 128000,
      });
      this.state.recordVideo.startRecording();
      this.intervalTrigger = window.setInterval(() => {
        counter++;
        this.setState({ counter });
      }, 1000);
    });
  }

  stopRecord() {
    const videoData = new FormData();
    this.setState({ isRecording: false });
    window.clearInterval(this.intervalTrigger);
    this.state.recordVideo.stopRecording((url) => {
      const params = {
        type: 'video/webm',
        id: Math.floor(Math.random() * 90000) + 10000,
        url,
        blob: this.state.recordVideo.blob,
      };
      videoData.append('video', params.blob, 'videoRecorded.webm');
      this.props.onRecorded(videoData, params.url);
      console.log('ACZ (params) --> ', params);
    });
  }

  stopIcon() {
    return <Glyphicon glyph="stop" style={styles.white} />;
  }

  render() {
    return (
      <div style={styles.marginTopVideo}>
        <Row>
          <Col xs={12} md={6} mdOffset={3} lg={6} lgOffset={3}>
            <Webcam style={styles.colCentered} src={this.state.src} />
            {/* <video ref="video" style={styles.colCentered} /> */}
          </Col>
        </Row>
        <Row>
          <Col xs={6} md={3} mdOffset={3} lg={3} lgOffset={3}>
            <RaisedButton
              label={this.state.isRecording ? 'RECORDING...' : 'RECORD'}
              backgroundColor="#2ab27b"
              labelColor="#fff"
              disabled={this.state.isRecording}
              onClick={this.startRecord}
              fullWidth
            />
          </Col>
          <Col xs={3} md={1} lg={1}>
            <Row style={styles.centerText}>
              <Time value={this.state.counter} />
            </Row>
          </Col>
          <Col xs={3} md={2} lg={2}>
            <RaisedButton
              icon={this.stopIcon()}
              backgroundColor="#eb4d5c"
              labelColor="#fff"
              disabled={!this.state.isRecording}
              onClick={this.stopRecord}
              fullWidth
            />
          </Col>
          <Row>
            <Col xs={3} md={2} lg={2}>
              <Button className="mui-btn mui-btn--fab">?</Button>
            </Col>
          </Row>
        </Row>
      </div>
    );
  }
}

export default VideoRecord;
