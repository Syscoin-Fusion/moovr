import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Col, Grid } from 'react-bootstrap';
import Platform from 'react-platform-js';

// redux
import { doCategoryReq } from '../../redux/actions/categoryActions';
import { doCurrencyReq } from '../../redux/actions/currencyActions';
import {
  doItemCreate,
  showSnackbar,
  newOfferAddMedia,
  newOfferSaveTemp,
} from '../../redux/actions/newOfferActions';
import {
  setRecord,
  deleteRecord,
  setOfferForm,
  updateSubtitles,
  setDuration,
  parseVideo,
} from '../../redux/actions/videoActions';
import { setImage, deleteImage, proceed } from '../../redux/actions/imageActions';

// components
import swal from 'sweetalert2';
import VideoRecord from './VideoRecord';
import VideoPlayer from './VideoPlayer';
import ImageEdit from './ImageEdit';
import SubtitlesEditer from './SubtitlesEditer';
import SellPageDesktop from './SellPageDesktop';
import SellPageMobile from './SellPageMobile';

// style
import './style/SellPageSwitcher.scss';

class SellPageSwitcher extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      /* open: false, */
      nextStp: 'selector',
      newOffer: this.props.newOffer.offer,
    };
    this.onDrop = this.onDrop.bind(this);
    this.onMediaProceed = this.onMediaProceed.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.changeStep = this.changeStep.bind(this);
    this.onLiveRecorded = this.onLiveRecorded.bind(this);
    this.dialog = this.dialog.bind(this);
  }

  componentDidMount() {
    this.props.getCategories();
    this.props.getCurrencies();

    // Make all active-link class empty
    const activeTabs = document.getElementsByClassName('active-link');
    if (activeTabs) {
      for (let i = 0; i < activeTabs.length; i++) {
        activeTabs[i].className = 'tab';
      }
    }

    // Make the Sell Tab Underline & Change Color
    let __sellTabLabelDom = document.querySelectorAll('#sellTabLabel>div div')[0],
      __underLine = document.querySelectorAll(
        '.tab-wrapper .tab-container > div:nth-child(2) div',
      )[0];

    if (__sellTabLabelDom && __underLine) {
      __sellTabLabelDom.className = 'active-link';
      __underLine.style.left = '33.3333%';
    }
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  changeStep(step) {
    this.setState({ nextStp: step });
  }
  onMediaProceed(mediaType) {
    let mediaURL;
    let videoSubtitles;

    switch (mediaType) {
      case 'img':
        mediaURL = this.props.image.data.location;
        videoSubtitles = '';
        this.props.onProceed();
        break;
      case 'vid':
        mediaURL = this.props.video.url;
        videoSubtitles = this.props.video.subtitles;
        this.props.onSave();
        break;
      default:
        mediaURL = '';
        videoSubtitles = '';
    }
    const mediaItem = {
      mediaType,
      mediaURL,
      videoSubtitles,
      poster: '',
    };
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    this.props.addMedia(mediaItem);
  }

  showEditImage() {
    return (
      <Grid>
        <center>
          <ImageEdit
            image_url={this.props.image.data.location}
            onDelete={this.props.onDeleteImage}
            onProceed={() => this.onMediaProceed('img')}
          />
        </center>
      </Grid>
    );
  }

  showEditVideo() {
    return (
      <div className="editVideo__container">
        <Col xs={10} xsOffset={1} className="mobile_Subtitle_View">
          <VideoPlayer
            url={this.props.video.localUrl}
            image={this.props.image.data}
            onDelete={this.props.onDelete}
            subtitles={this.props.video.subtitles}
            setDuration={this.props.setDuration}
          />

          <SubtitlesEditer
            subtitles={this.props.video.subtitles}
            onSave={() => this.onMediaProceed('vid')}
            onCancel={this.props.onDelete}
            updateSubtitles={this.props.updateSubtitles}
            videoDuration={this.props.video.videoDuration}
          />
        </Col>
      </div>
    );
  }

  openFileDialog() {
    const fileUploadDom = React.findDOMNode(this.refs.fileUpload);
    fileUploadDom.click();
  }

  onDrop(file) {
    const media = file.length > 0 ? file[0] : file;

    const formData = new FormData();
    window.clearInterval(self.intervalTrigger);
    if (media.type.includes('image/')) {
      formData.append('photos', media);
      this.props.imageUploaded(formData);
    } else if (media.type.includes('video/')) {
      const url = media.preview;
      formData.append('video', media, 'videoRecorded.webm');
      this.props.addVideo(formData, url);
      // this.props.onRecorded(formData, url);  ACZ --> No uncomment this
    } else this.setState({ open: true });
  }

  onSubmit(data, category, coords) {
    const payload = {
      alias: 'cortesa',
      category: category || 'For Sale',
      title: data.title,
      quantity: parseInt(data.quantity),
      price: parseFloat(data.price),
      description: data.description,
      media: this.props.newOffer.offer.media,
      currency: data.currency || 'USD',
      geolocation: { coords },
      paymentoptions: data.paymentOptions || 'SYS',
      private: data.certificate || false,
    };
    console.log('ACZ (Submit payload): ', payload);
    this.props.saveTempOffer(payload);
    this.props.onCreate(payload);
  }
  onLiveRecorded(data, url) {
    this.changeStep('selector');
    this.props.addVideo(data, url);
    // this.props.onRecorded(data, url); ACZ --> No uncomment this
  }

  dialog() {
    if (this.props.newOffer.success) {
      swal({
        type: 'success',
        title: 'Success! Offer created.',
      }).then((result) => {
        console.log('ACZ (result) --> ', result);
        const guid = this.props.newOffer.guid;
        browserHistory.push(`/offer/${guid}`);
      });
    }
  }

  render() {
    let output;
    if (this.state.nextStp === 'selector') {
      switch (Platform.DeviceType) {
        case 'mobile':
          output = (
            <SellPageMobile
              newOffer={this.state.newOffer}
              onDrop={this.onDrop}
              onSubmit={this.onSubmit}
              onChangeStep={this.changeStep}
            />
          );
          break;
        case 'tablet':
          output = (
            <SellPageMobile
              newOffer={this.state.newOffer}
              onDrop={this.onDrop}
              onSubmit={this.onSubmit}
              onChangeStep={this.changeStep}
            />
          );
          break;
        default:
          output = (
            <SellPageDesktop
              newOffer={this.state.newOffer}
              onDrop={this.onDrop}
              onSubmit={this.onSubmit}
              onChangeStep={this.changeStep}
            />
          );
      }
    }
    if (this.state.nextStp === 'liveVideo' && !this.props.video.recorded) {
      output = <VideoRecord startRecord onRecorded={this.onLiveRecorded} />;
    }
    if (this.props.image.loaded) {
      output = this.showEditImage();
    }
    if (this.props.video.recorded) {
      output = this.showEditVideo();
    }
    return (
      <div>
        {this.dialog()}
        {output}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const store = state;
  const image = state.image;
  const video = state.video;
  const categories = state.categories;
  const currencies = state.currencies;
  const newItem = state.newItem;
  const newOffer = state.newOffer;

  return { image, video, categories, currencies, newItem, store, newOffer };
}

function mapDispatchToProps(dispatch) {
  return {
    saveTempOffer: data => dispatch(newOfferSaveTemp(data)),
    imageUploaded: data => dispatch(setImage(data)),
    addMedia: data => dispatch(newOfferAddMedia(data)),
    addVideo: (data, url) => dispatch(parseVideo(data, url)),
    // onRecorded: (data, url) => dispatch(setRecord(data, url)), ACZ --> No uncomment this
    onDelete: () => dispatch(deleteRecord()),
    onDeleteImage: () => dispatch(deleteImage()),
    onSave: () => dispatch(setOfferForm()),
    onCreate: data => dispatch(doItemCreate(data)),
    onProceed: () => dispatch(proceed()),
    showSnackbar: () => dispatch(showSnackbar()),
    getCategories: () => dispatch(doCategoryReq()),
    getCurrencies: () => dispatch(doCurrencyReq()),
    updateSubtitles: subtitle => dispatch(updateSubtitles(subtitle)),
    setDuration: duration => dispatch(setDuration(duration)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SellPageSwitcher);
