import React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Grid, Button } from 'react-bootstrap';
import Dropzone from 'react-dropzone';
import { FlatButton, RaisedButton, IconButton } from 'material-ui';
import FontIcon from 'material-ui/FontIcon';
import { grey600, grey300, grey400 } from 'material-ui/styles/colors';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { List, ListItem } from 'material-ui/List';

import { geolocated } from 'react-geolocated';
import ReactGoogleMapLoader from 'react-google-maps-loader';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

// redux
import { doCategoryReq } from '../../redux/actions/categoryActions';

import Formsy from 'formsy-react';
import { FormsySelect, FormsyText, FormsyToggle } from 'formsy-material-ui/lib';

// components
import VideoPlayer from './VideoPlayer';

// style
import './style/SellPageDesktop.scss';

class SellPageDesktop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      category: 'Category',
      categoryToggle: false,
      canSubmit: true,
      coords: {},
      originCoords: {},
      address: '',
      place_id: '',
      geocodeResults: null,
      addrLoading: false,
      isShowVideo: false,
    };
    this.props.getCategories();
    this.onCategorySelect = this.onCategorySelect.bind(this);
    this.toggleCategory = this.toggleCategory.bind(this);
    this.enableButton = this.enableButton.bind(this);
    this.disableButton = this.disableButton.bind(this);
    this.addrHandleSelect = this.addrHandleSelect.bind(this);
    this.addrHandleChange = this.addrHandleChange.bind(this);
  }

  componentDidMount() {}

  componentWillReceiveProps(props) {
    console.log('Component Recieve props');
    this.setState({ isShowVideo: false });
    setTimeout(() => {
      this.setState({ isShowVideo: true });
    }, 2000);

    if (props.coords && !props.coords.positionError) {
      this.setState({
        coords: { lat: props.coords.latitude, lng: props.coords.longitude },
        originCoords: { lat: props.coords.latitude, lng: props.coords.longitude },
      });
      setTimeout(this.latlngToAddress(props.coords.latitude, props.coords.longitude), 1000);
    } else {
      fetch('http://ip-api.com/json')
        .then(res => res.json())
        .then((data) => {
          this.setState({
            coords: { lat: data.lat, lng: data.lon },
            originCoords: { lat: data.lat, lng: data.lon },
          });
          this.latlngToAddress(data.lat, data.lon);
        });
    }
  }

  disableButton() {
    this.setState({ canSubmit: false });
  }

  onCategorySelect(cati) {
    this.setState({
      categoryToggle: !this.state.categoryToggle,
      category: cati,
    });
  }

  enableButton() {
    const validCategory = this.state.category.includes('>');
    this.setState({ canSubmit: validCategory });
  }

  toggleCategory() {
    this.setState({ categoryToggle: !this.state.categoryToggle });
  }
  renderCategories(_categoryArray, key) {
    const __nestedMenu = [];
    if (_categoryArray && _categoryArray.length > 0) {
      for (let i = 0; i < _categoryArray.length; i++) {
        if (_categoryArray[i].items) {
          const cati = _categoryArray[i].cat;
          __nestedMenu.push(
            <MenuItem
              required
              key={key + i}
              value={_categoryArray[i].cat}
              primaryText={_categoryArray[i].text}
              primaryTogglesNestedList
              nestedItems={this.renderCategories(_categoryArray[i].items, _categoryArray[i].cat)}
              style={{ fontWeight: 'bold' }}
            />,
          );
        } else if (_categoryArray[i].text !== 'All') {
          const cati = _categoryArray[i].cat;
          __nestedMenu.push(
            <MenuItem
              required
              key={key + i}
              value={_categoryArray[i].cat}
              onClick={() => this.onCategorySelect(cati)}
              primaryText={_categoryArray[i].text}
            />,
          );
        }
      }
    }
    return __nestedMenu;
  }

  addrHandleSelect(address) {
    this.setState({
      address,
      loading: true,
    });
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(({ lat, lng }) => {
        this.setState({
          coords: { lat, lng },
          geocodeResults: 'success',
          loading: false,
        });
      })
      .catch((error) => {
        this.setState({
          geocodeResults: 'error',
          loading: false,
        });
      });
  }
  latlngToAddress(lat, lng) {
    const geocoder = new google.maps.Geocoder();
    const latlng = { lat, lng };
    geocoder.geocode({ location: latlng }, (results, status) => {
      this.setState({
        coords: { lat, lng },
        address: results[0].formatted_address,
        place_id: results[0].place_id,
      });
    });
  }
  addrHandleChange(address) {
    this.setState({
      address,
      geocodeResults: null,
    });
  }

  render() {
    const addImage = require('./assets/add-img.png');
    const noImage = require('./assets/no_video.png');
    const _newOffer = this.props.newOffer;
    const styles = {
      floatingLabelFocusStyle: {
        color: '#47a400',
      },
    };
    const addrAutocompleteItem = ({ formattedSuggestion }) => (
      <div className="GeoLoc__suggestion-item" style={{ zIndex: 3000 }}>
        <FontIcon style={{ color: '#ff0000' }} className="material-icons  GeoLoc__suggestion-icon">
          location_on
        </FontIcon>
        <span className="GeoLoc__mainText">{formattedSuggestion.mainText},</span>
        <small className="text-muted">{formattedSuggestion.secondaryText}</small>
      </div>
    );

    const cssClasses = {
      root: 'Geoloc__form-group',
      input: 'GeoLoc__search-input',
      autocompleteContainer: 'GeoLoc__autocomplete-container',
      autocompleteItemActive: 'GeoLoc__autocomplete-Item-Active',
    };

    const addrInputProps = {
      type: 'text',
      value: this.state.address,
      onChange: this.addrHandleChange,
      onBlur: () => {},
      onFocus: () => {},
      autoFocus: false,
      placeholder: 'Search Places',
      name: 'GeoLoc__input',
      id: 'my-input-id',
    };

    return (
      <div className="DesktopSellPage__container">
        <Row className="display-flex thumnail-img">
          {_newOffer.media.mediaVault.map((item, key) => {
            const media =
              item.mediaType == 'img' ? (
                <Col key={key} className="img">
                  <img src={item.mediaURL} />{' '}
                </Col>
              ) : (
                <Col key={key} className="img video-thumbnil">
                  {this.state.isShowVideo || key == _newOffer.media.mediaVault.length ? (
                    <VideoPlayer
                      url={item.mediaURL}
                      playOnHover
                      hideControls
                      muted
                      className="video-player"
                    />
                  ) : (
                    <img src={noImage} />
                  )}
                </Col>
              );
            return media;
          })}
          {_newOffer.media.mediaVault.length < 6 ? (
            <Col className="SellPage__addImgWrap">
              <Dropzone
                className="SellPage__addImg"
                accept=".jpg, .png, .bmp, .mp4, .3gp, .ogv, .webm, .flv, .wmv"
                onDrop={file => this.props.onDrop(file)}
              >
                <img src={addImage} height="50" />
              </Dropzone>
            </Col>
          ) : null}
        </Row>
        <Row style={{ textAlign: 'center' }}>
          <RaisedButton
            className="media-btn"
            label="Record Media"
            labelPosition="after"
            labelStyle={{ color: 'white' }}
            backgroundColor="#008511"
            icon={<FontIcon className="material-icons">photo_camera</FontIcon>}
            onClick={() => {
              this.props.onChangeStep('liveVideo');
            }}
            disabled={_newOffer.media.mediaVault.length >= 6}
          />
        </Row>
        <Formsy.Form
          className="sales-form"
          onValid={this.enableButton}
          onInvalid={this.disableButton}
          onValidSubmit={e => this.props.onSubmit(e, this.state.category, this.state.coords)}
        >
          <Col className="form-item">
            <FontIcon className="material-icons icon-style">loyalty</FontIcon>
            <FormsyText
              name="title"
              floatingLabelText="Title"
              requiredError="This field is required"
              required
              fullWidth
              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
            />
          </Col>
          <Col className="form-item">
            <FontIcon className="material-icons icon-style" style={{ top: '20px' }}>
              library_books
            </FontIcon>
            <FormsyText
              name="description"
              multiLine="true"
              floatingLabelText="Description"
              requiredError="This field is required"
              required
              fullWidth
              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
            />
          </Col>
          <Col className="form-list-item">
            <FontIcon className="material-icons icon-style">list</FontIcon>
            <ListItem
              primaryText={this.state.category}
              open={this.state.categoryToggle}
              className={
                this.state.category === 'Category'
                  ? 'placeholder menu-nested-list'
                  : 'menu-nested-list'
              }
              primaryTogglesNestedList
              onClick={this.toggleCategory}
              nestedItems={this.renderCategories(this.props.categories.list, 'parent')}
            />
          </Col>
          <Col className="form-item">
            <FontIcon className="material-icons icon-style">attach_money</FontIcon>
            <FormsyText
              name="price"
              floatingLabelText="Price"
              requiredError="This field is required"
              required
              fullWidth
              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
            />
          </Col>
          <Col className="form-item form-selectbox">
            <FontIcon
              style={{ color: 'green', paddingRight: '30px', fontSize: '30px' }}
              className="material-icons"
            >
              account_balance_wallet
            </FontIcon>
            <FormsySelect
              className="selectFeild form-feild"
              name="currency"
              floatingLabelText="Currency"
            >
              {this.props.currencies.rates.map((rates, index) => (
                <MenuItem value={rates.currency} primaryText={rates.currency} />
              ))}
            </FormsySelect>
          </Col>
          <Col className="form-item">
            <FontIcon className="material-icons icon-style">shopping_cart</FontIcon>
            <FormsyText
              name="quantity"
              floatingLabelText="Quantity"
              requiredError="This field is required"
              required
              fullWidth
              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
            />
          </Col>
          <div className="GeoLoc__form-item">
            <FontIcon className="material-icons icon-style GeoLoc__icon">location_on</FontIcon>
            <Col className="GeoLoc__container">
              <div className="GeoLoc__inputWrap">
                <Col xs={10} className="GeoLoc__floatText">
                  <span>Geolocation</span>
                </Col>
                <ReactGoogleMapLoader
                  params={{
                    key: 'AIzaSyA7XEFRxE4Lm28tAh44M_568fCLOP_On3k',
                    libraries: 'places, geocode',
                  }}
                  render={googleMaps =>
                    googleMaps && (
                      <div>
                        <PlacesAutocomplete
                          onSelect={this.addrHandleSelect}
                          autocompleteItem={addrAutocompleteItem}
                          onEnterKeyDown={this.addrHandleSelect}
                          classNames={cssClasses}
                          inputProps={addrInputProps}
                          highlightFirstSuggestion
                          googleLogo
                        />
                      </div>
                    )
                  }
                />
              </div>
              <IconButton
                className="current-location-icon"
                iconClassName="material-icons GeoLoc__OriginIcon"
                tooltip="Current Location"
                tooltipPosition="top-left"
                iconStyle={{ color: '#008511' }}
                onClick={() =>
                  this.latlngToAddress(this.state.originCoords.lat, this.state.originCoords.lng)
                }
              >
                gps_fixed
              </IconButton>
            </Col>
          </div>
          <Col>
            <RaisedButton
              className="send-btn"
              backgroundColor="#008511"
              labelStyle={{ color: 'white' }}
              label="Send"
              type="submit"
              disabled={!this.state.canSubmit && !this.state.category.includes('>')}
            />
          </Col>
        </Formsy.Form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const categories = state.categories;
  const currencies = state.currencies;
  return { categories, currencies };
}

function mapDispatchToProps(dispatch) {
  return {
    getCategories: () => {
      dispatch(doCategoryReq());
    },
  };
}

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(connect(mapStateToProps, mapDispatchToProps)(SellPageDesktop));
