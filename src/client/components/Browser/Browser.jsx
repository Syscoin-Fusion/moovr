import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Row, Col } from 'react-bootstrap';
import FontIcon from 'material-ui/FontIcon';
import ReactPaginate from 'react-paginate';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import Config from 'configEnv';

// redux
import offerActions from '../../redux/actions/offerActions';
import { doCategoryReq } from '../../redux/actions/categoryActions';
import { newOfferReset } from '../../redux/actions/newOfferActions';
import viewActions from '../../redux/actions/viewActions';

import SearchBrowser from '../Browser/SearchBrowser';
import MobileLanding from '../../UIMobile/landing/MobileLanding';
import ListBrowser from './ListBrowser';
import BrowserCarousel from './BrowserCarousel';
import FilterBrowser from './FilterBrowser';
import CatBreadcrums from './CatBreadcrums';
import Test from '../Test';

import './styles/Browser.scss';

import GlobalIcon from './assets/icon_map.svg';

const { filtersByMedia, filtersByPayment } = Config.Browser;

class Browser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      view: 'Grid',
      items: [],
      current: 0,
      open: false,
      regexp: '',
      safesearch: 'No',
      anchorOrigin: {
        horizontal: 'left',
        vertical: 'bottom'
      },
      targetOrigin: {
        horizontal: 'left',
        vertical: 'top'
      },
      categoryFilter: 'All',
      sortBy: 'Title: ascending',
      perPage: 10,
      paginationActivePage: 1
    };
    this.handleNewCategory = this.handleNewCategory.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleView = this.handleView.bind(this);
    this.handleSort = this.handleSort.bind(this);
    this.handlePerPage = this.handlePerPage.bind(this);
    this.showNextPage = this.showNextPage.bind(this);
    this.handlePageClick = this.handlePageClick.bind(this);
    this.showPreviousPage = this.showPreviousPage.bind(this);
    // ACZ --> think about carousell and fetured
    this.getOffersWithImages = this.getOffersWithImages.bind(this);
    this.handleActivePage = this.handleActivePage.bind(this);
    //---------------------------------------------
  }

  componentDidMount() {
    const { filter } = this.props.offers;
    filter.offerId = '';
    this.props.newOfferReset();
    this.props
      .sortOffersNew(filter)
      .then(() => {})
      .catch(err => {
        console.log(err);
      });
    // Make all active-link class empty
    const activeTabs = document.getElementsByClassName('active-link');
    if (activeTabs) {
      for (let i = 0; i < activeTabs.length; i++) {
        activeTabs[i].className = 'tab';
      }
    }

    // Enable Browser Tab Underline
    let __buyTabLabelDom = document.querySelectorAll('#buyTabLabel>div div')[0],
      __underLine = document.querySelectorAll(
        '.tab-wrapper .tab-container > div:nth-child(2) div'
      )[0];

    if (__buyTabLabelDom && __underLine) {
      __buyTabLabelDom.className = 'active-link';
      __underLine.style.left = '0%';
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      view: nextProps.view.layout
    });
  }

  handleNewCategory(event, key, value) {
    // Getting Selected Category here
    const { filter } = this.props.offers;
    this.setState({ categoryFilter: value });
    filter.category = value;
    filter.activePage = 1;
    this.props.sortOffersNew(filter);
  }

  handleSearch(data) {
    const { filter } = this.props.offers;
    filter.regExp = data.search;
    filter.activePage = 1;
    this.props.sortOffersNew(filter);
  }

  handleView(event, index, value) {
    /* this.setState({ view: value }); */
    const { filter } = this.props.offers;
    const nFilter = {
      ...filter,
      offerId: '',
      radius: 0
    };
    this.props.sortOffersNew(nFilter);
    this.props.changeView(value);
    if (value == 'Map') {
      document.getElementsByTagName('body')[0].className = 'map-view';
    } else {
      document.getElementsByTagName('body')[0].className = '';
    }
  }

  handleSort(event, index, value) {
    // TBAUSTIN -- SHOULD BE DONE
    const { filter } = this.props.offers;
    filter.sort = value;
    filter.activePage = 1;
    this.props.sortOffersNew(filter);
  }

  handlePerPage(event, index, value) {
    this.setState({ perPage: value });
    const { filter } = this.props.offers;
    filter.itemPerPage = value;
    filter.activePage = 1;
    this.props.sortOffersNew(filter);
  }

  showNextPage(event) {
    event.preventDefault();
    const { filter } = this.props.offers;
    if (filter.activePage < filter.totalPages) {
      filter.activePage += 1;
    }

    this.props
      .sortOffersNew(filter)
      .then(() => {})
      .catch(err => {
        console.log(err);
      });
  }

  handlePageClick(activePage) {
    console.log('ACZ (activePage): ', activePage);
    const { filter } = this.props.offers;
    filter.activePage = activePage.selected + 1;
    this.props
      .sortOffersNew(filter)
      .then(() => {})
      .catch(err => {
        console.log(err);
      });
  }

  showPreviousPage(event) {
    event.preventDefault();
    const { filter } = this.props.offers;
    if (filter.activePage > 1) {
      filter.activePage -= 1;
    }

    this.props
      .sortOffersNew(filter)
      .then(() => {})
      .catch(err => {
        console.log(err);
      });
  }

  // ACZ --> think about carousell and fetured
  getOffersWithImages(items) {
    const itemsImg = [];
    items.map((item, i) => {
      const media = item.media.mediaVault;
      if (media.length > 0) {
        itemsImg.push(item);
      }
    });
    return itemsImg;
  }
  //---------------------------------------------

  handleActivePage(event, key, value) {
    const { filter } = this.props.offers;
    filter.activePage = value;
    this.props.sortOffersNew(filter);
  }

  render() {
    const { browser, offers } = this.props;
    const { layout } = this.props.view;
    const props = Object.assign({}, this.props);
    const featuredOffers = this.props.featured.list;
    // delete props.categories;
    // delete props.getCategories;
    return (
      <div className="Browser__container">
        {window.location.pathname === '/' ? (
          <div>
            <div className="Browser__wrap">
              <Col xs={12}>
                <Row>
                  {offers.list.length > 0 && this.state.view !== 'Map' ? (
                    <BrowserCarousel featuredOffers={featuredOffers} />
                  ) : null}
                </Row>
              </Col>
            </div>
            <div className="row inline-block body_container">
              <Col>
                <div className="Browser__searchContainer">
                  <Col xs={12}>
                    <CatBreadcrums
                      categoryFilter={this.state.categoryFilter}
                      categoryList="{}"
                      handleNewCategory={this.handleNewCategory}
                      className="category-breadcrumb"
                    />
                    <SearchBrowser onSubmit={this.handleSearch} />
                    <FilterBrowser mediaFilter={filtersByMedia} paymentFilter={filtersByPayment} />
                  </Col>
                  <Row>
                    <Col lg={12} xs={12} sm={12} className="no-padding">
                      {layout === 'Map' ? null : (
                        <Col lg={3} sm={3} xs={3} className="filter-dropdown">
                          <DropDownMenu
                            value={this.state.sortBy}
                            onChange={this.handleSort}
                            disabled={this.state.view === 'Map'}
                          >
                            <MenuItem
                              value={'Title: ascending'}
                              primaryText={
                                <span className="filter-option">
                                  <span className="hidden-xs">Sort By Title</span>
                                  <span className="hidden-lg hidden-md hidden-sm">Sort Title</span>
                                  <FontIcon className="material-icons">keyboard_arrow_up</FontIcon>
                                </span>
                              }
                            />
                            <MenuItem
                              value={'Title: descending'}
                              primaryText={
                                <span className="filter-option">
                                  <span className="hidden-xs">Sort By Title</span>
                                  <span className="hidden-lg hidden-md hidden-sm">Sort Title</span>
                                  <FontIcon className="material-icons">
                                    keyboard_arrow_down
                                  </FontIcon>
                                </span>
                              }
                            />
                            <MenuItem
                              value={'Price: ascending'}
                              primaryText={
                                <span className="filter-option">
                                  <span className="hidden-xs">Sort By Price</span>
                                  <span className="hidden-lg hidden-md hidden-sm">Sort Price</span>
                                  <FontIcon className="material-icons">keyboard_arrow_up</FontIcon>
                                </span>
                              }
                            />
                            <MenuItem
                              value={'Price: descending'}
                              primaryText={
                                <span className="filter-option">
                                  <span className="hidden-xs">Sort By Price</span>
                                  <span className="hidden-lg hidden-md hidden-sm">Sort Price</span>
                                  <FontIcon className="material-icons">
                                    keyboard_arrow_down
                                  </FontIcon>
                                </span>
                              }
                            />
                          </DropDownMenu>
                        </Col>
                      )}

                      {layout === 'Map' ? null : (
                        <Col
                          lg={3}
                          sm={3}
                          xs={3}
                          className="filter-dropdown"
                          style={{ textAlign: 'left !important' }}
                        >
                          <DropDownMenu
                            value={this.state.perPage}
                            onChange={this.handlePerPage}
                            disabled={this.state.view === 'Map'}
                          >
                            <MenuItem
                              value={10}
                              primaryText={
                                <span className="filter-option">
                                  <span className="hidden-xs">10 Per Page</span>
                                  <span className="hidden-lg hidden-md hidden-sm">10 Offers</span>
                                </span>
                              }
                            />
                            <MenuItem
                              value={20}
                              primaryText={
                                <span className="filter-option">
                                  <span className="hidden-xs">20 Per Page</span>
                                  <span className="hidden-lg hidden-md hidden-sm">20 Offers</span>
                                </span>
                              }
                            />
                            <MenuItem
                              value={30}
                              primaryText={
                                <span className="filter-option">
                                  <span className="hidden-xs">30 Per Page</span>
                                  <span className="hidden-lg hidden-md hidden-sm">30 Offers</span>
                                </span>
                              }
                            />
                          </DropDownMenu>
                        </Col>
                      )}

                      <Col
                        lg={3}
                        sm={3}
                        xs={3}
                        lgOffset={layout === 'Map' ? 8 : 0}
                        smOffset={layout === 'Map' ? 8 : 0}
                        xsOffset={layout === 'Map' ? 9 : 0}
                        className="text-right map-filter filter-dropdown"
                      >
                        <DropDownMenu value={this.state.view} onChange={this.handleView}>
                          <MenuItem
                            value={'Map'}
                            primaryText={
                              <span className="filter-option">
                                <span>Map</span>
                                <FontIcon className="material-icons">explore</FontIcon>
                              </span>
                            }
                          />
                          <MenuItem
                            value={'Grid'}
                            primaryText={
                              <span className="filter-option">
                                <span>Grid</span>
                                <FontIcon className="material-icons">grid_on</FontIcon>
                              </span>
                            }
                          />
                          <MenuItem
                            value={'List'}
                            primaryText={
                              <span className="filter-option">
                                <span>List</span>
                                <FontIcon className="material-icons">list</FontIcon>
                              </span>
                            }
                          />
                        </DropDownMenu>
                      </Col>

                      {layout === 'Map' ? null : (
                        <Col lg={3} sm={3} xs={3} className="text-right filter-dropdown">
                          <DropDownMenu
                            value={offers.filter.activePage}
                            onChange={this.handleActivePage}
                            disabled={this.state.view === 'Map'}
                          >
                            {[...Array(offers.filter.totalPages)].map((e, i) => (
                              <MenuItem
                                key={i + 1}
                                value={i + 1}
                                primaryText={
                                  <span className="filter-option">
                                    <span className="hidden-xs">{`Page ${i + 1} of ${offers.filter
                                      .totalPages || 1}`}</span>
                                    <span className="hidden-lg hidden-md hidden-sm">{`Page ${i +
                                      1} / ${offers.filter.totalPages || 1}`}</span>
                                  </span>
                                }
                              />
                            ))}
                          </DropDownMenu>
                        </Col>
                      )}
                    </Col>
                  </Row>
                </div>

                <Col xs={12} md={12} mdOffset={0} className="items-container">
                  <ListBrowser offers={offers.list} view={this.state.view} />
                </Col>
                {this.state.view !== 'Map' && (
                  <Col xs={12} lg={12} className="xs-no-padding" style={{ textAlign: 'center' }}>
                    <ReactPaginate
                      previousLabel={
                        <span onClick={this.showPreviousPage.bind(this)}>
                          <FontIcon
                            className="material-icons"
                            style={{
                              color: '#000',
                              fontSize: '40px',
                              bottom: '8px',
                              fontWeight: '100'
                            }}
                          >
                            chevron_left
                          </FontIcon>
                          <span style={{ verticalAlign: 'top' }}>Previous</span>
                        </span>
                      }
                      nextLabel={
                        <span onClick={this.showNextPage.bind(this)}>
                          <span style={{ verticalAlign: 'top' }}>Next</span>
                          <FontIcon
                            className="material-icons"
                            style={{
                              color: '#000',
                              fontSize: '40px',
                              bottom: '8px',
                              fontWeight: '100'
                            }}
                          >
                            chevron_right
                          </FontIcon>
                        </span>
                      }
                      forcePage={this.props.offers.filter.activePage - 1}
                      breakLabel={<a href="">.</a>}
                      breakClassName={'break-me'}
                      pageCount={Number(this.props.offers.filter.totalPages)}
                      marginPagesDisplayed={2}
                      pageRangeDisplayed={5}
                      onPageChange={this.handlePageClick}
                      containerClassName={'pagination'}
                      subContainerClassName={'pages pagination'}
                      activeClassName={'active'}
                    />
                  </Col>
                )}
              </Col>
            </div>
          </div>
        ) : (
          <MobileLanding />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  // const sorter = state.sorter;
  // const categories = state.categories;
  // const filterOption = state.sorter.option;
  // const itemList = state.sorter.filter;
  // const payment = state.sorter.payment;
  const { offers, featured, view } = state;

  return {
    // sorter,
    // categories,
    // payment,
    // filterOption,
    // itemList,
    offers,
    featured,
    view
  };
}

function mapDispatchToProps(dispatch) {
  return {
    newOfferReset: () => dispatch(newOfferReset()),
    sortOffersNew: params => dispatch(offerActions.sortOffers(params)),
    getOffers: params => dispatch(offerActions.getOffers(params)),
    changeView: view => dispatch(viewActions.changeView(view))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Browser);
