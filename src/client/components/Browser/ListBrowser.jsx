import React from 'react';
import { Row } from 'react-bootstrap';
import OfferMap from './Map';

import ListViewItemsBrowser from './ListViewItemsBrowser';
import GridsViewItemsBrowser from './GridsViewItemsBrowser';

import './styles/listBrowser.scss';

class ListBrowser extends React.Component {
  constructor(props) {
    super(props);
    this.getErrorHeader = this.getErrorHeader.bind(this);
  }

  getErrorHeader() {
    return (
      <h3>{'No Offers to display, you have to perform a new search or just change the filter'}</h3>
    );
  }

  render() {
    let renderCode;
    switch (this.props.view) {
      case 'Map':
        renderCode = <OfferMap offers={this.props.offers} />;
        break;
      case 'Grid':
        renderCode = <GridsViewItemsBrowser items={this.props.offers} />;
        break;
      case 'List':
        renderCode = <ListViewItemsBrowser items={this.props.offers} />;
        break;
      default:
        renderCode = <ListViewItemsBrowser items={this.props.offers} />;
        break;
    }
    return (
      <Row className="listBrowser__wrap">
        {this.props.offers.length < 1 && this.getErrorHeader()}
        {renderCode}
      </Row>
    );
  }
}

export default ListBrowser;
