import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router';
import Paper from 'material-ui/Paper';

import ItemBrowser from './ItemBrowser';

import './styles/GirdsViewItemsBrowser.scss';

class GridsViewItemsBrowser extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const items = this.props.items.map((item, index) => {
      const price = item.sysprice.toString();
      const title = item.title;
      return (
        <li className="cards__item" key={item.offer}>
          <Link to={`/offer/${item.offer}`}>
            <div className="card">
              <div className="card__image">
                <ItemBrowser data={item} />
              </div>
              <div className="card__content">
                <div className="card__title">
                  {title.length > 30 ? `${title.substr(0, 30)}...` : `${title}`}
                </div>
                <div className="card__price">{`${price} SYS`}</div>
              </div>
            </div>
          </Link>
        </li>
      );
    });

    return <ul className="gridsViewItemsBrowser__wrap cards">{items}</ul>;
  }
}

export default GridsViewItemsBrowser;
