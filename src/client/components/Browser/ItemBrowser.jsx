import React from 'react';

// components
import { Row, Col, Grid, Button, Glyphicon } from 'react-bootstrap';
import VideoPlayer from '../Store/VideoPlayer';
import GaleryItemBrowser from './GaleryItemBrowser';

require('./styles/itemBrowser.scss');

const styles = {
  infoContainer: {
    position: 'absolute',
    zIndex: '1',
    padding: '20px',
  },
  link: {
    color: '#fff',
    textShadow: '1px 1px #000',
    fontSize: '24px',
  },
  currency: {
    color: '#fff',
    textShadow: '1px 1px #000',
  },
};

function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

class ItemBrowser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mediaType: '',
      videoSubtitles: [],
      mediaURL: '',
    };

    this.prepareData = this.prepareData.bind(this);
  }

  componentWillMount() {
    this.prepareData(this.props);
  }

  componentWillReceiveProps(nxtProps) {
    this.prepareData(nxtProps);
  }

  prepareData(props) {
    const offer = props.data;
    let mediaType = 'txt';
    let mediaURL = 'noURL';
    // Should be ready - tbaustinedit
    if (offer.media.mediaVault.length > 0) {
      const defaultIdx = offer.media.defaultIdx;

      if (offer.media.mediaVault[defaultIdx] != null) {
        mediaType = offer.media.mediaVault[defaultIdx].mediaType;
        mediaURL = offer.media.mediaVault[defaultIdx].mediaURL;
      }

      const videoSubtitles =
        mediaType === 'vid' ? offer.media.mediaVault[defaultIdx].videoSubtitles : [];

      this.setState({
        mediaURL,
        mediaType,
        videoSubtitles,
      });
    }
    this.setState({
      mediaType,
    });
  }

  render() {
    return (
      <div className="containerItemBrowser">
        <div className="contentItemBrowser">
          <div className="bgContainer">
            {this.state.mediaType === 'vid' && (
              <VideoPlayer
                url={this.state.mediaURL}
                subtitles={this.state.videoSubtitles}
                playOnHover
                hideControls
                muted
              />
            )}
            {this.state.mediaType === 'img' && <GaleryItemBrowser mediaURL={this.state.mediaURL} />}
            {this.state.mediaType === 'txt' && (
              <GaleryItemBrowser mediaURL={require('./assets/no_image.png')} />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default ItemBrowser;
