import React from 'react';
import { Col } from 'react-bootstrap';

// style
import './styles/Message.scss';

// redux

class Message extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    // Make all active-link class empty
    const activeTabs = document.getElementsByClassName('active-link');
    if (activeTabs) {
      for (let i = 0; i < activeTabs.length; i++) {
        activeTabs[i].className = 'tab';
      }
    }

    // Make the Sell Tab Underline & Change Color
    const __buyTabLabelDom = document.querySelectorAll('#buyTabLabel>div div')[0];
    const __messageTabLabelDom = document.querySelectorAll('#messageTabLabel>div div')[0];
    const __underLine = document.querySelectorAll(
      '.tab-wrapper .tab-container > div:nth-child(2) div',
    )[0];

    if (__messageTabLabelDom && __underLine) {
      __messageTabLabelDom.className = 'active-link';
      __underLine.style.left = '66.6666%';
    }
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  render() {
    return (
      <Col xs={12} style={{ marginTop: '10%', fontSize: '30px', textAlign: 'center' }}>
        To be implemented
      </Col>
    );
  }
}
export default Message;
