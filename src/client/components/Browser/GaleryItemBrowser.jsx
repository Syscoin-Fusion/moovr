import React from 'react';

// components
import { Row, Col, Grid, Button, Glyphicon } from 'react-bootstrap';
/*
class GaleryItemBrowser extends React.Component { */
const GaleryItemBrowser = props => (
  /* render() {
    return ( */
  <Row>
    <Col xs={12}>
      <img
        alt=""
        style={{ width: '100%' }}
        src={props.mediaURL}
      />
    </Col>
  </Row>
);
  /* }
} */

export default GaleryItemBrowser;
