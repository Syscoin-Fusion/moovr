import Browser from './Browser';
import Message from './Message';

const BrowserPage = {
  Browser,
  Message,
};

export default BrowserPage;
