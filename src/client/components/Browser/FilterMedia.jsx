import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { Checkbox } from 'material-ui';

import offerActions from '../../redux/actions/offerActions';

import CameraIcon from './assets/icon_camera.svg';
import DocumentIcon from './assets/icon_edit.svg';
import PlayIcon from './assets/icon_play.svg';
import MapIcon from './assets/avatar.svg';

const styles = {
  filter: {
    padding: '5px 10px',
    color: 'rgba(0, 0, 0, 0.298039)',
    fontSize: '16px',
    fontWeight: 'bold',
    display: 'inline-block'
  },
  label: {
    padding: '0 0 0 -20px',
    fontFamily: 'Roboto Light',
    fontSize: '16px'
  },
  icon: {
    fill: '#619d3d'
  }
};

class FilterMedia extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mediaFilter: ''
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event, isInputChecked) {
    const { filter } = this.props.offers;
    const mediaFilter = isInputChecked ? event.target.id : '';
    this.setState({ mediaFilter });
    filter.mediaType = mediaFilter;
    filter.activePage = 1;
    this.props.sortOffersNew(filter);
  }
  renderIcon(label) {
    switch (label) {
      case 'Photos-only':
        return (
          <span>
            <span>
              <img src={CameraIcon} className="large" />
            </span>
            <span className="search-title">{this.props.children}</span>
          </span>
        );
      case 'Text-only':
        return (
          <span>
            <span>
              <img src={DocumentIcon} className="spaced" />
            </span>
            <span className="search-title">{this.props.children}</span>
          </span>
        );
      case 'Videos-only':
        return (
          <span>
            <span>
              <img src={PlayIcon} className="large" />
            </span>
            <span className="search-title">{this.props.children}</span>
          </span>
        );
      case 'Map-only':
        return (
          <span>
            <span>
              <img src={MapIcon} />
            </span>
            <span className="search-title">{this.props.children}</span>
          </span>
        );
      default:
        return <span />;
    }
  }
  renderId(label) {
    switch (label) {
      case 'Photos-only':
        return 'img';
      case 'Text-only':
        return 'txt';
      case 'Videos-only':
        return 'vid';
      case 'Audio-only':
        return 'aud';
      case 'Map-only':
        return 'map';
      default:
        return '';
    }
  }
  render() {
    const input_label = this.props.children.split(' ').join('-');

    if (input_label !== 'All') {
      return (
        <li>
          <Checkbox
            id={this.renderId(input_label)}
            label={this.renderIcon(input_label)}
            labelStyle={styles.label}
            iconStyle={styles.icon}
            disableTouchRipple
            checked={this.props.offers.filter.mediaType === this.renderId(input_label)}
            onCheck={this.handleClick}
          />
        </li>
      );
    }
    return null;
  }
}

const mapStateToProps = state => ({
  offers: state.offers
});

const mapDispatchToProps = dispatch => ({
  sortOffersNew: params => dispatch(offerActions.sortOffers(params))
});

export default connect(mapStateToProps, mapDispatchToProps)(FilterMedia);
