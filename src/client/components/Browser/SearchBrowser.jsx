import React from 'react';

import { Row, Col, Grid, Button, Glyphicon } from 'react-bootstrap';
import Formsy from 'formsy-react';
import { FormsyText } from 'formsy-material-ui/lib';
import FontIcon from 'material-ui/FontIcon';
import './styles/form-browser.scss';


class SearchBrowser extends React.Component {
  constructor(props) {
    super(props);

    this.goSearch = this.goSearch.bind(this);
  }

  goSearch(data) {
    this.props.onSubmit(data);
  }



  render() {
    return (
      <Formsy.Form style={{ marginTop: '10px' }} onSubmit={this.goSearch}>
        <Row style={{ marginLeft: '0 !important', marginRight: '0 !important' }}>
          <Col>
            <div className="Search__input-wrapper">
              <FontIcon className="material-icons">search</FontIcon>
              <FormsyText
                name="search"
                style={{ textIndent: 30 }}
                inputStyle={{ width: '97%' }}
                hintText="Search"
              />
            </div>
          </Col>
        </Row>
      </Formsy.Form>
    );
  }
}

export default SearchBrowser;
