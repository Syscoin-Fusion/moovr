import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import { Carousel } from 'react-responsive-carousel';

// components
import { Row, Col, Grid, Button, Glyphicon } from 'react-bootstrap';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

// components
import VideoPlayer from '../Store/VideoPlayer.jsx';

import './styles/BrowserCarrousel.scss';

class BrowserCarousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      paused: false,
      showCarousel: true,
    };
    this.toggleCarousel = this.toggleCarousel.bind(this);
    this.featuredOffersArray = this.props.featuredOffers;
  }

  toggleCarousel() {
    this.setState({ showCarousel: !this.state.showCarousel });
  }

  renderSliders() {
    const silderContent = [];
    this.featuredOffersArray.map((item) => {
      const itemObject = {
        title: item.title,
        offer: item.offer,
        is_redirectOffer: item.offerlink,
        description: item.description,
        price: item.sysprice,
        currency: item.currency,
      };
      if (item.media.mediaVault.length > 0) {
        itemObject.media = item.media.mediaVault[item.media.defaultIdx];
      } else {
        itemObject.media = { mediaType: 'img', mediaURL: '../assets/no_image.1.png' };
      }
      silderContent.push(itemObject);
    });
    return silderContent.map((item, key) => (
      <Col key={key}>
        <Col lg={5} xs={12} sm={6} className="slider-image" ref="slides">
          <div className="slider-image">
            {item.media.mediaType === 'img' ? (
              <img src={item.media.mediaURL} className="img-responsive" />
            ) : (
              <VideoPlayer url={item.media.mediaURL} playOnHover hideControls muted />
            )}
          </div>
        </Col>
        <Col lg={5} xs={12} sm={6} className="slider-info" ref="slide-info">
          <h2 className="slider-title">{item.title.substring(0, 85)}</h2>
          <h3 className="slider-title__small">
            {' '}
            From: {item.price} {item.currency}{' '}
          </h3>
          <p className="slider-description">{item.description.substring(0, 150)}</p>
          <Link to={`/offer/${item.offer}`} className="slider-btn">
            <RaisedButton label="View Offer" className="btn-green" />
          </Link>
        </Col>
      </Col>
    ));
  }

  render() {
    const settings = {
      dots: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 2000,
      slidesToShow: 1,
      slidesToScroll: 1,
      accessibility: true,
      centerMode: true,
      arrows: true,
    };
    return (
      <div>
        <div className="BrowserCarousel__wraper">
          {!this.state.showCarousel && (
            <div className="BrowserCarousel__openContainer">
              <IconButton
                className="BrowserCarousel__openButton"
                style={{zIndex:'unset'}}
                onClick={this.toggleCarousel}
                tooltip="Open"
                touch
                tooltipPosition="top-center"
              >
                <FontIcon
                  className="material-icons BrowserCarousel__sliderContainer--bold"
                  color={'#5FA02F'}
                >
                  keyboard_arrow_down
                </FontIcon>
              </IconButton>
            </div>
          )}
          {this.state.showCarousel && (
            <div className="BrowserCarousel__sliderContainer">
              <div className="BrowserCarousel__close">
                <IconButton
                style={{zIndex:'unset'}}
                  onClick={this.toggleCarousel}
                  tooltip="Close"
                  touch
                  tooltipPosition="top-center"
                >
                  <FontIcon
                    className="material-icons BrowserCarousel__sliderContainer--bold"
                    color={'#5FA02F'}
                  >
                    keyboard_arrow_up
                  </FontIcon>
                </IconButton>
              </div>
              <Col xs={12} className="carousel__div">
                <Carousel
                  showArrows={false}
                  showStatus={false}
                  showThumbs={false}
                  infiniteLoop
                  autoPlay
                  interval={6000}
                >
                  {this.renderSliders()}
                </Carousel>
              </Col>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default BrowserCarousel;
