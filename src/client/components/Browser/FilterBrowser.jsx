import React from 'react';
import { connect } from 'react-redux';
import FontIcon from 'material-ui/FontIcon';
import { Checkbox } from 'material-ui';
import Platform from 'react-platform-js';
import { Col } from 'react-bootstrap';

import FilterMedia from './FilterMedia';

import offerActions from '../../redux/actions/offerActions';

const styles = {
  dropdown: {
    top: '85px',
    right: '10px',
    position: 'absolute',
  },
  label: {
    padding: '0',
    fontFamily: 'Roboto Light',
    fontSize: '16px',
  },
  icon: {
    fill: '#619d3d',
  },
};

class FilterBrowser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleAdvanceSearch: true,
      paymentFilter: '',
      mediaFilter: '',
    };

    this.handleToggleSearch = this.handleToggleSearch.bind(this);
    this.handlePaymentFilter = this.handlePaymentFilter.bind(this);
  }

  handlePaymentFilter(event, isInputChecked) {
    const { filter } = this.props.offers;
    console.log('ACZ (isInputChecked) --> ', isInputChecked);
    const paymentFilter = isInputChecked ? event.target.id : '';
    this.setState({ paymentFilter });
    filter.paymentoptions_display = paymentFilter;
    filter.activePage = 1;

    this.props.sortOffersNew(filter);
  }

  handleToggleSearch() {
    this.setState({ toggleAdvanceSearch: !this.state.toggleAdvanceSearch });
  }

  render() {
    const mediaFilters = this.props.mediaFilter.map((filter, i) => (
      <FilterMedia key={i} filter={filter.value}>
        {filter.name}
      </FilterMedia>
    ));
    const isMobile = Platform.DeviceType === 'mobile';
    return (
      <div className="advanced-search">
        {isMobile ? (
          <FontIcon
            className="material-icons"
            color="#827866"
            style={styles.dropdown}
            onClick={e => this.handleToggleSearch(e)}
          >
            more_vert
          </FontIcon>
        ) : (
          <p className="advance-search-text" onClick={e => this.handleToggleSearch(e)}>
            Advanced Search{' '}
            {this.state.toggleAdvanceSearch ? (
              <FontIcon className="material-icons">arrow_drop_down</FontIcon>
            ) : (
              <FontIcon className="material-icons">arrow_drop_up</FontIcon>
            )}
          </p>
        )}

        <Col xs={12} className={`search-params ${this.state.toggleAdvanceSearch ? 'hide' : ''}`}>
          <h4>Advanced:</h4>
          <ul className="search-type radio wider">{mediaFilters}</ul>
          <h4>Payment Type:</h4>
          <div>
            <ul className="search-type radio wider ">
              <li>
                <Checkbox
                  id="sys"
                  label="SYS"
                  labelStyle={styles.label}
                  iconStyle={styles.icon}
                  disableTouchRipple
                  checked={this.state.paymentFilter === 'sys'}
                  onCheck={this.handlePaymentFilter}
                />
              </li>
              <li>
                <Checkbox
                  id="btc"
                  label="BTC"
                  labelStyle={styles.label}
                  iconStyle={styles.icon}
                  disableTouchRipple
                  checked={this.state.paymentFilter === 'btc'}
                  onCheck={this.handlePaymentFilter}
                />
              </li>
              <li>
                <Checkbox
                  id="zec"
                  label="ZEC"
                  labelStyle={styles.label}
                  iconStyle={styles.icon}
                  disableTouchRipple
                  checked={this.state.paymentFilter === 'zec'}
                  onCheck={this.handlePaymentFilter}
                />
              </li>
            </ul>
          </div>
        </Col>
      </div>
    );
  }
}

const stateToProps = state => ({
  // filterOption: state.sorter.option,
  // sortOption: state.sorter.sortOption,
  offers: state.offers,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  sortOffersNew: params => dispatch(offerActions.sortOffers(params)),
});

export default connect(stateToProps, mapDispatchToProps)(FilterBrowser);
