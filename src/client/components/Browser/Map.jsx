/* global google */
import { default as React, Component } from 'react';
import { connect } from 'react-redux';

import { compose, withProps, lifecycle } from 'recompose';
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from 'react-google-maps';
import geolib from 'geolib';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';

const { MarkerClusterer } = require('react-google-maps/lib/components/addons/MarkerClusterer');

import { geolocated } from 'react-geolocated';
import { Col } from 'react-bootstrap';

const { SearchBox } = require('react-google-maps/lib/components/places/SearchBox');
const _ = require('lodash');

import './styles/Map.scss';
import ItemList from './ItemList';
import offerActions from '../../redux/actions/offerActions';

const googleMapURL =
  'https://maps.googleapis.com/maps/api/js?libraries=places,geometry&key=AIzaSyA7XEFRxE4Lm28tAh44M_568fCLOP_On3k';

const googleMapBounds = markers => {
  const bounds = new google.maps.LatLngBounds();
  markers.map(marker => {
    const loc = new google.maps.LatLng(marker.position.lat, marker.position.lng);
    bounds.extend(loc);
  });
  return bounds;
};

const GeolocationGoogleMap = compose(
  withProps({
    googleMapURL,
    loadingElement: <div style={{ height: '100%' }} />,
    containerElement: <div style={{ height: '100%' }} />,
    mapElement: <div style={{ height: '100%' }} />
  }),
  lifecycle({
    componentWillMount() {
      const refs = {};
      this.setState({
        onMapMounted: ref => {
          this.props.onMapMounted(ref);
          refs.map = ref;
          this.setState({
            refs
          });
        },
        onZoomChanged: () => {
          const refsZoom = this.state.refs;
          const newZoom = refsZoom.map.getZoom();
          this.props.onNewZoom(newZoom);
        },

        onSearchBoxMounted: ref => {
          refs.searchBox = ref;
          this.setState({
            refs
          });
        },
        onPlacesChanged: () => {
          const { refs } = this.state;
          const places = refs.searchBox.getPlaces();
          const bounds = new google.maps.LatLngBounds();

          // console.log('ACZ (Map) --> ', this.state.refs.map);

          this.props.closeAllInfoBox();

          places.forEach(place => {
            if (place.geometry.viewport) {
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          const nextMarkers = places.map(place => ({
            position: place.geometry.location
          }));
          const nextCenter = _.get(nextMarkers, '0.position', this.state.center);

          this.setState({
            center: nextCenter
          });
          // refs.map.fitBounds(bounds);
        },
        onClusterClick: cluster => {
          const refCluster = this.state.refs;
          const zoomLevel = refCluster.map.getZoom();
          this.props.onMarkerClustererClick(cluster, zoomLevel);
        }
      });
    },
    componentDidUpdate() {
      const refs = this.state.refs;
      if (refs) {
        const bounds = googleMapBounds(refs.map.props.markers);
        /* refs.map.fitBounds(bounds); */
      }
      // console.log('ACZ (Map Zoom): ', refs.map.props.center);
    }
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap
    ref={props.onMapMounted}
    defaultZoom={8}
    zoom={props.zoom}
    center={props.center}
    markers={props.markers}
    onClick={props.onMapClick}
    onZoomChanged={props.onZoomChanged}
  >
    <SearchBox
      ref={props.onSearchBoxMounted}
      bounds={props.bounds}
      controlPosition={google.maps.ControlPosition.TOP_LEFT}
      onPlacesChanged={props.onPlacesChanged}
    >
      <input className="map_search_location" type="text" placeholder="Search Location" />
    </SearchBox>
    {props.markers.map((marker, index) => {
      const onClick = () => props.onMarkerClick(marker);
      const onCloseClick = () => props.onCloseClick(marker);

      if (marker.type === 'userPos') {
        return (
          <Marker
            key={'userPos'}
            position={marker.position}
            title={"User's Location"}
            options={{ icon: require('./assets/blueDot.png') }}
          >
            {props.showUserInfo && (
              <InfoWindow>
                <div>User's Location</div>
              </InfoWindow>
            )}
          </Marker>
        );
      }
    })}
    <MarkerClusterer
      onClick={props.onClusterClick}
      averageCenter
      enableRetinaIcons
      gridSize={60}
      maxZoom={20}
    >
      {props.markers.map((marker, index) => {
        const onClick = () => props.onMarkerClick(marker);
        const onCloseClick = () => props.onCloseClick(marker);
        if (marker.type === 'cluster') {
          return (
            <Marker
              key={index}
              position={marker.position}
              onClick={onClick}
              options={{ icon: 'https://image.ibb.co/evMHxF/shopping_zone_marker_1.png' }}
            >
              {marker.showInfo && (
                <InfoWindow onCloseClick={onCloseClick}>
                  <div>{marker.markers.map((mark, i) => <ItemList marker={mark} key={i} />)}</div>
                </InfoWindow>
              )}
            </Marker>
          );
        } else if (marker.type === 'single') {
          return (
            <Marker
              key={index}
              position={marker.position}
              onClick={onClick}
              options={{ icon: 'https://image.ibb.co/evMHxF/shopping_zone_marker_1.png' }}
            >
              {marker.showInfo && (
                <InfoWindow onCloseClick={onCloseClick}>
                  <div>
                    <ItemList marker={marker} />
                  </div>
                </InfoWindow>
              )}
            </Marker>
          );
        }
      })}
    </MarkerClusterer>
  </GoogleMap>
));

class OfferMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPosition: null,
      center: null,
      userPosition: { lat: 0, lng: 0, lock: false },
      content: null,
      radius: 2500000, // ACZ --> put this const in config_env.
      showUserInfo: true,
      markers: [],
      defaultZoom: 11,
      zoom: null,
      bounds: null,
      googlMap: null,
      checkDistance: false,
      allCloseMarkers: []
    };

    this.handleMarkerClick = this.handleMarkerClick.bind(this);
    this.handleCloseClick = this.handleCloseClick.bind(this);
    this.handleMapClick = this.handleMapClick.bind(this);
    this.closeAllInfoBox = this.closeAllInfoBox.bind(this);
    this.offersToMarkers = this.offersToMarkers.bind(this);
    this.radiusChange = this.radiusChange.bind(this);
    this.handleMarkerClustererClick = this.handleMarkerClustererClick.bind(this);
    this.handleNewZoom = this.handleNewZoom.bind(this);
    this.resetUserPosition = this.resetUserPosition.bind(this);
  }

  componentWillMount() {
    fetch('http://ip-api.com/json')
      .then(res => res.json())
      .then(data => {
        const { filter } = this.props.offers;
        filter.radius = this.state.radius;
        filter.myPosLat = data.lat;
        filter.myPosLng = data.lon;
        this.props.sortOffersNew(filter);
      })
      .catch(err => {
        this.setState({ content: `Error: The Geolocation service failed (${error.message}).` });
      });
    this.setState({ zoom: this.state.defaultZoom });
  }

  componentWillReceiveProps(props) {
    if (props.coords && !props.coords.positionError) {
      this.setState({ center: { lat: props.coords.latitude, lng: props.coords.longitude } });
      if (!this.state.userPosition.lock) {
        this.setState({
          userPosition: {
            lat: props.coords.latitude,
            lng: props.coords.longitude,
            lock: true
          }
        });
      }
    } else {
      fetch('http://ip-api.com/json')
        .then(res => res.json())
        .then(data => {
          this.setState({ center: { lat: data.lat, lng: data.lon } });
          if (!this.state.userPosition.lock) {
            this.setState({
              userPosition: {
                lat: data.lat,
                lng: data.lon,
                lock: true
              }
            });
          }
        })
        .catch(error => {
          this.setState({ content: `Error: The Geolocation service failed (${error.message}).` });
        });
    }

    this.offersToMarkers(props.offers.list);
  }

  handleMapClick() {
    this.setState({ showUserInfo: false });
  }

  handleMarkerClick(targetMarker) {
    this.setState({
      markers: this.state.markers.map(marker => {
        const mark = {
          ...marker,
          showInfo: false
        };

        if (marker._id === targetMarker._id) {
          return {
            ...marker,
            showInfo: !marker.showInfo
          };
        }
        return mark;
      })
    });
  }

  closeAllInfoBox() {
    this.setState({
      markers: this.state.markers.map(marker => ({
        ...marker,
        showInfo: false
      })),
      zoom: 8
    });
  }

  handleCloseClick(targetMarker) {
    this.setState({
      markers: this.state.markers.map(marker => {
        if (marker._id === targetMarker._id) {
          return {
            ...marker,
            showInfo: !marker.showInfo
          };
        }
        return marker;
      })
    });
  }

  offersToMarkers(offers) {
    const markers = [];
    const userPos = this.state.userPosition;
    delete userPos.lock;
    offers.map((offer, i) => {
      let offerGeolocation;
      let offerDescription = 'NO DESCRIPTION';
      let offerThumb;
      if (offer) {
        offerDescription = offer.description;
        offerThumb = offer.media.mediaVault[offer.media.defaultIdx] || {
          mediaType: 'txt'
        };
      }
      if (offer.geolocation) {
        offerGeolocation = offer.geolocation.coords;
        markers.push({
          _id: offer._id,
          type: 'single',
          position: offerGeolocation,
          number: i + 1,
          content: offerDescription,
          price: offer.price,
          quantity: offer.quantity,
          currency: offer.currency,
          category: offer.category,
          title: offer.title,
          offer: offer.offer,
          thumbnail: offerThumb,
          showInfo: offer.showInfo || false
        });
      }
    });

    let closeMarkers = [];
    let clusteredMarkers = [];
    const allCloseMarkers = [];
    markers.forEach(marker1 => {
      const pos1 = {
        latitude: marker1.position.lat,
        longitude: marker1.position.lng
      };
      markers.forEach(marker2 => {
        if (marker1._id !== marker2._id) {
          const pos2 = {
            latitude: marker2.position.lat,
            longitude: marker2.position.lng
          };
          const distance = geolib.getDistance(pos1, pos2);
          const miles = (distance / 1609.34).toFixed(2);
          if (miles < 0.01) {
            if (closeMarkers.indexOf(marker1.number) === -1) {
              closeMarkers.push(marker1.number);
            }
            if (closeMarkers.indexOf(marker2.number) === -1) {
              closeMarkers.push(marker2.number);
            }
          }
        }
      });
      if (closeMarkers.length > 0) {
        if (
          !allCloseMarkers.some(
            a => a.length == closeMarkers.length && a.every(e => closeMarkers.includes(e))
          )
        ) {
          allCloseMarkers.push(closeMarkers);
        }
      }

      closeMarkers = [];
    });

    allCloseMarkers.map(markerArr => {
      markerArr.map(markerNum => {
        markers.forEach((marker, i) => {
          if (marker.number === markerNum) {
            clusteredMarkers.push(marker);
            markers.splice(i, 1);
          }
        });
      });

      markers.push({
        _id: clusteredMarkers[0]._id,
        type: 'cluster',
        position: clusteredMarkers[0].position,
        number: clusteredMarkers[0].number,
        markers: clusteredMarkers,
        showInfo: clusteredMarkers[0].showInfo || false
      });
      clusteredMarkers = [];
    });

    markers.push({ type: 'userPos', position: userPos });
    this.setState({
      checkDistance: true,
      allCloseMarkers,
      markers
    });
  }

  radiusChange(event) {
    this.setState({
      radius: event.target.value
    });

    const { filter } = this.props.offers;
    filter.radius = event.target.value;
    filter.myPosLat = this.state.center.lat;
    filter.myPosLng = this.state.center.lng;
    this.props.sortOffersNew(filter);
  }

  handleMarkerClustererClick(markerClusterer, zoomLevel) {
    const clickedMarkers = markerClusterer.getMarkers();
    this.setState({
      showUserInfo: false
    });
  }

  handleNewZoom(newZoom) {
    this.setState({ zoom: newZoom });
    if (newZoom > 20) {
      this.offersToMarkers(this.props.offers.list);
    }
  }

  resetUserPosition() {
    const newPosition = { lat: this.state.userPosition.lat, lng: this.state.userPosition.lng };
    this.closeAllInfoBox();
    this.setState({
      center: newPosition,
      zoom: this.state.defaultZoom
    });
  }

  render() {
    return (
      <Col xs={12} smOffset={0} mdOffset={0} style={{ padding: '0px' }}>
        {/* <div>
          <div style={{ fontFamily: 'Roboto', fontStyle: 'normal' }}>
            Offers within radius of:{' '}
            <input type="text" defaultValue={this.state.radius} onChange={this.radiusChange} />{' '}
            miles
            <br />
          </div>
        </div>
        <br />
 */}
        <div
          style={{
            width: '100%',
            height: '600px',
            position: 'relative'
          }}
        >
          <RaisedButton
            type="submit"
            style={{
              position: 'absolute',
              top: '55px',
              left: '113px',
              boxShadow: '5px 5px 8px #888888',
              zIndex: 5
            }}
            onClick={this.resetUserPosition}
            backgroundColor="#5FA02F"
            labelColor="#ffffff"
          >
            <IconButton tooltip="Reset Position" tooltipPosition="bottom-center" touch={true}>
              <FontIcon color="#ffffff" className="material-icons">
                my_location
              </FontIcon>
            </IconButton>
          </RaisedButton>
          <GeolocationGoogleMap
            onMapMounted={map => {
              if (this.state.googleMap != null) return;

              this.setState({
                googlMap: map
              });
            }}
            onMarkerClustererClick={this.handleMarkerClustererClick}
            center={this.state.center}
            showUserInfo={this.state.showUserInfo}
            content={this.state.content}
            radius={this.state.radius}
            onMapClick={this.handleMapClick}
            onMarkerClick={this.handleMarkerClick}
            onCloseClick={this.handleCloseClick}
            onNewZoom={this.handleNewZoom}
            closeAllInfoBox={this.closeAllInfoBox}
            zoom={this.state.zoom}
            markers={this.state.markers}
          />
        </div>
      </Col>
    );
  }
}

function mapStateToProps({ browser, offers }) {
  return { browser, offers };
}

const dispatchToProps = dispatch => ({
  sortOffersNew: filter => dispatch(offerActions.sortOffers(filter))
});

export default connect(mapStateToProps, dispatchToProps)(
  geolocated({
    positionOptions: {
      enableHighAccuracy: false
    },
    userDecisionTimeout: 5000
  })(OfferMap)
);
