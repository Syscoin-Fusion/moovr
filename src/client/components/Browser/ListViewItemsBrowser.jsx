import React from 'react';
import { Link } from 'react-router';
import { Row, Col } from 'react-bootstrap';
import { Paper } from 'material-ui';
import FontIcon from 'material-ui/FontIcon';
import { grey500, grey600 } from 'material-ui/styles/colors';

import VideoPlayer from '../Store/VideoPlayer';

import './styles/ListViewItemsBrowser.scss';

function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}
const styles = {
  paper: {
    background: '#f9f9f9',
    height: '120px',
    width: '100%',
  },
  imageContainer(url) {
    return {
      width: '100px',
      height: '100px',
      margin: '10px 10px 0px 10px',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      backgroundColor: '#fff',
      backgroundImage: url,
    };
  },
  videoContainer: {
    width: '100px',
    height: '100px',
    marginRight: '20px',
    overflow: 'hidden',
    backgroundColor: '#fff',
    position: 'relative',
  },
  video: {
    transform: 'translate(-50%, -50%)',
    position: 'absolute',
    top: '50%',
    left: '50%',
    width: '100%',
    height: '100%',
  },
  txtHeader: {
    cursor: 'pointer',
  },
  sortIconStyles: {
    marginLeft: '2px',
    color: grey500,
    verticalAlign: 'middle',
    fontSize: 'x-large',
  },
  trSeparator(color) {
    return { borderBottom: `1px solid ${color}` };
  },
};

class ListViewItemsBrowser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      thSortBy: '',
      thSortAZ: true,
    };
    this.getMedia = this.getMedia.bind(this);
    this.sortItems = this.sortItems.bind(this);
  }

  componentDidMount() {
    if (sessionStorage.getItem('catagory')) {
      const data = {
        category: sessionStorage.getItem('catagory').trim(),
      };
      this.props.onSearch(data);
      // this.handleToggle();
      sessionStorage.removeItem('catagory');
    }
  }

  getMedia(media) {
    // Should be ready - tbaustinedit
    let type = 'txt';
    let mediaURL = require('./assets/no_image.png');
    let videoSubtitles = '';
    if (media) {
      if (media.mediaVault.length > 0) {
        const mediaVault = media.mediaVault;
        const defaultIdx = media.defaultIdx;
        type = mediaVault[defaultIdx].mediaType;
        mediaURL = mediaVault[defaultIdx].mediaURL;
        videoSubtitles = mediaVault[defaultIdx].videoSubtitles;
      }
    }
    return { type, mediaURL, videoSubtitles };
  }

  sortItems(items) {
    const field = this.state.thSortBy;
    const sortAZ = this.state.thSortAZ;
    let sortedItems;
    if (field === '') {
      return items;
    }
    if (isNaN(parseFloat(items[0][field]))) {
      sortedItems = items
        .slice(0)
        .sort((a, b) => a[field].localeCompare(b[field], { numeric: true }));
    } else {
      sortedItems = items.slice(0).sort((a, b) => parseFloat(a[field]) - parseFloat(b[field]));
    }
    if (sortAZ) {
      return sortedItems;
    }
    return sortedItems.reverse();
  }

  thClick(field) {
    if (field === this.state.thSortBy) {
      this.setState({
        thSortAZ: !this.state.thSortAZ,
        thSortBy: field,
      });
    } else {
      this.setState({ thSortAZ: true, thSortBy: field });
    }
  }

  renderMedia(mediaData) {
    let mediaDiv = '';
    switch (mediaData.type) {
      case 'vid':
        mediaDiv = (
          <div style={styles.videoContainer}>
            <VideoPlayer
              style={styles.video}
              url={mediaData.mediaURL}
              subtitles={mediaData.videoSubtitles}
              playOnHover
              hideControls
              fullView
              muted
            />
          </div>
        );
        break;
      default:
        mediaDiv = <div style={styles.imageContainer(`url(${mediaData.mediaURL})`)} />;
        break;
    }
    return mediaDiv;
  }

  render() {
    console.log('ListViewItemsBrowser', this.props.items);
    const itemsOutput = this.sortItems(this.props.items).map((item, key) => {
      const mediaData = this.getMedia(item.media);
      const price = item.sysprice.toString();
      const title = item.title;
      const description = item.description;
      return (
        <div key={key}>
          <Col xs={12} xsOffset={0} key={item.txid}>
            <Paper style={styles.paper} zDepth={1}>
              <Link style={{ textDecoration: 'none' }} to={`/offer/${item._id}`}>
                <div className="listViewItemsBrowser__card">
                  <div className="listViewItemsBrowser__card-image">
                    <div>{this.renderMedia(mediaData)}</div>
                  </div>
                  <div className="listViewItemsBrowser__card-info">
                    <div className="listViewItemsBrowser__card-title">
                      <span>{title.length > 100 ? `${title.substr(0, 100)}...` : `${title}`}</span>
                    </div>
                    <div className="listViewItemsBrowser__card-description">
                      <span>
                        {description.length > 190
                          ? `${description.substr(0, 190)}...`
                          : `${description}`}
                      </span>
                    </div>
                    <div className="listViewItemsBrowser__card-price">
                      <span>
                        {`${price} SYS`}
                        {/* {price.length > 6 ? `${price.substr(0, 6)}... SYS` : `${price} SYS`} */}
                      </span>
                    </div>
                  </div>
                </div>
              </Link>
            </Paper>
          </Col>
        </div>
      );
    });
    return <div>{itemsOutput}</div>;
  }
}
export default ListViewItemsBrowser;
