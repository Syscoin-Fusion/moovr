import React from 'react';
import { connect } from 'react-redux';

import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import Menu from 'material-ui/Menu';
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right';
import Platform from 'react-platform-js';

import './styles/CatBreadcrums.scss';

const styles = {
  root: {
    display: 'auto',
  },
  label: {
    padding: '0 20px 20px',
    color: '#5FA02F',
  },
  span: {
    color: '#fff',
  },
};

class CatBreadcrums extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: Object.assign([], JSON.parse(JSON.stringify(this.props.categoriesList))),
      filterCategory: this.props.getFilterCategory,
      reRender: false,
    };
    this.renderSubcategoriesB = this.renderSubcategoriesB.bind(this);
    this.renderBreadCrumElem = this.renderBreadCrumElem.bind(this);
    this.renderNestedCategories = this.renderNestedCategories.bind(this);
    this.__selectedBreadCumbs = this.state.filterCategory;
    // console.log('Filter Category ====>', this.__selectedBreadCumbs);
    // console.log('Category List', this.state.categories);
    this.____categories = Object.assign([], JSON.parse(JSON.stringify(this.state.categories)));
  }

  componentWillMount() {
    this.getTreeLevel(this.state.categories, this.__selectedBreadCumbs, (arrr) => {
      this.filter(arrr);
    });
    // console.log('Component Did Mount', this.state.categories);
  }

  // Method will return in callback level index
  getTreeLevel(array, value, cb) {
    const level = [];
    for (let i = 0; i < array.length; i++) {
      level[0] = i;
      if (array[i] && array[i].cat == value) {
        i = array.length;
        level.splice(1, level.length);
        cb(level);
      }
      if (array[i] && array[i].items) {
        const level1Child = array[i].items;
        for (let j = 0; j < level1Child.length; j++) {
          level[1] = j;
          if (level1Child[j] && level1Child[j].cat == value) {
            i = array.length;
            j = level1Child.length;
            level.splice(2, level.length);
            cb(level);
          }
          if (array[i] && array[i].items[j] && array[i].items[j].items) {
            const level2Child = array[i].items[j].items;
            for (let k = 0; k < level2Child.length; k++) {
              level[2] = k;
              if (level2Child[k] && level2Child[k].cat == value) {
                i = array.length;
                j = level1Child.length;
                k = level2Child.length;
                level.splice(3, level.length);
                cb(level);
              }
              if (
                array[i] &&
                array[i].items[j] &&
                array[i].items[j].items &&
                array[i].items[j].items[k] &&
                array[i].items[j].items[k].items
              ) {
                const level3Child = array[i].items[j].items[k].items;
                for (let l = 0; l < level3Child.length; l++) {
                  level[3] = l;
                  if (level3Child[l] && level3Child[l].cat == value) {
                    i = array.length;
                    j = level1Child.length;
                    k = level2Child.length;
                    l = level3Child.length;
                    level.splice(4, level.length);
                    cb(level);
                  }
                  if (
                    array[i] &&
                    array[i].items[j] &&
                    array[i].items[j].items[k] &&
                    array[i].items[j].items[k].items &&
                    array[i].items[j].items[k].items[l] &&
                    array[i].items[j].items[k].items[l].items
                  ) {
                    const level4Child = array[i].items[j].items[k].items[l].items;
                    for (let m = 0; m < level4Child.length; m++) {
                      level[4] = m;
                      if (level4Child[m] && level4Child[m].cat == value) {
                        i = array.length;
                        j = level1Child.length;
                        k = level2Child.length;
                        l = level3Child.length;
                        m = level4Child.length;
                        level.splice(5, level.length);
                        cb(level);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  makeItemInTop(array, topCategory, level, cb) {
    this.getTreeLevel(array, topCategory, (__level) => {
      console.log('____level', __level);
      cb(__level);
    });
  }

  organizeTreeStructure(ev, key, value) {
    let __categoryCopy;
    if (value.indexOf('>') > -1) {
      __categoryCopy = Object.assign([], this.state.categories);
    } else {
      this.setState({
        categories: Object.assign([], JSON.parse(JSON.stringify(this.props.categoriesList))),
      });
      __categoryCopy = Object.assign([], JSON.parse(JSON.stringify(this.props.categoriesList)));
    }

    // Make the value in top
    let levelIndex = [];
    this.props.handleNewCategory(ev, key, value);
    this.makeItemInTop(__categoryCopy, value, 0, (sequenceArray) => {
      levelIndex = sequenceArray;
      if (levelIndex.length == 1) {
        var _tempItem = __categoryCopy[0];
        __categoryCopy[0] = __categoryCopy[levelIndex[0]];
        __categoryCopy[levelIndex[0]] = _tempItem;
      } else if (levelIndex.length == 2) {
        var _tempItem = __categoryCopy[0].items[0];
        __categoryCopy[0].items[0] = __categoryCopy[0].items[levelIndex[1]];
        __categoryCopy[0].items[levelIndex[1]] = _tempItem;
      } else if (levelIndex.length == 3) {
        var _tempItem = __categoryCopy[0].items[0].items[0];
        __categoryCopy[0].items[0].items[0] = __categoryCopy[0].items[0].items[levelIndex[2]];
        __categoryCopy[0].items[0].items[levelIndex[2]] = _tempItem;
      } else if (levelIndex.length == 4) {
        var _tempItem = __categoryCopy[0].items[0].items[0].items[0];
        __categoryCopy[0].items[0].items[0].items[0] =
          __categoryCopy[0].items[0].items[0].items[levelIndex[3]];
        __categoryCopy[0].items[0].items[0].items[levelIndex[3]] = _tempItem;
      }
      this.setState({
        reRender: true,
        categories: __categoryCopy,
      });
    });
  }

  renderSubcategoriesB(items) {
    return items.map(
      (cat, i) =>
        cat.items ? (
          <MenuItem
            key={i}
            value={cat.cat}
            primaryText={`${cat.text}`}
            rightIcon={<FontIcon className="material-icons">more_horiz</FontIcon>}
          />
        ) : (
          <MenuItem key={i} value={cat.cat} primaryText={`${cat.text}`} />
        ),
    );
  }

  renderBreadCrumElem(_categories, index) {
    const ddItems = Object.assign([], _categories);
    // ddItems.splice(index, 1);
    return (
      <div className="CatBreadcrums__itemWrap">
        <div className="CatBreadcrums__spacer">
          <FontIcon className="material-icons" style={{ color: '#999' }}>
            keyboard_arrow_right
          </FontIcon>
        </div>
        <DropDownMenu
          className="CatBreadcrums__dropDown"
          value={_categories[index].cat}
          iconStyle={{ display: 'none' }}
          underlineStyle={{ display: 'none' }}
          labelStyle={styles.label}
          onChange={(ev, key, value) => this.organizeTreeStructure(ev, key, value)}
          menuStyle={{ backgroundColor: '#F3F2F7' }}
          key={_categories[index].cat}
        >
          {this.renderSubcategoriesB(ddItems)}
        </DropDownMenu>
        {_categories[index].items && _categories[index].items.length > 0
          ? this.renderBreadCrumElem(_categories[index].items, index)
          : null}
      </div>
    );
  }

  renderNestedCategories(_categoryArray, key) {
    const __nestedMenu = [];
    if (_categoryArray && _categoryArray.length > 0) {
      for (let i = 0; i < _categoryArray.length; i++) {
        if (_categoryArray[i].items) {
          const cati = _categoryArray[i].cat;
          __nestedMenu.push(
            <MenuItem
              key={key + i}
              primaryText={_categoryArray[i].text}
              rightIcon={<ArrowDropRight />}
              menuItems={this.renderNestedCategories(
                _categoryArray[i].items,
                _categoryArray[i].cat,
              )}
            />,
          );
        } else {
          const cati = _categoryArray[i].cat,
            hasChild = _categoryArray[i].text == 'All';
          __nestedMenu.push(
            <MenuItem
              key={key + i}
              onTouchTap={() => this.nestedBreadCrumbSelected(cati, hasChild)}
              primaryText={_categoryArray[i].text}
            />,
          );
        }
      }
    }
    return __nestedMenu;
  }

  nestedBreadCrumbSelected(value, hasChild) {
    let valueShown = value;
    if (hasChild && value !== 'All') {
      valueShown = `${valueShown} > All`;
    }
    this.setState({ filterCategory: valueShown });
    this.props.handleNewCategory(null, null, value);
  }

  filter(array) {
    if (array) {
      if (array.length === 4) {
        const cat = this.____categories[array[0]].items[array[1]].items[array[2]].items[array[3]];
        this.____categories[array[0]].items[array[1]].items[array[2]].items.splice(array[3], 1);
        this.____categories[array[0]].items[array[1]].items[array[2]].items.unshift(cat);
        array.pop();
        this.filter(array);
      } else if (array.length === 3) {
        const cat2 = this.____categories[array[0]].items[array[1]].items[array[2]];
        this.____categories[array[0]].items[array[1]].items.splice(array[2], 1);
        this.____categories[array[0]].items[array[1]].items.unshift(cat2);
        array.pop();
        this.filter(array);
      } else if (array.length === 2) {
        const cat3 = this.____categories[array[0]].items[array[1]];
        this.____categories[array[0]].items.splice(array[1], 1);
        this.____categories[array[0]].items.unshift(cat3);
        array.pop();
        this.filter(array);
      } else if (array.length === 1) {
        const cat4 = this.____categories[array[0]];
        this.____categories.splice(array[0], 1);
        this.____categories.unshift(cat4);
        array.pop();
        this.filter(array);
      } else if (array.length === 0) {
        this.setState({
          reRender: true,
          categories: this.____categories,
        });
      }
    }
  }

  componentWillReceiveProps(nxtProps) {
    const isMobile = Platform.DeviceType === 'mobile';
    if (isMobile) {
      this.setState({ filterCategory: nxtProps.getFilterCategory });
    } else {
      this.getTreeLevel(this.state.categories, nxtProps.getFilterCategory, (arrr) => {
        this.filter(arrr);
      });
    }
  }

  render() {
    const isMobile = Platform.DeviceType === 'mobile';
    return (
      <div className="CatBreadcrums__wrap">
        {isMobile ? (
          <div className={this.state.filterCategory === 'All'?'default_selected mobile__container':'mobile__container'}>
            {this.state.filterCategory === 'All'?<div className="CatBreadcrums__title">Category > </div>: null}
            <Menu desktop width={320} className="nested-category-mobile">
              <MenuItem
                key="parent"
                primaryText={this.state.filterCategory}
                menuItems={this.renderNestedCategories(this.state.categories, 'parent')}
              />
            </Menu>
          </div>
        ) : (
          <div className="desktop__container">
            <div className="CatBreadcrums__title">Category</div>
            {this.renderBreadCrumElem(this.state.categories, 0)}
          </div>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const categoriesList = state.categories.list;
  const getFilterCategory = state.offers.filter.category;
  return { categoriesList, getFilterCategory };
}

export default connect(mapStateToProps, null)(CatBreadcrums);
