import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import RaisedButton from 'material-ui/RaisedButton';

import VideoPlayer from '../Store/VideoPlayer';

import './styles/style.scss';

function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

class OfferSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      price: '',
      description: '',
      mediaShow: '',
      mediaVault: [],
    };
    this.onItemClick = this.onItemClick.bind(this);
    this.prepareData = this.prepareData.bind(this);
  }

  componentDidMount() {
    this.prepareData(this.props.offer);
  }
  componentWillReceiveProps(nxtProps) {
    this.prepareData(nxtProps);
  }

  onItemClick(item) {
    // It's due to '../Store/VideoPlayer' lib issue.
    this.setState(
      {
        mediaShow: {
          mediaType: 'img',
          mediaURL: './assets/no_image.png',
          videoSubtitles: '',
          poster: '',
        },
      },
      () => {
        this.setState({ mediaShow: item });
      },
    );
  }

  prepareData(offer) {
    if (offer.media.mediaVault.length > 0) {
      this.setState({
        mediaVault: offer.media.mediaVault,
        mediaShow: offer.media.mediaVault[offer.media.defaultIdx],
      });
    }
    this.setState({
      title: offer.title,
      price: `${offer.price} ${offer.currency}`,
      description: offer.description,
    });
  }

  renderMedia() {
    let thumb = (
      <li className="active-image">
        <img alt="" src={require('./assets/no_image.png')} className="img img-responsive" />
      </li>
    );
    if (this.state.mediaVault.length > 0) {
      thumb = this.state.mediaVault.map((media, i) => {
        switch (media.mediaType) {
          case 'img':
            return (
              <li key={i} onClick={() => this.onItemClick(media)}>
                <img alt="" src={media.mediaURL} className="img img-responsive" />
              </li>
            );
          case 'vid':
            return (
              <li key={i} onClick={() => this.onItemClick(media)}>
                <VideoPlayer
                  url={media.mediaURL}
                  subtitles={media.videoSubtitles}
                  hideControls
                  thumbnail
                />
              </li>
            );
          default:
            break;
        }
      });
    }
    return thumb;
  }

  render() {
    const offer = this.state;
    const defaultUrl = this.state.mediaShow.mediaURL
      ? this.state.mediaShow.mediaURL
      : require('./assets/no_image.png');
    const mediaType = this.state.mediaShow.mediaType;
    const videoSubtitles = this.state.mediaShow.videoSubtitles
      ? this.state.mediaShow.videoSubtitles
      : '';

    return (
      <Col xs={12} className="offerSlider-wrapper">
        <div className="slider_content">
          {this.state.mediaVault.length > 1 ? (
            <Col xs={12} lg={1} sm={1} className="offerSlider-thumbnails">
              <ul>{this.renderMedia()}</ul>
            </Col>
          ) : null}
          <Col
            xs={12}
            sm={11}
            lg={11}
            className={
              this.state.mediaVault.length == 1
                ? 'no-padding offerSlider-image'
                : 'offerSlider-image'
            }
          >
            <Col xs={12} md={5} className={this.state.mediaVault.length == 1 ? 'no-padding' : ''}>
              {mediaType === 'vid' ? (
                <Col xs={11}>
                  <VideoPlayer
                    url={defaultUrl}
                    subtitles={videoSubtitles}
                    playOnLoad
                    hideControls
                  />
                </Col>
              ) : (
                <img alt="" src={defaultUrl} className="img img-responsive item-image" />
              )}
            </Col>
            <Col xsHidden smHidden md={7} className="OfferSlider__wideWrap">
              <h2 className="offerSlider-title">{offer.title}</h2>
              <h3 className="offerSlider-title__small">{`From: ${offer.price}`}</h3>
              <p className="offerSlider-description">{offer.description}</p>
              <RaisedButton label="Add to Cart" className="btn-green" />
            </Col>
            <Col xs={12} mdHidden lgHidden>
              <h2 className="offerSlider-title">{offer.title}</h2>
              <h3 className="offerSlider-title__small">{`From: ${offer.price}`}</h3>
              <p className="offerSlider-description">{offer.description}</p>
              <RaisedButton label="Add to Cart" className="btn-green" />
            </Col>
          </Col>
        </div>
      </Col>
    );
  }
}

export default OfferSlider;
