import React from 'react';

// components
import { Row, Col } from 'react-bootstrap';
import OfferTabs from './OfferTabs';
import OfferSlider from './OfferSlider';
/* import VideoPlayer from '../Store/VideoPlayer'; */

import './styles/style.scss';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
    color: '#5FA02F',
  },
  bar: {
    backgroundColor: '#5FA02F',
    borderColor: '#5FA02F',
  },
  tab: {
    color: '#CDCDCD',
  },
  activeTab: {
    background: '#FFF',
  },
};

class OfferViewSuccess extends React.Component {
  constructor(props) {
    super(props);
  }

  handleActive(e) {
    const activeTabs = document.getElementsByClassName('active-link');
    if (activeTabs) {
      for (let i = 0; i < activeTabs.length; i++) {
        activeTabs[i].className = '';
      }
    }
    e.target.className = '';
    e.target.className = 'active-link';
  }

  render() {
    return (
      <div className="offers-main-div">
        <Col>
          <Row style={{ paddingRight: '10px' }}>
            <OfferSlider offer={this.props.offer} />
          </Row>
          <Row style={{ backgroundColor: '#fff' }}>
            <OfferTabs styles={styles} />
          </Row>
        </Col>
      </div>
    );
  }
}

export default OfferViewSuccess;
