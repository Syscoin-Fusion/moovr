import React from 'react';
import { Tabs, Tab } from 'material-ui/Tabs';
import { List, ListItem } from 'material-ui/List';
import RaisedButton from 'material-ui/RaisedButton';
import StarRating from 'material-ui/svg-icons/toggle/star';
import { Col, Button } from 'react-bootstrap';

function handleActive(e) {
  console.log(e.target,"working e")
  const activeTabs = document.getElementsByClassName('active-link');
  if (activeTabs) {
    for (let i = 0; i < activeTabs.length; i++) {
      activeTabs[i].className = '';
    }
  }
  e.target.className = '';
  e.target.className = 'active-link';
}
const styles = {
  bar: {
    backgroundColor: '#5FA02F',
  },
};
const tabStyle = {
  background: '#fff',
  color: '#5C8788',
  borderBottom: '1.4px solid #F6F5F9',
  textTransform: 'none',
};

const OfferTabs = props => (
  <Col xs={12} sm={10} className="product-info-tabs tab-view-wrapper" smOffset={1}>
    <Tabs inkBarStyle={styles.bar} className="tab-container">
      {/* overvie tab */}
      <Tab label="Overview" className="tab" onClick={e =>handleActive(e)}>
        <div className="tab-content">
          <p>
            Mean Girls (Meninas Malvadas, no Brasil; Giras e Terríveis, em Portugal) é um filme
            americano de 2004 dirigido por Mark Waters e escrito por Tina Fey. A atriz americana
            Lindsay Lohan é a protagonista e interpreta Cady, uma ingênua garota que retornou à
            América após doze anos a viver em África e que vai à escola pela primeira vez aos
            dezesseis anos após receber ensino domiciliar por anos.
          </p>
        </div>
      </Tab>
      {/* rating tab */}
      <Tab label="Ratings" className="tab" onClick={e =>handleActive(e)}>
        <div className="tab-content rating-container">
          <div className="rating-content">
            <div className="rating-star">
              <StarRating />
              <StarRating />
              <StarRating />
              <StarRating />
              <StarRating />
            </div>
            <div className="tab-comments">
              <h4>Great table for basic use</h4>
              <h5>
                By <label>Brian</label> 10th August 2017
              </h5>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                pariatur.
              </p>
            </div>
          </div>
          <div className="rating-content">
            <div className="rating-star">
              <StarRating />
              <StarRating />
              <StarRating />
              <StarRating />
              <StarRating />
            </div>
            <div className="tab-comments">
              <h4>Awesome product</h4>
              <h5>
                By <label>Madge</label> 15th August 2017
              </h5>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                pariatur.
              </p>
            </div>
          </div>
          <RaisedButton label="View More" className="btn-green" />
        </div>
      </Tab>
      {/* seller info tab */}
      <Tab label="Seller Info" className="tab" onClick={e =>handleActive(e)}>
        <div className="tab-content">
          <h3 className="tab-content-title">Seller Name</h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
            dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </p>
          <RaisedButton label="View Profile" className="btn-primary" />
        </div>
      </Tab>
      {/* faq tab */}
      {/* <Tab label="FAQ" className="tab-info" onClick={handleActive}>
        <div className="tab-content">

          <div className="faq-content">
            <h3>Q: What do I get with this</h3>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
            dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </p>
          </div>

          <div className="faq-content">
            <h3>Q: What do I get with this</h3>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
            dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </p>
          <RaisedButton label="View Profile" className="btn-green"/>
          </div>

        </div>
      </Tab> */}
    </Tabs>
  </Col>
);

export default OfferTabs;
