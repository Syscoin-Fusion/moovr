import React from 'react';
import { connect } from 'react-redux';

// redux
import offerActions from '../../redux/actions/offerActions';

// components
import { Row, Col, Grid } from 'react-bootstrap';
import OfferViewSuccess from './OfferViewSuccess';

const newItemStyle = {
  loadingDiv: {
    marginTop: '30vh',
    textAlign: 'center',
  },
};

class Offer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      offerId: '',
    };
  }
  componentWillMount() {
    const offerId = this.props.params ? this.props.params.id : 'NoOfferByURL';
    this.setState({ offerId });
    this.offerToRender(offerId);
  }

  offerToRender(offerId) {
    const { filter } = this.props.offers;
    filter.offerId = offerId;
    this.props.sortOffersNew(filter);
  }

  render() {
    let offer = '';
    let renderMe;
    if (this.props.newOffer.guid) {
      offer = this.props.newOffer.offer;
    }
    if (this.props.offers.list.length === 1) {
      offer = this.props.offers.list[0];
    }
    if (!offer.offer) {
      renderMe = (
        <Grid>
          <h1 style={newItemStyle.loadingDiv}>Ooops! No offer Found</h1>
        </Grid>
      );
    }
    if (offer.offer) {
      renderMe = (
        <Grid>
          <OfferViewSuccess offer={offer} />{' '}
        </Grid>
      );
    }
    return renderMe;
  }
}

function mapStateToProps(state) {
  const offers = state.offers;
  const newOffer = state.newOffer;
  return { offers, newOffer };
}

function mapDispatchToProps(dispatch) {
  return {
    sortOffersNew: params => dispatch(offerActions.sortOffers(params)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Offer);
