import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

import actions from '../redux/actions/offerActions';

class Test extends Component {
  test(event) {
    event.preventDefault();

    const filter = {
      offerId: '', // if !== "" then override all following coditions
      regExp: '', // Search by this string, if === "" then search ALL
      radius: 3, // if !== 0 then override pagination
      belongsTo: null, // [ true | false | null ]
      category: 'All', // if === 'All' show all offers
      mediaType: '', // if "" return all types, [ vid | img | aud | text | "" ]
      paymentoptions_display: '', // if "" return all types, [ SYS | BTC | ZEC ]
      itemPerPage: 30, // if === 0 then override pagination, the value are fixed by client
      activePage: 1, // active page
    };

    axios
      .post('/API/users/login', { aliasname: 'test123', password: '123' })
      .then(response => {
        console.log('ACZ (/API/users/login): ', response.data);
      })
      .catch(err => {
        console.log('ACZ (/offerfilter.err): ', err);
      });
  }

  render() {
    return (
      <div>
        <button
          style={{ width: '100px', height: '100px', fontSize: '28px' }}
          onClick={this.test.bind(this)}
        >
          Test
        </button>
      </div>
    );
  }
}

const stateToProps = state => ({
  offers: state.offers,
});

const dispatchToProps = dispatch => ({
  sortOffers: params => dispatch(actions.sortOffers(params)),
});

export default connect(stateToProps, dispatchToProps)(Test);
