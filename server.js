require('babel-core/register');

/* if (process.env.NODE_ENV !== 'production') {
  require('dotenv').load();
} */

['.css', '.less', '.sass', '.ttf', '.woff', '.woff2'].forEach((ext) => {
  require.extensions[ext] = () => {};
  return ext;
});

require('babel-polyfill');
require('./src/server/app.js');

global.navigator = { navigator: 'all' };
