# Offer Object transformations

## We have this in the DB:
```json
{  
  "_id": {  
    "$oid": ""  
  },  
  "offer": "",  
  "txid": "",  
  "expires_in": 0,  
  "expires_on": 0,  
  "expired": false,  
  "height": "",  
  "category": "",  
  "title": "",  
  "quantity": "",  
  "currency": "",  
  "sysprice": 0,  
  "price": "",  
  "ismine": false,  
  "commission": "",  
  "offerlink": "",  
  "private": "",  
  "paymentoptions": 0,  
  "paymentoptions_display": "",  
  "alias_peg": "",  
  "description": "{
    "hash":"",  
    "description":"",  
    "media": {  
      "defaultIdx": 0,  
      "mediaVault": [ 
        {"mediaType": "", "mediaURL": "", "videoSubtitles":"", "poster": "" }  
      ]  
    }  
  }",  
  "alias": "",  
  "address": "",  
  "alias_rating_display": "",  
  "offers_sold": 0,  
  "geolocation": "{  
    "coords": {  
      "lat": 0,  
      "lng": 0  
    }  
  }",  
  "alias_rating_count": 0,  
  "alias_rating": 0,  
  "safetylevel": 0,  
  "safesearch": "",  
  "offerlink_seller": "",  
  "offerlink_guid": "",  
  "time": {  
      "$date": ""  
  },  
  "cert": "",  
  "__v": 0  
}
```
`description` field is stringify and inside we save:  
```json
{ 
  "hash":"", 
  "description":"",  
  "media": {  
    "defaultIdx": 0,  
    "mediaVault": [   
      {"mediaType": "", "mediaURL": "", "videoSubtitles":"", "poster": "" }  
    ]  
  }  
}
```  
`geolocation` field is stringify and inside we save:
```json
{  
  "coords": {  
    "lat": 0,  
    "lng": 0  
  }  
}
```  


## And we serve this: