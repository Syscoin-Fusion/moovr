# Redux Descriptions

## Folder tree

The path where the redux files are:  
```
/src/client/redux
```
it have four folders:
- **`actions`**: We put here the code that get a new value to be set in the Store.
- **`constants`**: The code here set the names of the actions.
- **`middleware`**: Defines functions that can be called before reducers, this is a good place to make check or adapt info
- **`reducer`**: This code are use to set the new state of the redux store.  

## Imports
- **`actions`**: They import the modules that are needed to get the values and constants.
- **`constants`**: It is only a definition of actions.
- **`middleware`**: Piece of code that run automatically when certain conditions happend.
- **`reducer`**: This code is in charge of modifying the Redux store state.

## Naming files
`actions`, `constants`, `middleware` and `reducer` files have the same name to identify correctly its context in the redux tree and add its type as sufix using camelcase.

```
./redux/actions/offerActions.js
./redux/constants/offerConstants.js
./redux/middleware/offerMiddleware.js
./redux/reducer/offerReducer.js
```