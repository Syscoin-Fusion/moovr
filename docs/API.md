# API Description

## Folder tree

The path where the server API is:  
```
/src/server/API
```
it have three folders:
- **`controllers`**: We put here the code that interact directly with our DB.
- **`helpers`**: The code here makes transformation or prepare the items returned by controllers.
- **`routes`**: Defines the public path where to attend the get/post requests from the client browser and use helpers for that.

## Imports
- **`controllers`**: They import the modules that are needed to manage the DB.
- **`helpers`**: They import controllers and other modules needed.
- **`controllers`**: They import helpers and other modules but never controllers.

## Naming files
`Controllers`, `helpers` and `routes` files have the same name to identify correctly its chain  in the API definitions.

```
./API/controllers/offer.js
./API/helper/offer.js
./API/routes/offer.js
```