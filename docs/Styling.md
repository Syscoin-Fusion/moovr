# Styling rules
```
NOTE: If you see classes that not match with the next rules, we leave they as they is until we have to modify it.
```
***
  
## Naming CSS classes
  
We are using for naming classes [BEM](URL "http://getbem.com/naming/") philosophy but adapted to our project, basically it uses this schema:  
```css
.block__element--modifier
```

### **`block`**:
In our case the `block` part will be the name of the components where applied.  
  
**CSS class**: we never can found a class formed with only block, we always use element naming.

### **`element`**:
It will be any block or significant part of the code in the render/return function of a component.

**CSS class**: it is formed as component name plus two underscores plus element name (`.block__elem`).
  
`Browser.jsx`
```jsx
class Browser extends React.Component {
  ...
render () {
  return(
    <div className="browser__wrap">
      ...
    </div>
  )
}
```
`Browser.scss`
```css
.browser__wrap {
  margin: 30px;
}
```

### **`modifier`**:
It will be Flags on elements and is used to change appearence, behavior or state.

**CSS class**: it is formed as element's name plus two dashes: (`.block__elem--hidden`).  
Complicates modiffier are name using [camelCase](URL "https://en.wikipedia.org/wiki/Camel_case"): (`.block__elem--colorGreen`).

`Browser.jsx`
```jsx
class Browser extends React.Component {
  ...
render () {
  return(
    <div className="browser__wrap browser__wrap--fontColorGreen">
      ...
    </div>
  )
}
```
`Browser.scss`
```css
.browser__wrap {
  margin: 30px;
}

.browser__wrap--fontColorGreen{
  color: green;
}
```