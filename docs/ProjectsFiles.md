# Naming Files
## Style Sheets files (.scss)
- The Style Sheets files sould be named after his component files where it is applied  
  - **i.e.**: _If a component are called `Browser.js` and needs a Style Sheet, it have to be called `browsre.scss` and we only can find in it, styles that apply to this component._
