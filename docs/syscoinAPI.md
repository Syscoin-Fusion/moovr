## Offers

**`/offerupdate`**

minimum fields required: 
- `alias`,
- `guid`,
- `category`,
- `title`,
- `quantity`,
- `price`,
- `description`,
- `currency`,
- `paymentoptions`

Example:
```Json
{
    "alias": "cortesa",
    "guid": "c5ef79b0af9cc020",
    "category": "for sale",
    "title": "ACZ test for the hash",
    "quantity": 0,
    "price": 123,
    "description": "{\"hash\":\"$2a$08$Ij3T3SJ/gzxSJNMxdKlzeubW6CUXk6GEAaxLpdYQXJ1Y/B92IHYCS\",\"description\":\" Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\",\"media\":{\"defaultIdx\":0,\"mediaVault\":[{\"mediaType\":\"img\",\"mediaURL\":\"https://i.imgur.com/CG8VFBO.png?1\",\"videoSubtitles\":\"\",\"poster\":\"\"}]}}",
    "currency": "USD",
    "paymentoptions": "SYS"
}
```